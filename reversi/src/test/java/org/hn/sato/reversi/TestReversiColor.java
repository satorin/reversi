package org.hn.sato.reversi;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * ReversiPointのテスト
 *
 * @author s-kotaki
 *
 */
@RunWith(JUnit4.class)
public class TestReversiColor {

	/**
	 * fromValue
	 */
	@Test
	public void fromValue_001() {
		assertThat(ReversiColor.BLACK, is(ReversiColor.fromValue(1)));
		assertThat(ReversiColor.WHITE, is(ReversiColor.fromValue(-1)));
		assertThat(ReversiColor.NONE, is(ReversiColor.fromValue(0)));
	}

	/**
	 * fromValue
	 */
	@Test
	public void fromValue_002() {
		try {
			final ReversiColor color = ReversiColor.fromValue(100);
			fail("例外が発生しませんでした。" + color);
		} catch (IllegalArgumentException e) {
			assertThat("無効な色が指定されました。" + 100, is(e.getMessage()));
		}
	}
}
