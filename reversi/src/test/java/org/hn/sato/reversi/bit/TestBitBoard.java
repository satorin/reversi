package org.hn.sato.reversi.bit;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import org.hn.sato.reversi.ReversiColor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * BitBoardのテスト
 *
 * @author s-kotaki
 *
 */
@RunWith(JUnit4.class)
public class TestBitBoard {

	/**
	 * getFlipPoint
	 */
	@Test
	public void getFlipPoint_001() {
		final BitBoard board = new BitBoard(
				"□○○○○●●●" +
				"□□○●●●●●" +
				"□□○●●●○●" +
				"□○○●●●●●" +
				"□○○●○●○●" +
				"□□○○●○○□" +
				"□□○○○○○○" +
				"□□□●●●●□");
		final long blackFlipPoint = board.getFlipPoint(ReversiColor.BLACK.getValue());
		final long whiteFlipPoint = board.getFlipPoint(ReversiColor.WHITE.getValue());
		assertThat(blackFlipPoint,
				is(BitPoint.A1.mask | BitPoint.A3.mask | BitPoint.A4.mask | BitPoint.A5.mask | BitPoint.A6.mask
						| BitPoint.B2.mask | BitPoint.B3.mask | BitPoint.B6.mask | BitPoint.B7.mask | BitPoint.B8.mask
						| BitPoint.C8.mask | BitPoint.H6.mask | BitPoint.H8.mask));
		assertThat(whiteFlipPoint, is(0L));
	}

	/**
	 * getFlipPoint
	 */
	@Test
	public void getFlipPoint_002() {
		final BitBoard board = new BitBoard(
				"□□○○○●□□" +
				"□□○●●□□□" +
				"□□○●●●○●" +
				"□○○●●●●●" +
				"□○○●○●○□" +
				"□□○○●○○○" +
				"□□○○○○□□" +
				"□□□●●□□□");
		final long blackFlipPoint = board.getFlipPoint(ReversiColor.BLACK.getValue());
		final long whiteFlipPoint = board.getFlipPoint(ReversiColor.WHITE.getValue());
		assertThat(blackFlipPoint,
				is(BitPoint.A4.mask | BitPoint.A5.mask | BitPoint.A6.mask
						| BitPoint.B1.mask | BitPoint.B2.mask | BitPoint.B3.mask | BitPoint.B6.mask | BitPoint.B7.mask | BitPoint.B8.mask
						| BitPoint.C8.mask | BitPoint.F2.mask | BitPoint.F8.mask | BitPoint.G2.mask | BitPoint.G7.mask | BitPoint.G8.mask
						| BitPoint.H2.mask | BitPoint.H5.mask | BitPoint.H7.mask));
		assertThat(whiteFlipPoint,
				is(BitPoint.F2.mask | BitPoint.G1.mask | BitPoint.G2.mask | BitPoint.H5.mask));
	}

	/**
	 * flip
	 */
	@Test
	public void flip_001() {
		final BitBoard board = new BitBoard(
				"□□○○○●□□" +
				"□□●●●●□□" +
				"●□○●●●○●" +
				"□○○○●●●●" +
				"●○□○○●○●" +
				"□□○○●○○□" +
				"□□○○○○□□" +
				"□□●●●●□□");
		final BitBoard turned = board.flip(BitPoint.C5.mask, ReversiColor.BLACK.getValue());
		final BitBoard expected = new BitBoard(
				"□□○○○●□□" +
				"□□●●●●□□" +
				"●□●●●●○●" +
				"□●●●●●●●" +
				"●●●●●●○●" +
				"□□●●●○○□" +
				"□□●○●○□□" +
				"□□●●●●□□");
		assertThat(turned.blackDiscs, is(expected.blackDiscs));
		assertThat(turned.whiteDiscs, is(expected.whiteDiscs));
	}
}
