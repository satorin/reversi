package org.hn.sato.reversi;

import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitMask;
import org.hn.sato.reversi.bit.BitPoint;

public class Test {

	public static void main(String[] args) {

		long a = BitMask.UPPER_LINE.mask;
		final long bb = BitPoint.A1.mask;
		a ^= bb;
		System.out.println(BitBoardUtil.toBoardString(a));
		final BitBoard b = new BitBoard();
		b.put(BitPoint.H1, 1);
		b.put(BitPoint.H2, 1);
		b.put(BitPoint.H8, 1);
//		b.put(BitPoint.H6, 1);
//		b.put(BitPoint.H7, 1);
//		b.put(BitPoint.B8, 1);
//		b.put(BitPoint.C8, 1);
//		b.put(BitPoint.D8, 1);
//		b.put(BitPoint.E8, 1);
//		b.put(BitPoint.F8, 1);
//		b.put(BitPoint.G8, 1);
//		b.put(BitPoint.H8, 1);
		System.out.println(BitBoardUtil.toBitString(b.blackDiscs));
		System.out.println(BitBoardUtil.toHexString(b.blackDiscs));
		System.out.println(BitBoardUtil.toBoardString(b.blackDiscs));
		System.out.println();
	}

}
