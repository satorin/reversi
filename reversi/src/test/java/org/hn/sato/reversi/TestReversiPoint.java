package org.hn.sato.reversi;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * ReversiPointのテスト
 *
 * @author s-kotaki
 *
 */
@RunWith(JUnit4.class)
public class TestReversiPoint {

	/**
	 * fromValue
	 */
	@Test
	public void fromValue() {
		assertThat(ReversiPoint.A1, is(ReversiPoint.fromValue(ReversiPoint.A1.x, ReversiPoint.A1.y)));
		assertThat(ReversiPoint.H8, is(ReversiPoint.fromValue(ReversiPoint.H8.x, ReversiPoint.H8.y)));
		assertThat(ReversiPoint.OUT_OF_RANGE, is(ReversiPoint.fromValue(-1, -1)));
		assertThat(ReversiPoint.OUT_OF_RANGE, is(ReversiPoint.fromValue(1, 1001)));
	}
}
