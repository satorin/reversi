package org.hn.sato.reversi;

import java.util.Stack;

public class Test2 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		final Stack<ReversiBoard> stack = new Stack<>();

		final ReversiBoard board = new ReversiBoard();

		stack.push(board.clone());

		board.flip(ReversiPoint.D3, 1);

		stack.push(board.clone());

		System.out.println(stack);

		stack.pop();

		stack.push(board.clone());

		System.out.println(stack);
	}

}
