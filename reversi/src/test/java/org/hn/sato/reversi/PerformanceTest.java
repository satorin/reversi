package org.hn.sato.reversi;

import org.hn.sato.reversi.ai.ArtificalIntelligence;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.ai.bitsimulator.BitEval;
import org.hn.sato.reversi.ai.bitsimulator.MiddleBitSimulator;
import org.hn.sato.reversi.ai.bitsimulator.SBitMiniMaxAlphaBeta;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitPoint;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * 性能テスト
 *
 * @author s-kotaki
 *
 */
@RunWith(JUnit4.class)
public class PerformanceTest {

	/**
	 * 性能テスト(SBitMiniMaxAlphaBeta.execute)
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void execute_001() throws InterruptedException {

		final ReversiBoard board = new ReversiBoard(
				"□□□○○○○□" +
				"□□○○○○□□" +
				"□□○○●○○□" +
				"□○○○●○□●" +
				"□○○○○○●●" +
				"□□○○●●●●" +
				"□□●○●●□□" +
				"□□□○●□□□");

		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final long start = System.currentTimeMillis();

		for (int i = 0; i < 1; i++) {
			ai.execute(board, ReversiColor.BLACK, 12, 18);
		}

		final long end = System.currentTimeMillis();
		System.out.println((end - start) + "ms");

		// 2018/07/30 (読みの深さ 8 評価回数：44,970) 385ms
		// 2018/07/30 (読みの深さ10 評価回数：419,807) 1,616ms
		// 2018/07/30 (読みの深さ12 評価回数：3,059,876) 8,548ms
		// 2018/08/03 (読みの深さ12 評価回数：3,789,405) 12,922ms･･･手数の評価を詳細化(純粋手数・余裕手)
		// 2018/09/03（読みの深さ12 評価回数：7,857,360）50,198ms･･･いろいろ評価値増やした
		// 2018/09/05（読みの深さ12 評価回数：6,457,077）34,767ms･･･evalLineの条件見直し
	}

	/**
	 * 性能テスト(BitEval.evalFixedDisc)
	 */
	@Test
	public void evalFixedDisc_001() {
		final BitBoard board = new BitBoard(
				"○○●●□□□○" +
				"○○●○●□□○" +
				"○●○●○●□○" +
				"□○○○●○□○" +
				"□○○●●●○○" +
				"□□○●●○○○" +
				"□□●○○○○○" +
				"●●●○○□●●");
		// 全方位検索は残10未満で実施によるパフォーマンス⇒実践では残15未満で全方位検索実施の予定
		// １億回の呼び出しで 4,316ms (2018/07/27)
		// １億回の呼び出しで 2,303ms (2018/07/29)
		// １億回の呼び出しで 2,570ms (2018/07/30)･･･全方位検索追加
		// １億回の呼び出しで 3,993ms (2018/08/03)･･･いろいろ不具合修正
		// １億回の呼び出しで 3,125ms (2018/08/04)･･･根本的な問題があったため組み直し
		// １億回の呼び出しで 3,145ms (2018/09/03)･･･
		final long start = System.currentTimeMillis();
		for (int i = 0; i < 100000000; i++) {
			BitEval.evalFixedDisc(board);
		}
		final long end = System.currentTimeMillis();
		System.out.println((end - start) + "ms");
	}

	/**
	 * 性能テスト(BitEval.evalFixedDisc)
	 */
	@Test
	public void evalFixedDisc_002() {
		final BitBoard board = new BitBoard(
				"○○○○○○○○" +
				"○○○○○○○○" +
				"○○○○○○○○" +
				"●○□○●○□○" +
				"●○○●●●○○" +
				"●●○●●○○○" +
				"●●●○○○○○" +
				"●●●●●●●●");
		// １億回の呼び出しで 12,731ms (2018/07/30)
		// １億回の呼び出しで 6,961ms (2018/07/30)･･･全方位検索追加
		// １億回の呼び出しで 8,393ms (2018/08/03)･･･いろいろ不具合修正
		// １億回の呼び出しで 7,247ms (2018/08/04)･･･根本的な問題があったため組み直し
		// １億回の呼び出しで 7,189ms (2018/09/03)･･･
		final long start = System.currentTimeMillis();
		for (int i = 0; i < 100000000; i++) {
			BitEval.evalFixedDisc(board);
		}
		final long end = System.currentTimeMillis();
		System.out.println((end - start) + "ms");
	}

	/**
	 * 性能テスト(BitEval.evalLine)
	 */
	@Test
	public void evalLine_001() {
		final BitBoard board = new BitBoard(
				"□○○○○□□□" +
				"□□●●○○□□" +
				"○○●●○○●●" +
				"○●●○○○●●" +
				"○●○●○○●●" +
				"○○○○○●●●" +
				"○□●●●●□●" +
				"□●●●●●□□");

		final long[][] flips = BitEval.evalFlip(board);
		final long[][] mountains = BitEval.evalMountain(board);
		final long[][] wings = BitEval.evalWing(board, flips);
		final long[][] blocks = BitEval.evalBlock(board);
		final long[] fixed = BitEval.evalFixedDisc(board);
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		// １億回の呼び出しで 227,494ms (2018/09/03)･･･性能悪すぎ
		final long start = System.currentTimeMillis();
		for (int i = 0; i < 100000000; i++) {
			BitEval.evalLine(board, mountains, wings, blocks, fixed, cs, flips);
		}
		final long end = System.currentTimeMillis();
		System.out.println((end - start) + "ms");
	}

	/**
	 * flip
	 */
	@Test
	public void flip_001() {
		final BitBoard board = new BitBoard(
				"□□○○○●□□" +
				"□□●●●●□□" +
				"●□○●●●○●" +
				"□○○○●●●●" +
				"●○□○○●○●" +
				"□□○○●○○□" +
				"□□○○○○□□" +
				"□□●●●●□□");

		// １億回の呼び出しで 3,042ms (2018/07/31)
		// １億回の呼び出しで 2,075ms (2018/08/03)･･･無駄な判定削除
		// １億回の呼び出しで 2,112ms (2018/09/03)･･･
		final long start = System.currentTimeMillis();
		for (int i = 0; i < 100000000; i++) {
			board.flip(BitPoint.C5.mask, ReversiColor.BLACK.getValue());
		}
		final long end = System.currentTimeMillis();
		System.out.println((end - start) + "ms");
	}

	/**
	 * stddev
	 */
	@Test
	public void stddev_001() {
		final BitBoard board = new BitBoard(
				"□□○○○●□□" +
				"□□●●●●□□" +
				"●□○●●●○●" +
				"□○○○●●●●" +
				"●○□○○●○●" +
				"□□○○●○○□" +
				"□□○○○○□□" +
				"□□●●●●□□");
		final EvalResult evalBlack = new MiddleBitSimulator().eval(board, board, ReversiColor.BLACK.getValue(), ReversiColor.BLACK.getValue());
		final EvalResult evalWhite = new MiddleBitSimulator().eval(board, board, ReversiColor.WHITE.getValue(), ReversiColor.WHITE.getValue());
		final double[] evaluation = new double[] { evalBlack.getEvaluation(), evalWhite.getEvaluation() };

		// １億回の呼び出しで 5ms (2018/08/11)
		final long start = System.currentTimeMillis();
		for (int i = 0; i < 100000000; i++) {
			final double stddev = BitEval.stddev(evaluation, ReversiColor.BLACK.getValue());
		}
		final long end = System.currentTimeMillis();
		System.out.println((end - start) + "ms");
	}
}
