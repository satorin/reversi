package org.hn.sato.reversi.bit;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.util.Arrays;
import java.util.List;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ReversiPoint;
import org.hn.sato.reversi.ai.ArtificalIntelligence;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.ai.bitsimulator.BitEval;
import org.hn.sato.reversi.ai.bitsimulator.BitSimulator;
import org.hn.sato.reversi.ai.bitsimulator.MiddleBitSimulator;
import org.hn.sato.reversi.ai.bitsimulator.SBitMiniMaxAlphaBeta;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * {@link BitEval} のテスト
 *
 * @author s-kotaki
 *
 */
@RunWith(JUnit4.class)
public class TestBitEval {

	/**
	 * getFlipPointListOrderByPriority
	 */
	@Test
	public void getFlipPointListOrderByPriority_001() {
		final BitBoard board = new BitBoard(
				"□○●●□□□□" +
				"□□●○●□□□" +
				"□●○●○●□□" +
				"□○○○●○□□" +
				"□○○●●●○□" +
				"□□○●●○○□" +
				"□□●○○○□□" +
				"□□●○○○□□");
		final long[] points = BitBoardUtil.getFlipPointListOrderByPriority(board, ReversiColor.BLACK.getValue());
		final List<BitPoint> pointList = BitBoardUtil.convertToBitPointList(points);
		assertThat(pointList,
				is(Arrays.asList(BitPoint.A1, BitPoint.H5, BitPoint.G4, BitPoint.G3, BitPoint.A6, BitPoint.A5,
						BitPoint.A3, BitPoint.B6, BitPoint.A4, BitPoint.H6, BitPoint.G8, BitPoint.H7, BitPoint.G7,
						BitPoint.B7, BitPoint.B2)));
	}

	/**
	 * calcOpenRatio
	 */
	@Test
	public void calcOpenRatio_001() {
		final BitBoard board = new BitBoard(
				"□○●●□□□□" +
				"□□●○●□□□" +
				"□●○●○●□□" +
				"□○○○●○□□" +
				"□○○●●●○□" +
				"□□○●●○○□" +
				"□□●○○○□□" +
				"□□●○○○□□");
		final int openRatio = BitEval.calcOpenRatio(board, BitPoint.B6.mask, ReversiColor.BLACK.getValue());
		assertThat(openRatio, is(5));
	}

	/**
	 * calcOpenRatio
	 */
	@Test
	public void calcOpenRatio_002() {
		final BitBoard board = new BitBoard(
				"□○●●□□□□" +
				"●□●○●□□□" +
				"□○●●○●□□" +
				"□□○○●○□□" +
				"□□□○●●○□" +
				"□□○●○○○□" +
				"□□●○○○□□" +
				"□□●○○○□□");
		final int openRatio = BitEval.calcOpenRatio(board, BitPoint.G8.mask, ReversiColor.BLACK.getValue());
		assertThat(openRatio, is(7));
	}

	/**
	 * calcOpenRatioEx
	 */
	@Test
	public void calcOpenRatioEx_001() {
		final BitBoard board = new BitBoard(
				"□○●●□□□□" +
				"□□●○●□□□" +
				"□●○●○●□□" +
				"□○○○●○□□" +
				"□○○●●●○□" +
				"□□○●●○○□" +
				"□□●○○○□□" +
				"□□●○○○□□");
		final int openRatio = BitEval.calcOpenRatio(board, BitPoint.B6.mask, ReversiColor.BLACK.getValue());
		assertThat(openRatio, is(5));
	}

	/**
	 * calcOpenRatioEx
	 */
	@Test
	public void calcOpenRatioEx_002() {
		final BitBoard board = new BitBoard(
				"□○●●□□□□" +
				"●□●○●□□□" +
				"□○●●○●□□" +
				"□□○○●○□□" +
				"□□□○●●○□" +
				"□□○●○○○□" +
				"□□●○○○□□" +
				"□□●○○○□□");
		final int openRatio = BitEval.calcOpenRatioEx(board, BitPoint.G8.mask, ReversiColor.BLACK.getValue());
		assertThat(openRatio, is(5));
	}

	/**
	 * evalFixedDisc
	 */
	@Test
	public void evalFixedDisc_001() {
		final BitBoard board = new BitBoard(
				"○○●●□□□○" +
				"○○●○●□□○" +
				"○●○●○●□○" +
				"□○○○●○□○" +
				"□○○●●●○○" +
				"□□○●●○○○" +
				"□□●○○○○○" +
				"●●●○○□●●");
		final long[] fixed = BitEval.evalFixedDisc(board);
		assertThat(fixed[0],
				is(BitPoint.A8.mask | BitPoint.B8.mask | BitPoint.C8.mask | BitPoint.G8.mask | BitPoint.H8.mask));
		assertThat(fixed[1], is(BitPoint.A1.mask | BitPoint.A2.mask | BitPoint.A3.mask | BitPoint.B1.mask | BitPoint.B2.mask
				| BitPoint.H1.mask | BitPoint.H2.mask | BitPoint.H3.mask | BitPoint.H4.mask | BitPoint.H5.mask
				| BitPoint.H6.mask | BitPoint.H7.mask));
	}

	/**
	 * evalFixedDisc
	 */
	@Test
	public void evalFixedDisc_002() {
		final BitBoard board = new BitBoard(
				"□○○○○●●●" +
				"□□○●●●●●" +
				"□□○●●●○●" +
				"□○○●●●●●" +
				"□○○●○●○●" +
				"□□○○●○○□" +
				"□□○○○○○○" +
				"□□□●●●●□");
		final long[] fixed = BitEval.evalFixedDisc(board);

		// 実際にはG4の黒は確定石だが、これの判別はかなりコストがかかる？斜めの埋まり具合を見ないといけない
		assertThat(fixed[0],
				is(BitPoint.H1.mask | BitPoint.H2.mask | BitPoint.H3.mask | BitPoint.H4.mask | BitPoint.H5.mask
						| BitPoint.G1.mask | BitPoint.G2.mask | BitPoint.F1.mask));
		assertThat(fixed[1], is(0L));
	}

	/**
	 * evalFixedDisc
	 */
	@Test
	public void evalFixedDisc_003() {
		final BitBoard board = new BitBoard(
				"○○○○○○○○" +
				"○○○○○○○○" +
				"○○○○○○○○" +
				"●○□○●○□○" +
				"●○○●●●○○" +
				"●●○●●○○○" +
				"●●●○○○○○" +
				"●●●●●●●●");
		final long[] fixed = BitEval.evalFixedDisc(board);
		assertThat(fixed[0],
				is(BitPoint.A4.mask | BitPoint.A5.mask | BitPoint.A6.mask | BitPoint.A7.mask | BitPoint.A8.mask
						| BitPoint.B6.mask | BitPoint.B7.mask | BitPoint.B8.mask | BitPoint.C7.mask | BitPoint.C8.mask
						| BitPoint.D6.mask | BitPoint.D8.mask | BitPoint.E5.mask | BitPoint.E8.mask | BitPoint.F8.mask
						| BitPoint.G8.mask | BitPoint.H8.mask));
		assertThat(fixed[1],
				is(BitPoint.A1.mask | BitPoint.A2.mask | BitPoint.A3.mask | BitPoint.B1.mask | BitPoint.B2.mask
						| BitPoint.B3.mask | BitPoint.C1.mask | BitPoint.C2.mask | BitPoint.C3.mask | BitPoint.D1.mask
						| BitPoint.D2.mask | BitPoint.D3.mask | BitPoint.E1.mask | BitPoint.E2.mask | BitPoint.E3.mask
						| BitPoint.E7.mask | BitPoint.F1.mask | BitPoint.F2.mask | BitPoint.F3.mask | BitPoint.F6.mask
						| BitPoint.G1.mask | BitPoint.G2.mask | BitPoint.G3.mask | BitPoint.H1.mask | BitPoint.H2.mask
						| BitPoint.H3.mask | BitPoint.H4.mask | BitPoint.H5.mask | BitPoint.H6.mask
						| BitPoint.H7.mask));
	}

	/**
	 * evalFixedDisc
	 */
	@Test
	public void evalFixedDisc_004() {
		final BitBoard board = new BitBoard(
				"○●●○○○○□" +
				"●●●○○○□○" +
				"●●○●○○○●" +
				"●●●○●○○●" +
				"●●●●○●○□" +
				"●●●○●●○○" +
				"●●●●●●□□" +
				"●●●●●●●□");
		final long[] fixed = BitEval.evalFixedDisc(board);
		assertThat(fixed[0],
				is(BitPoint.A2.mask | BitPoint.A3.mask | BitPoint.A4.mask | BitPoint.A5.mask | BitPoint.A6.mask | BitPoint.A7.mask | BitPoint.A8.mask
						| BitPoint.B3.mask | BitPoint.B4.mask | BitPoint.B5.mask | BitPoint.B6.mask | BitPoint.B7.mask | BitPoint.B8.mask
						| BitPoint.C4.mask | BitPoint.C5.mask | BitPoint.C6.mask | BitPoint.C7.mask | BitPoint.C8.mask
						| BitPoint.D7.mask | BitPoint.D8.mask | BitPoint.E6.mask | BitPoint.E7.mask | BitPoint.E8.mask
						| BitPoint.F7.mask | BitPoint.F8.mask | BitPoint.G8.mask));
		assertThat(fixed[1],
				is(BitPoint.A1.mask | BitPoint.D6.mask | BitPoint.E3.mask | BitPoint.F4.mask));
	}

	/**
	 * evalXLine
	 */
	@Test
	public void evalXLine_001() {
		final BitBoard board = new BitBoard(
				"□○●●□□□○" +
				"○□●○●□□○" +
				"○●●●○●□○" +
				"□○○●●○□○" +
				"□○○○●●○○" +
				"□□●●●●○○" +
				"□□●○○○○○" +
				"□●●○○□●●");
		final int[] xLines = BitEval.evalXLine(board);
		assertThat(xLines[0], is(1));
		assertThat(xLines[1], is(0));
	}

	/**
	 * evalXLine
	 */
	@Test
	public void evalXLine_002() {
		final BitBoard board = new BitBoard(
				"□□□□□□□□" +
				"□□□□●○□□" +
				"□○○●●●○○" +
				"□●●●●○●●" +
				"□●○○○○●□" +
				"□●●○○○●□" +
				"□□○○○○□□" +
				"□○○○○○□□");
		final int[] xLines = BitEval.evalXLine(board);
		assertThat(xLines[0], is(0));
		assertThat(xLines[1], is(0));
	}

	/**
	 * evalCLine
	 */
	@Test
	public void evalCLine_001() {
		final BitBoard board = new BitBoard(
				"□□□●□□□□" +
				"□□●○●○□□" +
				"●●●●○●●□" +
				"●○●○●●○○" +
				"□○○●●●○○" +
				"□○●●●●●□" +
				"□□●●○●□□" +
				"□□□○○□□□");
		final int[] cLines = BitEval.evalCLine(board);
		assertThat(cLines[0], is(3));
		assertThat(cLines[1], is(1));
	}

	/**
	 * evalFlip
	 */
	@Test
	public void evalFlip_001() {
		final BitBoard board = new BitBoard(
				"□□□●□□□□" +
				"□□●○●□□○" +
				"●●●●○●●○" +
				"●○●○●●○○" +
				"□○○●○●○□" +
				"□□●●○●●●" +
				"□□●●○●□□" +
				"□□□○○□●□");
		final long[][] flips = BitEval.evalFlip(board);
		// 黒の純粋手数
		assertThat(flips[0][0], is(BitPoint.A5.mask | BitPoint.A6.mask | BitPoint.B6.mask | BitPoint.C1.mask
				| BitPoint.E1.mask | BitPoint.F8.mask | BitPoint.H5.mask));
		// 黒の手数(悪手排除)
		assertThat(flips[0][1], is(BitPoint.A5.mask | BitPoint.A6.mask | BitPoint.B6.mask | BitPoint.C1.mask
				| BitPoint.E1.mask | BitPoint.H5.mask));
		// 黒の余裕手
		assertThat(flips[0][2], is(BitPoint.A5.mask | BitPoint.A6.mask | BitPoint.H5.mask));
		// 白の純粋手数
		assertThat(flips[1][0],
				is(BitPoint.A2.mask | BitPoint.B2.mask | BitPoint.B6.mask | BitPoint.B7.mask | BitPoint.B8.mask
						| BitPoint.C1.mask | BitPoint.C8.mask
						| BitPoint.E1.mask | BitPoint.F1.mask | BitPoint.F2.mask | BitPoint.G2.mask | BitPoint.G7.mask
						| BitPoint.H5.mask));
		// 白の手数(悪手排除)
		assertThat(flips[1][1], is(BitPoint.B6.mask | BitPoint.B8.mask | BitPoint.C1.mask | BitPoint.C8.mask
				| BitPoint.E1.mask | BitPoint.F1.mask | BitPoint.F2.mask));
		// 白の余裕手
		assertThat(flips[1][2], is(BitPoint.B8.mask | BitPoint.C8.mask | BitPoint.F1.mask));
	}

	/**
	 * evalCorner
	 */
	@Test
	public void evalCorner_001() {
		final BitBoard board = new BitBoard(
				"○□□●□□□○" +
				"□○●○●□●□" +
				"●●●●○●●□" +
				"●○●●●●○○" +
				"□○○●●●○○" +
				"□□●●●●●□" +
				"□●●●○●○□" +
				"●□□○○□□□");
		final long[] corners = BitEval.evalCorner(board);
		assertThat(Long.bitCount(corners[0]), is(1));
		assertThat(Long.bitCount(corners[1]), is(2));
	}

	/**
	 * evalX
	 */
	@Test
	public void evalX_001() {
		final BitBoard board = new BitBoard(
				"□□□●□□□□" +
				"□○●○●□●□" +
				"●●●●○●●□" +
				"●○●●●●○○" +
				"□○○●●●○○" +
				"●□●●●●●□" +
				"●●●●○●○○" +
				"●●□○○○○○");
		final long[] fixed = BitEval.evalFixedDisc(board);
		final long[] xs = BitEval.evalX(board, fixed);
		assertThat(Long.bitCount(xs[0]), is(1));
		assertThat(Long.bitCount(xs[1]), is(1));
	}

	/**
	 * evalX
	 */
	@Test
	public void evalX_002() {
		final BitBoard board = new BitBoard(
				"□□□□□□□○" +
				"□□□□●●○○" +
				"□○●●●●●○" +
				"●●●●○●●○" +
				"○○○○○○○○" +
				"●○●○●○●○" +
				"□□○●●●●●" +
				"□○○○○○○○");
		final long[] fixed = BitEval.evalFixedDisc(board);
		final long[] xs = BitEval.evalX(board, fixed);
		assertThat(Long.bitCount(xs[0]), is(0));
		assertThat(Long.bitCount(xs[1]), is(0));
	}

	/**
	 * evalC
	 */
	@Test
	public void evalC_001() {
		final BitBoard board = new BitBoard(
				"□●●●●□□□" +
				"●□●○●□□○" +
				"●●●●○●●○" +
				"●○●●●●○○" +
				"●○○●●●○○" +
				"●●●●●●●□" +
				"●□●●○●○○" +
				"□○○○○○○○");
		final long[][] flips = BitEval.evalFlip(board);
		final long[] fixed = BitEval.evalFixedDisc(board);
		final long[][] mountains = BitEval.evalMountain(board);
		final long[][] wings = BitEval.evalWing(board, flips);
		final long[][] blocks = BitEval.evalBlock(board);
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		assertThat(Long.bitCount(cs[0][0]), is(1));
		assertThat(Long.bitCount(cs[0][1]), is(0));
		assertThat(Long.bitCount(cs[1][0]), is(1));
		assertThat(Long.bitCount(cs[1][1]), is(0));
	}

	/**
	 * evalC
	 */
	@Test
	public void evalC_002() {
		final BitBoard board = new BitBoard(
				"□□□□□□□□" +
				"□□□○●□□□" +
				"□○○○○●□□" +
				"□□○○○○●□" +
				"□□●○●●○□" +
				"□□●●●○○○" +
				"□□●○○○○□" +
				"□●●●●●●□");
		final long[][] flips = BitEval.evalFlip(board);
		final long[] fixed = BitEval.evalFixedDisc(board);
		final long[][] mountains = BitEval.evalMountain(board);
		final long[][] wings = BitEval.evalWing(board, flips);
		final long[][] blocks = BitEval.evalBlock(board);
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		assertThat(Long.bitCount(cs[0][0]), is(0));
		assertThat(Long.bitCount(cs[0][1]), is(0));
		assertThat(Long.bitCount(cs[1][0]), is(0));
		assertThat(Long.bitCount(cs[1][1]), is(0));
	}

	/**
	 * evalWall
	 */
	@Test
	public void evalWall_001() {
		final BitBoard board = new BitBoard(
				"□□□□□□□□" +
				"□□□□●○□□" +
				"□□○○●●○●" +
				"□□□○●●○○" +
				"□□□●●●○□" +
				"□□□□□□□□" +
				"□□□□□□□□" +
				"□□□□□□□□");
		final int[] wall = BitEval.evalWall(board);
		assertThat(wall[0], is(11));
		assertThat(wall[1], is(4));
	}

	/**
	 * evalLine
	 */
	@Test
	public void evalLine_001() {
		final BitBoard board = new BitBoard(
				"□□□●□○□○" +
				"○□●●●●○●" +
				"○○●○○○●□" +
				"○○○○○○●□" +
				"□○●○●●●●" +
				"□○○●●○●●" +
				"□□●●●●□●" +
				"□□●●●●●□");
		final long[][] flips = BitEval.evalFlip(board);
		// 確定石の評価
		final long[] fixed = BitEval.evalFixedDisc(board);
		// 山の評価
		final long[][] mountains = BitEval.evalMountain(board);
		// 翼の評価
		final long[][] wings = BitEval.evalWing(board, flips);
		// ブロックの評価
		final long[][] blocks = BitEval.evalBlock(board);
		// Ｃの評価
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		// 辺の総合評価
		final int[][] lines = BitEval.evalLine(board, mountains, wings, blocks, fixed, cs, flips);
		assertThat(lines[0][0], is(10));
		assertThat(lines[0][1], is(-15));
		assertThat(lines[1][0], is(4));
		assertThat(lines[1][1], is(0));
	}

	/**
	 * evalLine
	 */
	@Test
	public void evalLine_002() {
		final BitBoard board = new BitBoard(
				"□□□□□□□□" +
				"□□○○○□□●" +
				"□□●○○●●●" +
				"□□○○○○●●" +
				"□□○○○●○□" +
				"□□○○○○○○" +
				"□□□●●●□□" +
				"□□□●●●□□");
		final long[][] flips = BitEval.evalFlip(board);
		// 確定石の評価
		final long[] fixed = BitEval.evalFixedDisc(board);
		// 山の評価
		final long[][] mountains = BitEval.evalMountain(board);
		// 翼の評価
		final long[][] wings = BitEval.evalWing(board, flips);
		// ブロックの評価
		final long[][] blocks = BitEval.evalBlock(board);
		// Ｃの評価
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		// 辺の総合評価
		final int[][] lines = BitEval.evalLine(board, mountains, wings, blocks, fixed, cs, flips);
		assertThat(lines[0][0], is(6));
		assertThat(lines[0][1], is(0));
		assertThat(lines[1][0], is(1));
		assertThat(lines[1][1], is(0));
	}

	/**
	 * evalLine
	 */
	@Test
	public void evalLine_003() {
		final BitBoard board = new BitBoard(
				"○□○□●○□□" +
				"□○○●●○□□" +
				"□●●○●○○○" +
				"□●○○●○○○" +
				"□●●○○○○○" +
				"□●○●○○●○" +
				"□□○○●●●□" +
				"□○○○○○●○");
		final long[][] flips = BitEval.evalFlip(board);
		// 確定石の評価
		final long[] fixed = BitEval.evalFixedDisc(board);
		// 山の評価
		final long[][] mountains = BitEval.evalMountain(board);
		// 翼の評価
		final long[][] wings = BitEval.evalWing(board, flips);
		// ブロックの評価
		final long[][] blocks = BitEval.evalBlock(board);
		// Ｃの評価
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		// 辺の総合評価
		final int[][] lines = BitEval.evalLine(board, mountains, wings, blocks, fixed, cs, flips);
		assertThat(lines[0][0], is(2));
		assertThat(lines[0][1], is(-15));
		assertThat(lines[1][0], is(11));
		assertThat(lines[1][1], is(-15));
	}

	/**
	 * evalOpen
	 */
	@Test
	public void evalOpen_001() {
		final BitBoard board = new BitBoard(
				"□□□□□□□□" +
				"□□□□□□□□" +
				"□□□●○○●○" +
				"□□□●●●●●" +
				"□□○●○○●□" +
				"□□○○○●●□" +
				"□□□□○●□□" +
				"□□□□○●□□");
		final long[] evaluation = BitEval.evalOpen(board);
		assertThat(Long.bitCount(evaluation[0]), is(11));
		assertThat(Long.bitCount(evaluation[1]), is(11));
	}

	/**
	 * evalSurrounded
	 */
	@Test
	public void evalSurrounded_001() {
		final BitBoard board = new BitBoard(
				"□□□□□□□□" +
				"□□□□□□□□" +
				"□□□●○○●○" +
				"□□□●●●●●" +
				"□□○●○○●□" +
				"□□○○○●●□" +
				"□□□□○●□□" +
				"□□□□○●□□");
		final long[] evaluation = BitEval.evalSurrounded(board);
		assertThat(Long.bitCount(evaluation[0]), is(4));
		assertThat(Long.bitCount(evaluation[1]), is(5));
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_001() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□□□□□□" +
				"□□□□□□□□" +
				"□□□●○□●○" +
				"□□□●●●●●" +
				"□□○●○○●□" +
				"□□○○○●●□" +
				"□□□□○●□□" +
				"□□□□○●□□");
		// 白番(MiddleSimulator)
		// WZebra(24)の評価は C4=+3.78, F3=+0.34, C2=-2.13
		final EvalResult result = ai.execute(board, ReversiColor.WHITE, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.C4, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_002() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□□□□□□" +
				"□□○●□□□□" +
				"□□□●□□□□" +
				"□□□●○○□□" +
				"□□□●○□□□" +
				"□□□□○□□□" +
				"□□□□○□□□" +
				"□□□□□□□□");
		// 黒番
		// WZebra(Book)の評価は F6=+2.91, F3=+2.21, F7=-1.64
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.F6, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_003() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□○○●□□" +
				"□□○○○●□□" +
				"□□○●●○□□" +
				"□□●●○○○□" +
				"□□□●○○○□" +
				"□□□●●○○□" +
				"□□□●○○□□" +
				"□□□□○□□□");
		// 黒番
		// WZebra(24)の評価は H5=+6.61, B3=+6.55, G3=+5.99, H4=+5.67, H3=+5.43, C1=+3.75
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.H5, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_004() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□○□□□□" +
				"□□○○○●□□" +
				"□□○○●●□□" +
				"□○○●○●□□" +
				"□○○○●●●□" +
				"□□○○○●○□" +
				"□□□□●●□□" +
				"□□□□●□□□");
		// 黒番
		// WZebra(24)の評価は H5=+17.17, H6=+10.16, D7=+7.44, E1=+5.59, C7=+3.74
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.H5, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_005() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□○○□□□" +
				"□□○○○○□□" +
				"□□○○○○○●" +
				"□○○○○○●●" +
				"□○○○○○●●" +
				"□□○○○●○□" +
				"□□□□●●□○" +
				"□□□●●●●□");
		// 黒番
		// WZebra(Exact)の評価は H6=+52, D7=+50, F1=+46, C1=+42
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.H6, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_006() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□○□□□□" +
				"□□○○○●□□" +
				"□□○○●●□□" +
				"□□□○○●□□" +
				"□□□●○●□□" +
				"□□□□○●□□" +
				"□□□□○□□□" +
				"□□□□□□□□");
		// 黒番(BeginningSimulator)
		// WZebra(24)の評価は C4=+8.09, E1=+4.43, C5=+2.43, F7=+2.19
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.C4, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_007() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□□□●□□" +
				"□□□□●●□□" +
				"□□●●○○○○" +
				"□□○○○○○□" +
				"□□□○○○□□" +
				"□□□□○○□□" +
				"□□□□○□□□" +
				"□□□□□□□□");
		// 黒番(BeginningSimulator)
		// WZebra(24)の評価は C5=+5.03, H5=+5.02, B5=+4.90, G6=+2.80
		// 個人的には右の壁を破るより左側に引っ張ったほうが(C5, B5)のほうが好手と思う
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.C5, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_008() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□□●□□□" +
				"□□○○●●□●" +
				"□●○○●●●●" +
				"□○○●○●●●" +
				"□○○●●○●●" +
				"□○○●●●○●" +
				"□□●●●●○●" +
				"□□□□●○○●");
		// 黒番(MiddleSimulator)
		// WZebra(Exact)の評価は A3=0, A4=-2, B1=-4, D1=-4, A6=-4
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.A3, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_009() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□●●□□□" +
				"□□●●●□□□" +
				"○○●●●●●●" +
				"□●○●○○●●" +
				"●●●○○○□●" +
				"□●○○○□□□" +
				"□□□□□□□□" +
				"□□□□□□□□");
		// 白番(MiddleSimulator)
		// WZebra(24)の評価は A4=+9.75, F2=+7.70, F1=+5.22, C1=+5.11
		final EvalResult result = ai.execute(board, ReversiColor.WHITE, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.A4, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_010() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□○○●□□" +
				"□□○○●●□□" +
				"□○○●●●□□" +
				"○○●●○●□□" +
				"○○○○○●□□" +
				"○○○○●●□□" +
				"□□□□○●□□" +
				"□□□□□□□□");
		// 黒番(MiddleSimulator)
		// WZebra(24)の評価は E8=+5.80, D8=+3.65, C1=-1.09, C7=-1.23, A3=-2.86
		final EvalResult result = ai.execute(board, ReversiColor.BLACK, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.E8, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_011() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□□□□□□" +
				"□□□□□□□□" +
				"□□□●○□□□" +
				"□□□●●○□□" +
				"□□○●●●●□" +
				"□□○○●●●□" +
				"□□□□●○□□" +
				"□□□□□□□□");
		// 白番(BeginningSimulator)
		// WZebra(Book)の評価は F8=+0.00, E8=+0.00, C3=-0.76, D7=-1.43, C4=-1.97
		final EvalResult result = ai.execute(board, ReversiColor.WHITE, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.F8, result.getPoint());
	}

	/**
	 * 着手位置テスト
	 *
	 * @throws InterruptedException
	 */
	@Test
	public void evalBestFlip_012() throws InterruptedException {
		final ArtificalIntelligence ai = new SBitMiniMaxAlphaBeta();
		final ReversiBoard board = new ReversiBoard(
				"□□□□□□□□" +
				"□□□□●□□□" +
				"□□□●●□□□" +
				"□□□●●○□□" +
				"□□○●●●●□" +
				"□□○○●●●□" +
				"□□□□○○□□" +
				"□□□□□○□□");
		// 白番(BeginningSimulator)
		// WZebra(B24)の評価は F3=+0.69(Book), H6=-3.60(Book), C4=-5.13, H4=-5.37, E1=-8.26
		final EvalResult result = ai.execute(board, ReversiColor.WHITE, 10, 18);
		System.out.println(result);
		System.out.println(result.getEvaluation());
		Assert.assertEquals(ReversiPoint.F3, result.getPoint());
	}

	/**
	 * 異なる２つの局面が同一評価になってしまう問題を解決する
	 */
	@Test
	public void evalTotal_001() {
		final BitSimulator simulator = new MiddleBitSimulator();
		final BitBoard board1 = new BitBoard(
				"□□□○○○○□" +
				"□□○○○○□□" +
				"□□○○●○○□" +
				"□○○○●○□●" +
				"□○○○○○●●" +
				"□□○○●●●●" +
				"□□●○●●□□" +
				"□□□○●□□□");
		final EvalResult result1 = simulator.eval(board1, board1, ReversiColor.BLACK.getValue(), ReversiColor.BLACK.getValue());
		System.out.println(result1);
		System.out.println(result1.getEvaluation());
		final BitBoard board2 = new BitBoard(
				"□□□○○○○□" +
				"□□○○○○□□" +
				"○○○○○○□●" +
				"□○●○○○●□" +
				"□○○●●○●●" +
				"□□○○●○●□" +
				"□□□●○○□□" +
				"□□□□●○□□");
		final EvalResult result2 = simulator.eval(board2, board2, ReversiColor.BLACK.getValue(), ReversiColor.BLACK.getValue());
		System.out.println(result2);
		System.out.println(result2.getEvaluation());
		Assert.assertNotEquals(result1.getEvaluation(), result2.getEvaluation());
	}
}
