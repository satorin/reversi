package org.hn.sato.reversi;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

/**
 * リバーシの色
 *
 * @author s-kotaki
 *
 */
public enum ReversiColor {

	/** 黒 */
	BLACK(1, "黒", "●"),
	/** 白 */
	WHITE(-1, "白", "○"),
	/** 空 */
	NONE(0, "空", "□");

	/** キャッシュ */
	private static Map<Integer, ReversiColor> cache = new HashMap<>();

	static {
		Arrays.stream(ReversiColor.values()).forEach(c -> cache.put(c.value, c));
	}

	/** 値 */
	@Getter
	private final int value;
	/** 表示名 */
	@Getter
	private final String displayName;
	/** シンボル */
	@Getter
	private final String symbol;

	/**
	 * コンストラクタ
	 *
	 * @param value 値
	 * @param displayName 表示名
	 * @param symbol シンボル
	 */
	ReversiColor(int value, String displayName, String symbol) {
		this.value = value;
		this.displayName = displayName;
		this.symbol = symbol;
	}

	/**
	 * 黒の場合白、白の場合黒を返却する
	 *
	 * @return 反対の色
	 */
	public ReversiColor opponent() {
		return ReversiColor.fromValue(~this.value + 1);
	}

	/**
	 * 値からリバーシの色を返却する
	 *
	 * @param value 値
	 * @return リバーシの色
	 */
	public static ReversiColor fromValue(int value) {
		if (cache.containsKey(value)) {
			return cache.get(value);
		}
		throw new IllegalArgumentException("無効な色が指定されました。" + value);
	}

	@Override
	public String toString() {
		return this.symbol;
	}
}
