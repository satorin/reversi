package org.hn.sato.reversi.ai.simulator;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.EvalResult;

/**
 * 序盤の戦略シミュレーター
 *
 * @author s-kotaki
 *
 */
public class BeginningSimulator extends AbstractEvalSimulator {

	/** 確定石評価値 */
	private static final double VAL_FIXED = 10;
	/** 自石評価値 */
	private static final double VAL_DISC = -3.2;
	/** 隅評価値 */
	private static final double VAL_CORNER = 50;
	/** Ⅹ評価値 */
	private static final double VAL_X = -36.7;
	/** Ｃ評価値 */
	private static final double VAL_C = -6.4;
	/** 山評価値 */
	private static final double VAL_YAMA = 36.7;
	/** 翼評価値 */
	private static final double VAL_TSUBASA = -15;
	/** 包囲評価値 */
	private static final double VAL_SURROUNDED = -1.8;
	/** 手数評価値 */
	private static final double VAL_MOVABLE = 1.2;

	@Override
	public final EvalResult eval(ReversiBoard board, int baseColor) {

		final SimulatorEvalResult result = new SimulatorEvalResult(board.clone(), baseColor);

		final boolean[][] blackFixed = new boolean[ReversiBoard.SIZE][ReversiBoard.SIZE];

		if (baseColor == ReversiColor.BLACK.getValue()) {
			// 黒の隅取得可否
			result.enableGetCorner[0] = SimulatorEval.evalEnableGetCorner(board, ReversiColor.BLACK.getValue()) * VAL_CORNER
					* 0.75;
		}
		// 黒の確定石の評価
		result.fixed[0] = SimulatorEval.evalFixedDisc(board, ReversiColor.BLACK.getValue(), blackFixed) * VAL_FIXED;
		// 黒の数
		result.disc[0] = board.count(ReversiColor.BLACK.getValue()) * VAL_DISC;
		// 黒の隅の評価
		result.corner[0] = SimulatorEval.evalCorner(board, ReversiColor.BLACK.getValue()) * VAL_CORNER;
		// 黒のⅩの評価
		result.x[0] = SimulatorEval.evalX(board, ReversiColor.BLACK.getValue(), blackFixed) * VAL_X;
		// 黒のＣの評価
		result.c[0] = SimulatorEval.evalC(board, ReversiColor.BLACK.getValue(), blackFixed) * VAL_C;
		// 黒の山の評価
		result.mountain[0] = SimulatorEval.evalMountain(board, ReversiColor.BLACK.getValue()) * VAL_YAMA;
		// 黒の翼の評価
		result.wing[0] = SimulatorEval.evalWing(board, ReversiColor.BLACK.getValue()) * VAL_TSUBASA;
		// 黒の包囲評価
		result.surrounded[0] = SimulatorEval.evalSurrounded(board, ReversiColor.BLACK.getValue()) * VAL_SURROUNDED;
		// 黒の手数の評価
		result.flip[0] = SimulatorEval.evalFlipCount(board, ReversiColor.BLACK.getValue()) * VAL_MOVABLE;

		final boolean[][] whiteFixed = new boolean[ReversiBoard.SIZE][ReversiBoard.SIZE];

		if (baseColor == ReversiColor.WHITE.getValue()) {
			// 白の隅取得可否
			result.enableGetCorner[1] = SimulatorEval.evalEnableGetCorner(board, ReversiColor.WHITE.getValue()) * VAL_CORNER
					* 0.75;
		}
		// 白の確定石の評価
		result.fixed[1] = SimulatorEval.evalFixedDisc(board, ReversiColor.WHITE.getValue(), whiteFixed) * VAL_FIXED;
		// 白の数
		result.disc[1] = board.count(ReversiColor.WHITE.getValue()) * VAL_DISC;
		// 白の隅の評価
		result.corner[1] = SimulatorEval.evalCorner(board, ReversiColor.WHITE.getValue()) * VAL_CORNER;
		// 白のⅩの評価
		result.x[1] = SimulatorEval.evalX(board, ReversiColor.WHITE.getValue(), whiteFixed) * VAL_X;
		// 白のＣの評価
		result.c[1] = SimulatorEval.evalC(board, ReversiColor.WHITE.getValue(), whiteFixed) * VAL_C;
		// 白の山の評価
		result.mountain[1] = SimulatorEval.evalMountain(board, ReversiColor.WHITE.getValue()) * VAL_YAMA;
		// 白の翼の評価
		result.wing[1] = SimulatorEval.evalWing(board, ReversiColor.WHITE.getValue()) * VAL_TSUBASA;
		// 白の包囲評価
		result.surrounded[1] = SimulatorEval.evalSurrounded(board, ReversiColor.WHITE.getValue()) * VAL_SURROUNDED;
		// 白の手数の評価
		result.flip[1] = SimulatorEval.evalFlipCount(board, ReversiColor.WHITE.getValue()) * VAL_MOVABLE;

		final double black = result.enableGetCorner[0] + result.fixed[0] + result.disc[0] + result.corner[0] + result.x[0] + result.c[0]
				+ result.mountain[0] + result.wing[0] + result.surrounded[0] + result.flip[0];
		final double white = result.enableGetCorner[1] + result.fixed[1] + result.disc[1] + result.corner[1] + result.x[1] + result.c[1]
				+ result.mountain[1] + result.wing[1] + result.surrounded[1] + result.flip[1];
		result.setEvaluation((baseColor == ReversiColor.BLACK.getValue()) ? black - white : white - black);

		return result;
	}
}
