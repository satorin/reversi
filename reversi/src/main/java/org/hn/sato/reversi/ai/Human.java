package org.hn.sato.reversi.ai;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiClickEvent;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ReversiPoint;

import lombok.Getter;

/**
 * 人間
 *
 * @author s-kotaki
 *
 */
public class Human implements ArtificalIntelligence {

	/** クリックイベント */
	private ReversiClickEvent event;
	@Getter
	private long evalCount;
	@Getter
	private long alphaCutCount;
	@Getter
	private long betaCutCount;

	/**
	 * コンストラクタ
	 */
	public Human() {
	}

	/**
	 * イベントを設定する
	 *
	 * @param event イベント
	 */
	public void setEvent(ReversiClickEvent event) {
		this.event = event;
		this.event.setIntelligence(this);
	}

	@Override
	public EvalResult execute(ReversiBoard board, ReversiColor color, int depth, int finalDepth) throws InterruptedException {
		while (this.event.getPoint() == null) {
			synchronized (this) {
				wait();
			}
		}
		final ReversiPoint point = this.event.getPoint();
		this.event.setPoint(null);
		return new EmptyEvalResult(point);
	}

	@Override
	public void resetEvaluation() {
	}

	/**
	 * 空の評価結果
	 *
	 * @author s-kotaki
	 *
	 */
	private static class EmptyEvalResult implements EvalResult {

		/** 着手位置 */
		private ReversiPoint point;

		/**
		 * コンストラクタ
		 *
		 * @param point 着手位置
		 */
		private EmptyEvalResult(ReversiPoint point) {
			this.point = point;
		}

		@Override
		public int getColor() {
			return 0;
		}
		@Override
		public ReversiPoint getPoint() {
			return this.point;
		}
		@Override
		public void setPoint(ReversiPoint point) {
		}
		@Override
		public void setEvaluation(double evaluation) {
		}
		@Override
		public double getEvaluation() {
			return 0;
		}
	}
}
