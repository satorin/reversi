package org.hn.sato.reversi;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * 盤面設定イベント
 *
 * @author s-kotaki
 *
 */
public class ReversiSettingEvent implements EventHandler<MouseEvent> {

	/** リバーシメイン */
	private Reversi reversi;

	/**
	 * コンストラクタ
	 *
	 * @param reversi リバーシメイン
	 */
	public ReversiSettingEvent(Reversi reversi) {
		this.reversi = reversi;
	}

	@Override
	public void handle(MouseEvent event) {
		final ReversiPoint point = this.reversi.calcPoint(event.getSceneX(), event.getSceneY());
		this.reversi.board.put(point, this.reversi.getBoardSettingColor().getValue());
		this.reversi.repaintBoard();
	}
}
