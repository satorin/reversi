package org.hn.sato.reversi.ai.common;

import org.hn.sato.reversi.ReversiAbstractBoard;
import org.hn.sato.reversi.ReversiPoint;
import org.hn.sato.reversi.ai.EvalResult;

import lombok.Getter;
import lombok.Setter;

/**
 * 評価結果抽象クラス
 *
 * @author s-kotaki
 *
 */
public class AbstractEvalResult implements EvalResult {

	/** [0]=黒の評価、[1]=白の評価 */
	public double[] enableGetCorner = new double[2];
	/** 確定石評価値 */
	public double[] fixed = new double[2];
	/** 余裕手の評価値 */
	public double[] advantage = new double[2];
	/** 石の数 */
	public double[] disc = new double[2];
	/** 角評価値 */
	public double[] corner = new double[2];
	/** Ⅹ評価値 */
	public double[] x = new double[2];
	/** Ｃ評価値 */
	public double[] c = new double[2];
	/** 山評価値 */
	public double[] mountain = new double[2];
	/** 翼評価値 */
	public double[] wing = new double[2];
	/** ブロック評価値 */
	public double[] block = new double[2];
	/** 壁評価値 */
	public double[] wall = new double[2];
	/** 開放度評価値 */
	public double[] open = new double[2];
	/** 包囲評価値 */
	public double[] surrounded = new double[2];
	/** 手数評価値 */
	public double[] flip = new double[2];
	/** 辺総合評価値 */
	public double[] line = new double[2];

	/** 盤面 */
	private ReversiAbstractBoard board;
	@Getter
	private int color;
	@Setter
	@Getter
	private ReversiPoint point;
	@Setter
	@Getter
	private double evaluation;

	/**
	 * コンストラクタ
	 *
	 * @param board 盤面
	 * @param color 基準色
	 */
	public AbstractEvalResult(ReversiAbstractBoard board, int color) {
		this.board = board;
		this.color = color;
	}

	/**
	 * 評価の合計値を返却する
	 *
	 * @return 評価の合計値 [0]:黒 [1]:白
	 */
	public double[] calcEvaluation() {
		final double black = enableGetCorner[0] + fixed[0] + advantage[0] + disc[0] + corner[0]
				+ x[0] + c[0] + mountain[0] + wing[0] + block[0] + wall[0] + open[0] + surrounded[0] + flip[0] + line[0];
		final double white = enableGetCorner[1] + fixed[1] + advantage[1] + disc[1] + corner[1]
				+ x[1] + c[1] + mountain[1] + wing[1] + block[1] + wall[1] + open[1] + surrounded[1] + flip[1] + line[1];
		return new double[] { black, white };
	}

	@Override
	public String toString() {
		final double[] evaluation = this.calcEvaluation();
		final StringBuilder sb = new StringBuilder();
		sb.append(this.board);
		sb.append("\n");
		sb.append("隅取得可 " + enableGetCorner[0] + " : " + enableGetCorner[1]);
		sb.append("\n");
		sb.append("確定石   " + fixed[0] + " : " + fixed[1]);
		sb.append("\n");
		sb.append("余裕手   " + advantage[0] + " : " + advantage[1]);
		sb.append("\n");
		sb.append("自石数   " + disc[0] + " : " + disc[1]);
		sb.append("\n");
		sb.append("隅       " + corner[0] + " : " + corner[1]);
		sb.append("\n");
		sb.append("Ⅹ       " + x[0] + " : " + x[1]);
		sb.append("\n");
		sb.append("Ｃ       " + c[0] + " : " + c[1]);
		sb.append("\n");
		sb.append("山       " + mountain[0] + " : " + mountain[1]);
		sb.append("\n");
		sb.append("翼       " + wing[0] + " : " + wing[1]);
		sb.append("\n");
		sb.append("ブロック " + block[0] + " : " + block[1]);
		sb.append("\n");
		sb.append("開放度   " + open[0] + " : " + open[1]);
		sb.append("\n");
		sb.append("包囲     " + surrounded[0] + " : " + surrounded[1]);
		sb.append("\n");
		sb.append("手数     " + flip[0] + " : " + flip[1]);
		sb.append("\n");
		sb.append("辺の形   " + line[0] + " : " + line[1]);
		sb.append("\n");
		sb.append("総合評価 " + evaluation[0] + " : " + evaluation[1]);
		return sb.toString();
	}
}
