package org.hn.sato.reversi;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.StringUtils;
import org.hn.sato.reversi.ai.ArtificalIntelligence;
import org.hn.sato.reversi.ai.Human;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitMask;
import org.hn.sato.reversi.bit.BitPoint;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * Reversiのメインクラス
 *
 * @author s-kotaki
 *
 */
public class Reversi extends Application {
	/** フロントパネルの余白 */
	public static final int MARGIN = 25;
	/** コマの描画サイズ */
	public static final int RECTANGLE = 50;
	/** 石の描画サイズ */
	public static final int ELLIPSE_IMAGE_SIZE = 20;
	/** 盤面 */
	public ReversiBoard board = new ReversiBoard();
	/** AIマップ */
	public Map<String, Class<? extends ArtificalIntelligence>> aiMap = new TreeMap<>();
	/** プレイヤーマップ */
	public Map<ReversiColor, ArtificalIntelligence> players = new HashMap<>();
	/** クリックイベント(先手) */
	public ReversiClickEvent clickEventForBlack = new ReversiClickEvent(this, ReversiColor.BLACK);
	/** クリックイベント(後手) */
	public ReversiClickEvent clickEventForWhite = new ReversiClickEvent(this, ReversiColor.WHITE);
	/** 盤面設定クリックイベント */
	public ReversiSettingEvent mouseEventForSetting = new ReversiSettingEvent(this);
	/** 開始ボタン */
	public Button buttonStart;
	/** 戻すボタン */
	public Button buttonUndo;
	/** 現在の着手側 */
	public int currentTurn = ReversiColor.BLACK.getValue();
	/** 読みの深さ(先手) */
	public int depth1 = 8;
	/** 最終読みの深さ(先手) */
	public int finalDepth1 = 18;
	/** 読みの深さ(後手) */
	public int depth2 = 8;
	/** 最終読みの深さ(後手) */
	public int finalDepth2 = 18;
	/** サービス */
	private ExecutorService service = Executors.newSingleThreadExecutor();
	/** 盤面上のオセロのマッピング */
	private Map<ReversiPoint, Ellipse> colorMapping = new HashMap<>();
	/** 履歴 */
	private Stack<ReversiHistory> hisotry = new Stack<>();
	/** 先手の読みの深さプルダウン */
	private final ChoiceBox<Integer> selectDepth1 = new ChoiceBox<>(
			FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
					22, 23, 24, 25, 26, 27, 28, 29, 30));
	/** 先手の最終読みの深さプルダウン */
	private final ChoiceBox<Integer> selectFinalDepth1 = new ChoiceBox<>(
			FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
					22, 23, 24, 25, 26, 27, 28, 29, 30));
	/** 後手の読みの深さプルダウン */
	private final ChoiceBox<Integer> selectDepth2 = new ChoiceBox<>(
			FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
					22, 23, 24, 25, 26, 27, 28, 29, 30));
	/** 後手の最終読みの深さプルダウン */
	private final ChoiceBox<Integer> selectFinalDepth2 = new ChoiceBox<>(
			FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
					22, 23, 24, 25, 26, 27, 28, 29, 30));

	/** 現在の手番ラベル */
	private Label labelCurrentTurn;
	/** 現在の評価値ラベル */
	private Label labelCurrentEval;
	/** CPU思考中ラベル */
	private Label labelThinking;
	/** 黒の数ラベル */
	private Label labelBlackDisc;
	/** 白の数ラベル */
	private Label labelWhiteDisc;
	/** 盤面設定(黒) */
	private RadioButton boardSettingBlack;
	/** 盤面設定(白) */
	private RadioButton boardSettingWhite;
	/** 盤面設定(空白) */
	private RadioButton boardSettingNone;
	/** 再開(黒) */
	private RadioButton restartBlack = new RadioButton("黒");
	/** 再開(白) */
	private RadioButton restartWhite = new RadioButton("白");

	/**
	 * メイン
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		final BorderPane layout = new BorderPane();

		final BorderPane back = new BorderPane();

		final BorderPane front = new BorderPane();

		// 背面パネル
		try (InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("img/framePanel_default.gif")) {
			final Image img = new Image(is);
			final BackgroundSize bs = new BackgroundSize(440, 440, false, false, false, true);
			final BackgroundImage bImg = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,
					BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, bs);
			final Background background = new Background(bImg);
			back.setBackground(background);
			back.setMaxSize(440, 440);
		}

		// 前面パネル
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("img/backPanel.gif")) {
			final Image img = new Image(is);
			final BackgroundSize bs = new BackgroundSize(400, 400, false, false, false, true);
			final BackgroundImage bImg = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,
					BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, bs);
			final Background background = new Background(bImg);
			front.setBackground(background);
			front.setMaxSize(400, 400);
			front.setStyle("-fx-border-style:solid;-fx-border-width:2px;");
		}

		back.setCenter(front);
		layout.setCenter(back);

		// 右パネル
		final VBox rightPane = new VBox();
		rightPane.setMinSize(190, 440);
		rightPane.setAlignment(Pos.TOP_CENTER);

		// ロゴ
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("img/logo.gif")) {
			final Image img = new Image(is);
			final BackgroundSize bs = new BackgroundSize(150, 50, false, false, false, true);
			final BackgroundImage bImg = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,
					BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, bs);
			final Background background = new Background(bImg);
			final Label logo = new Label();
			logo.setBackground(background);
			logo.setMinSize(150, 50);
			rightPane.getChildren().add(logo);
		}

		// クリックイベント
		this.addClickEvent(front);

		// AIの検索
		new FastClasspathScanner().matchClassesImplementing(ArtificalIntelligence.class,
				clazz -> {
					aiMap.put(clazz.getSimpleName(), clazz);
				}).scan();

		// 先手選択
		final HBox boxPlayer1 = new HBox();
		boxPlayer1.setAlignment(Pos.BASELINE_LEFT);
		final Label labelPlayer1 = new Label("先手");
		labelPlayer1.setMinSize(30, 20);
		final ChoiceBox<String> selectPlayer1 = new ChoiceBox<>(
				FXCollections.observableArrayList(aiMap.keySet()));
		selectPlayer1.getSelectionModel().selectedItemProperty().addListener(
				(ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
					if (newValue.equals("Human")) {
						this.selectDepth1.setDisable(true);
						this.selectFinalDepth1.setDisable(true);
					} else {
						this.selectDepth1.setDisable(false);
						this.selectFinalDepth1.setDisable(false);
					}
				});
		boxPlayer1.getChildren().addAll(labelPlayer1, selectPlayer1);
		rightPane.getChildren().add(boxPlayer1);

		// 先手の読みの深さ選択
		final HBox boxDepth1 = new HBox();
		boxDepth1.setAlignment(Pos.BASELINE_LEFT);
		final Label labelDepth1 = new Label("読みの深さ");
		labelDepth1.setMinSize(90, 20);
		this.selectDepth1.setValue(this.depth1);
		this.selectDepth1.getSelectionModel().selectedItemProperty().addListener(
				(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
					this.depth1 = (Integer) newValue;
				});
		boxDepth1.getChildren().addAll(labelDepth1, this.selectDepth1);
		rightPane.getChildren().add(boxDepth1);

		// 先手の最終読みの深さ選択
		final HBox boxFinalDepth1 = new HBox();
		boxFinalDepth1.setAlignment(Pos.BASELINE_LEFT);
		final Label labelFinalDepth1 = new Label("最終読みの深さ");
		labelFinalDepth1.setMinSize(90, 20);
		this.selectFinalDepth1.setValue(this.finalDepth1);
		this.selectFinalDepth1.getSelectionModel().selectedItemProperty().addListener(
				(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
					this.finalDepth1 = (Integer) newValue;
				});
		boxFinalDepth1.getChildren().addAll(labelFinalDepth1, this.selectFinalDepth1);
		rightPane.getChildren().add(boxFinalDepth1);

		// 後手選択
		final HBox boxPlayer2 = new HBox();
		boxPlayer2.setAlignment(Pos.BASELINE_LEFT);
		final Label labelPlayer2 = new Label("後手");
		labelPlayer2.setMinSize(30, 20);
		final ChoiceBox<String> selectPlayer2 = new ChoiceBox<>(
				FXCollections.observableArrayList(aiMap.keySet()));
		selectPlayer2.getSelectionModel().selectedItemProperty().addListener(
				(ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
					if (newValue.equals("Human")) {
						this.selectDepth2.setDisable(true);
						this.selectFinalDepth2.setDisable(true);
					} else {
						this.selectDepth2.setDisable(false);
						this.selectFinalDepth2.setDisable(false);
					}
				});
		boxPlayer2.getChildren().addAll(labelPlayer2, selectPlayer2);
		rightPane.getChildren().add(boxPlayer2);

		// 後手の読みの深さ選択
		final HBox boxDepth2 = new HBox();
		boxDepth2.setAlignment(Pos.BASELINE_LEFT);
		final Label labelDepth2 = new Label("読みの深さ");
		labelDepth2.setMinSize(90, 20);
		this.selectDepth2.setValue(this.depth2);
		this.selectDepth2.getSelectionModel().selectedItemProperty().addListener(
				(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
					this.depth2 = (Integer) newValue;
				});
		boxDepth2.getChildren().addAll(labelDepth2, this.selectDepth2);
		rightPane.getChildren().add(boxDepth2);

		// 後手の最終読みの深さ選択
		final HBox boxFinalDepth2 = new HBox();
		boxFinalDepth2.setAlignment(Pos.BASELINE_LEFT);
		final Label labelFinalDepth2 = new Label("最終読みの深さ");
		labelFinalDepth2.setMinSize(90, 20);
		this.selectFinalDepth2.setValue(this.finalDepth2);
		this.selectFinalDepth2.getSelectionModel().selectedIndexProperty().addListener(
				(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
					this.finalDepth2 = (Integer) newValue;
				});
		boxFinalDepth2.getChildren().addAll(labelFinalDepth2, this.selectFinalDepth2);
		rightPane.getChildren().add(boxFinalDepth2);

		// 対局・終了
		final HBox boxButton = new HBox();
		boxButton.setAlignment(Pos.CENTER);
		this.buttonStart = new Button("対局");
		this.buttonStart.setOnAction(event -> {
			final String player1 = selectPlayer1.getSelectionModel().getSelectedItem();
			final String player2 = selectPlayer2.getSelectionModel().getSelectedItem();
			this.board.init();
			this.removeAllHistories();
			this.currentTurn = ReversiColor.BLACK.getValue();
			this.play(player1, player2);
		});

		this.buttonUndo = new Button("戻す");
		this.buttonUndo.setOnAction(event -> {
			if (this.canUndo()) {
				final ReversiBoard current = this.undo();
				this.board.setBoard(current.getBoard());
				this.repaintBoard();
			}
		});

		final Button buttonStop = new Button("終了");
		buttonStop.setOnAction(event -> {
			final String message = "処理を終了しました。";
			this.setLabelThinking(message);
			this.destroy();
		});
		boxButton.getChildren().addAll(this.buttonStart, this.buttonUndo, buttonStop);
		rightPane.getChildren().add(boxButton);

		// 着手状況
		final HBox boxTurn = new HBox();
		final Label labelTurnTitle = new Label("手番：");
		labelTurnTitle.setPrefSize(40, 20);
		this.labelCurrentTurn = new Label();
		this.labelCurrentTurn.setPrefSize(180, 20);
		boxTurn.getChildren().addAll(labelTurnTitle, labelCurrentTurn);
		rightPane.getChildren().add(boxTurn);

		// 評価情報
		final HBox boxEval = new HBox();
		final Label labelEvalTitle = new Label("評価：");
		labelEvalTitle.setPrefSize(40, 20);
		this.labelCurrentEval = new Label();
		this.labelCurrentEval.setPrefSize(180, 20);
		boxEval.getChildren().addAll(labelEvalTitle, labelCurrentEval);
		rightPane.getChildren().add(boxEval);

		// 思考中
		final HBox boxThinking = new HBox();
		this.labelThinking = new Label();
		this.labelThinking.setPrefSize(180, 20);
		boxThinking.getChildren().add(this.labelThinking);
		rightPane.getChildren().add(boxThinking);

		// 石の数表示
		final HBox boxDiscs = new HBox();
		final Rectangle bgBlack = new Rectangle(20.0, 20.0, Color.GREEN);
		final Rectangle bgWhite = new Rectangle(20.0, 20.0, Color.GREEN);
		final Circle circleBlack = new Circle(10.0, 10.0, 8.0, Color.BLACK);
		final Circle circleWhite = new Circle(10.0, 10.0, 8.0, Color.WHITE);
		final Group groupBlack = new Group(bgBlack, circleBlack);
		final Group groupWhite = new Group(bgWhite, circleWhite);
		boxDiscs.getChildren().add(groupBlack);
		this.labelBlackDisc = new Label();
		this.labelBlackDisc.setPrefSize(30, 20);
		this.labelBlackDisc.setAlignment(Pos.CENTER);
		boxDiscs.getChildren().add(this.labelBlackDisc);
		boxDiscs.getChildren().add(groupWhite);
		this.labelWhiteDisc = new Label();
		this.labelWhiteDisc.setPrefSize(30, 20);
		this.labelWhiteDisc.setAlignment(Pos.CENTER);
		boxDiscs.getChildren().add(this.labelWhiteDisc);
		rightPane.getChildren().add(boxDiscs);

		// 盤面設定
		final Button buttonSetting = new Button("盤面設定");
		final Button buttonRestart = new Button("再開");

		final HBox boxBoardSettingRadios = new HBox();
		final ToggleGroup boardSettingGroup = new ToggleGroup();
		this.boardSettingBlack = new RadioButton("黒");
		this.boardSettingBlack.setPrefSize(40, 20);
		this.boardSettingBlack.setAlignment(Pos.CENTER_LEFT);
		this.boardSettingBlack.setSelected(true);
		this.boardSettingBlack.setToggleGroup(boardSettingGroup);
		this.boardSettingWhite = new RadioButton("白");
		this.boardSettingWhite.setPrefSize(40, 20);
		this.boardSettingWhite.setAlignment(Pos.CENTER_LEFT);
		this.boardSettingWhite.setToggleGroup(boardSettingGroup);
		this.boardSettingNone = new RadioButton("空白");
		this.boardSettingNone.setPrefSize(60, 20);
		this.boardSettingNone.setAlignment(Pos.CENTER_LEFT);
		this.boardSettingNone.setToggleGroup(boardSettingGroup);
		boxBoardSettingRadios.getChildren().addAll(buttonSetting, boardSettingBlack, boardSettingWhite,
				boardSettingNone);
		rightPane.getChildren().add(boxBoardSettingRadios);

		final HBox boxRestartRadios = new HBox();
		final ToggleGroup restartGroup = new ToggleGroup();
		this.restartBlack.setPrefSize(40, 20);
		this.restartBlack.setAlignment(Pos.CENTER_LEFT);
		this.restartBlack.setSelected(true);
		this.restartBlack.setToggleGroup(restartGroup);
		this.restartWhite.setPrefSize(40, 20);
		this.restartWhite.setAlignment(Pos.CENTER_LEFT);
		this.restartWhite.setToggleGroup(restartGroup);
		boxRestartRadios.getChildren().addAll(buttonRestart, this.restartBlack, this.restartWhite);
		rightPane.getChildren().add(boxRestartRadios);

		// 盤面設定イベント
		buttonSetting.setOnAction(event -> {
			this.destroy();
			this.removeClickEvent(front);
			this.addSettingClickEvent(front);
		});

		// 再開イベント
		buttonRestart.setOnAction(event -> {
			this.removeSettingClickEvent(front);
			this.addClickEvent(front);
			this.currentTurn = this.restartBlack.isSelected() ? ReversiColor.BLACK.getValue()
					: ReversiColor.WHITE.getValue();
			final String player1 = selectPlayer1.getSelectionModel().getSelectedItem();
			final String player2 = selectPlayer2.getSelectionModel().getSelectedItem();
			this.play(player1, player2);
		});
		layout.setRight(rightPane);

		// 終了時処理
		primaryStage.setOnCloseRequest(event -> {
			this.service.shutdownNow();
		});

		// 盤面の初期化
		this.initialPaintBoard(front);

		primaryStage.setTitle("sato's Reversi");
		primaryStage.setScene(new Scene(layout, 660, 440));
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	/**
	 * X座標、Y座標から盤面上の位置を計算する
	 *
	 * @param x X座標
	 * @param y Y座標
	 * @return 盤面の位置
	 */
	public ReversiPoint calcPoint(double x, double y) {
		final double posX = (x - Reversi.MARGIN) / Reversi.RECTANGLE;
		final double posY = (y - Reversi.MARGIN) / Reversi.RECTANGLE;
		return ReversiPoint.fromValue((int) posX, (int) posY);
	}

	/**
	 * 終了
	 */
	public void finish() {
		this.showMessageDialog("黒：" + this.board.count(ReversiColor.BLACK.getValue()) + " 白："
				+ this.board.count(ReversiColor.WHITE.getValue()));
	}

	/**
	 * メッセージダイアログを表示する
	 *
	 * @param message メッセージ
	 */
	public void showMessageDialog(String message) {
		this.showDialog(message, AlertType.INFORMATION);
	}

	/**
	 * エラーダイアログを表示する
	 *
	 * @param message メッセージ
	 */
	public void showErrorDialog(String message) {
		this.showDialog(message, AlertType.ERROR);
	}

	/**
	 * メッセージダイアログを表示する
	 *
	 * @param message メッセージ
	 * @param messageType メッセージ種別
	 */
	private void showDialog(String message, AlertType messageType) {
		Platform.runLater(() -> {
			final Alert alert = new Alert(AlertType.NONE, message, ButtonType.OK);
			alert.setTitle(messageType.name());
			alert.showAndWait();
		});
	}

	/**
	 * 現在の手番のメッセージを設定する
	 *
	 * @param message メッセージ
	 */
	public void setLabelCurrentTurn(String message) {
		Platform.runLater(() -> {
			this.labelCurrentTurn.setText(message);
		});
	}

	/**
	 * 現在の評価値のメッセージを設定する
	 *
	 * @param message メッセージ
	 */
	public void setLabelCurrentEval(String message) {
		Platform.runLater(() -> {
			this.labelCurrentEval.setText(message);
		});
	}

	/**
	 * 思考中のメッセージを設定する
	 *
	 * @param message メッセージ
	 */
	public void setLabelThinking(String message) {
		Platform.runLater(() -> {
			this.labelThinking.setText(message);
		});
	}

	/**
	 * 黒の数を設定する
	 *
	 * @param count 数
	 */
	public void setLabelBlackDiscs(int count) {
		Platform.runLater(() -> {
			this.labelBlackDisc.setText(Integer.toString(count));
		});
	}

	/**
	 * 白の数を設定する
	 *
	 * @param count 数
	 */
	public void setLabelWhiteDiscs(int count) {
		Platform.runLater(() -> {
			this.labelWhiteDisc.setText(Integer.toString(count));
		});
	}

	/**
	 * 破棄
	 */
	public void destroy() {
		this.service.shutdownNow();
		this.buttonStart.setDisable(false);
		this.buttonUndo.setDisable(false);
	}

	/**
	 * 履歴を追加する
	 *
	 * @param history 履歴
	 */
	public void addHistory(ReversiHistory history) {
		this.hisotry.push(history);
	}

	/**
	 * 履歴をクリアする
	 */
	public void removeAllHistories() {
		this.hisotry.clear();
	}

	/**
	 * アンドゥー出来るかチェックする
	 *
	 * @return アンドゥー出来る場合 true
	 */
	public boolean canUndo() {
		if (2 < this.hisotry.size()) {
			final ListIterator<ReversiHistory> it = this.hisotry.listIterator(this.hisotry.size() - 1);
			while (it.hasPrevious()) {
				final ReversiHistory history = it.previous();
				if (history.getColor().equals(ReversiColor.fromValue(this.currentTurn))) {
					return it.hasPrevious();
				}
			}
		}
		return false;
	}

	/**
	 * アンドゥーを行う
	 *
	 * @return アンドゥー後の盤面
	 */
	public ReversiBoard undo() {
		if (this.canUndo()) {
			// 最初に直近の着手を取り消し
			final ReversiHistory latest = this.hisotry.pop();
			if (latest.getColor().getValue() == this.currentTurn) {
				// 自分が連続して着手(相手パス)の場合
				return BitBoardUtil.toReversiBoard(this.hisotry.peek().getBoard());
			}
			final ListIterator<ReversiHistory> it = this.hisotry.listIterator(this.hisotry.size());
			while (it.hasPrevious()) {
				final ReversiHistory history = it.previous();
				// 自分の着手が出てくるまで繰り返し探索
				if (history.getColor().getValue() == this.currentTurn) {
					// 自分の着手を見つけたら自分の着手を取り消してその前の盤面を返す
					it.remove();
					return BitBoardUtil.toReversiBoard(it.previous().getBoard());
				}
				it.remove();
			}
		}
		throw new IllegalStateException("現在の局面ではアンドゥーできません。\n" + this.board);
	}

	/**
	 * クリックイベントを登録する
	 *
	 * @param front フロントペイン
	 */
	private void addClickEvent(BorderPane front) {
		// クリックイベント
		front.addEventHandler(MouseEvent.MOUSE_CLICKED, this.clickEventForBlack);
		front.addEventHandler(MouseEvent.MOUSE_CLICKED, this.clickEventForWhite);
	}

	/**
	 * クリックイベントを解除する
	 *
	 * @param front フロントペイン
	 */
	private void removeClickEvent(BorderPane front) {
		// クリックイベント
		front.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.clickEventForBlack);
		front.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.clickEventForWhite);
	}

	/**
	 * 盤面設定クリックイベントを登録する
	 *
	 * @param front フロントペイン
	 */
	private void addSettingClickEvent(BorderPane front) {
		front.addEventHandler(MouseEvent.MOUSE_CLICKED, this.mouseEventForSetting);
		front.addEventHandler(MouseEvent.MOUSE_DRAGGED, this.mouseEventForSetting);
	}

	/**
	 * 盤面設定クリックイベントを解除する
	 *
	 * @param front フロントペイン
	 */
	private void removeSettingClickEvent(BorderPane front) {
		front.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.mouseEventForSetting);
		front.removeEventHandler(MouseEvent.MOUSE_DRAGGED, this.mouseEventForSetting);
	}

	/**
	 * 盤面の初期描画
	 *
	 * @param front フロントペイン
	 */
	private void initialPaintBoard(BorderPane front) {
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				final int color = this.board.getColor(x, y);
				final Color paintColor;
				if (color == ReversiColor.NONE.getValue()) {
					paintColor = Color.TRANSPARENT;
				} else if (color == ReversiColor.BLACK.getValue()) {
					paintColor = Color.BLACK;
				} else {
					paintColor = Color.WHITE;
				}
				final int posX = (x * Reversi.RECTANGLE) + Reversi.MARGIN;
				final int posY = (y * Reversi.RECTANGLE) + Reversi.MARGIN;
				final Ellipse ellipse = new Ellipse(posX, posY, ELLIPSE_IMAGE_SIZE, ELLIPSE_IMAGE_SIZE);
				final Group group = new Group(ellipse);
				ellipse.setFill(paintColor);
				ellipse.setSmooth(true);
				front.getChildren().add(group);
				this.colorMapping.put(ReversiPoint.fromValue(x, y), ellipse);
			}
		}
		this.setLabelBlackDiscs(this.board.count(ReversiColor.BLACK.getValue()));
		this.setLabelWhiteDiscs(this.board.count(ReversiColor.WHITE.getValue()));
	}

	/**
	 * 盤面の再描画
	 */
	public void repaintBoard() {
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				final int color = this.board.getColor(x, y);
				final Color paintColor;
				if (color == ReversiColor.NONE.getValue()) {
					paintColor = Color.TRANSPARENT;
				} else if (color == ReversiColor.BLACK.getValue()) {
					paintColor = Color.BLACK;
				} else {
					paintColor = Color.WHITE;
				}
				final Ellipse ellipse = this.colorMapping.get(ReversiPoint.fromValue(x, y));
				ellipse.setFill(paintColor);
			}
		}
		this.setLabelBlackDiscs(this.board.count(ReversiColor.BLACK.getValue()));
		this.setLabelWhiteDiscs(this.board.count(ReversiColor.WHITE.getValue()));
	}

	/**
	 * 盤面の再描画(アニメーション)
	 *
	 * @param point 着手位置
	 * @param color 色
	 * @param oldBoard 変更前の盤面
	 * @param newBoard 変更後の盤面
	 */
	public void repaintBoard(ReversiPoint point, ReversiColor color, ReversiBoard oldBoard, ReversiBoard newBoard) {

		// 変更が加わった箇所の色を変える
		final BitBoard oldBitBoard = new BitBoard(oldBoard);
		final BitBoard newBitBoard = new BitBoard(newBoard);
		final long flipPoint = BitPoint.fromValue(point.x, point.y).mask;
		final long diff;

		// 描画色
		final Color paintColor;
		if (color.equals(ReversiColor.BLACK)) {
			paintColor = Color.BLACK;
			diff = oldBitBoard.blackDiscs ^ newBitBoard.blackDiscs & ~flipPoint;
		} else if (color.equals(ReversiColor.WHITE)) {
			paintColor = Color.WHITE;
			diff = oldBitBoard.whiteDiscs ^ newBitBoard.whiteDiscs & ~flipPoint;
		} else {
			throw new IllegalArgumentException("無効な色が指定されました。" + color);
		}

		int blackCount = oldBoard.count(ReversiColor.BLACK.getValue());
		int whiteCount = oldBoard.count(ReversiColor.WHITE.getValue());

		// 着手位置の色を変える
		final Ellipse ellipse = this.colorMapping.get(point);
		ellipse.setFill(paintColor);

		// 石の表示数を更新
		if (color.equals(ReversiColor.BLACK)) {
			blackCount++;
			this.setLabelBlackDiscs(blackCount);
		} else {
			whiteCount++;
			this.setLabelWhiteDiscs(whiteCount);
		}

		// ビット演算用変数
		long t;
		// 左上
		for (t = flipPoint << 9; (t & BitMask.LOWER_RIGHT_REVERSE.mask) != 0; t <<= 9) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, -1, 1);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 上
		for (t = flipPoint << 8; (t & BitMask.LOWER_LINE_REVERSE.mask) != 0; t <<= 8) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, 0, 1);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 右上
		for (t = flipPoint << 7; (t & BitMask.LOWER_LEFT_REVERSE.mask) != 0; t <<= 7) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, 1, 1);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 右
		for (t = flipPoint >>> 1; (t & BitMask.LEFT_LINE_REVERSE.mask) != 0; t >>>= 1) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, 1, 0);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 右下
		for (t = flipPoint >>> 9; (t & BitMask.UPPER_LEFT_REVERSE.mask) != 0; t >>>= 9) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, 1, -1);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 下
		for (t = flipPoint >>> 8; (t & BitMask.UPPER_LINE_REVERSE.mask) != 0; t >>>= 8) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, 0, 1);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 左下
		for (t = flipPoint >>> 7; (t & BitMask.LEFT_LINE_REVERSE.mask) != 0; t >>>= 7) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, -1, -1);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}
		// 左
		for (t = flipPoint << 1; (t & BitMask.RIGHT_LINE_REVERSE.mask) != 0; t <<= 1) {
			if ((t & diff) != 0) {
				final BitPoint target = BitPoint.fromMask(t);
				final Ellipse targetEllipse = this.colorMapping.get(ReversiPoint.fromValue(target.x, target.y));
				this.flipAnimation(targetEllipse, paintColor, -1, 0);
				// 石の表示数を更新
				if (color.equals(ReversiColor.BLACK)) {
					blackCount++;
					whiteCount--;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				} else {
					blackCount--;
					whiteCount++;
					this.setLabelBlackDiscs(blackCount);
					this.setLabelWhiteDiscs(whiteCount);
				}
			}
		}

		this.setLabelBlackDiscs(this.board.count(ReversiColor.BLACK.getValue()));
		this.setLabelWhiteDiscs(this.board.count(ReversiColor.WHITE.getValue()));
	}

	/**
	 * 反転される石をアニメーションする
	 *
	 * @param ellipse 石
	 * @param paintColor 色
	 * @param x Ⅹ軸
	 * @param y Ｙ軸
	 */
	private void flipAnimation(Ellipse ellipse, Color paintColor, int x, int y) {
		int angle = -1;
		if (x == -1) {
			if (y == 1) {
				// 左上
				angle = 360 - 45;
			} else if (y == 0) {
				// 左
				angle = 360 - 90;
			} else if (y == -1) {
				// 左下
				angle = 360 - 135;
			}
		} else if (x == 0) {
			if (y == 1) {
				// 上
				angle = 0;
			} else if (y == -1) {
				// 下
				angle = 180;
			}
		} else if (x == 1) {
			if (y == 1) {
				// 右上
				angle = 45;
			} else if (y == 0) {
				// 右
				angle = 90;
			} else if (y == -1) {
				// 右下
				angle = 135;
			}
		}

		if (angle < 0) {
			throw new IllegalArgumentException(MessageFormat.format("不正な座標が指定されました。[{0}, {1}]", x, y));
		}

		// 円形に対して角度を付ける
		ellipse.setRotate(angle);

		// 徐々に小さくする
		for (int i = ELLIPSE_IMAGE_SIZE; 0 <= i; i -= 2) {
			ellipse.setRadiusY(i);
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		// 色を変える
		ellipse.setFill(paintColor);
		// 徐々に大きくする
		for (int i = 0; i <= ELLIPSE_IMAGE_SIZE; i += 2) {
			ellipse.setRadiusY(i);
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

		// 円形を初期化
		ellipse.setRotate(0);
		ellipse.setRadiusX(ELLIPSE_IMAGE_SIZE);
		ellipse.setRadiusY(ELLIPSE_IMAGE_SIZE);
	}

	/**
	 * 盤面設定色を返す
	 *
	 * @return 盤面設定色
	 */
	public ReversiColor getBoardSettingColor() {
		if (this.boardSettingBlack.isSelected()) {
			return ReversiColor.BLACK;
		}
		if (this.boardSettingWhite.isSelected()) {
			return ReversiColor.WHITE;
		}
		if (this.boardSettingNone.isSelected()) {
			return ReversiColor.NONE;
		}
		return null;
	}

	/**
	 * ゲーム開始
	 *
	 * @param player1 先手
	 * @param player2 後手
	 */
	protected void play(String player1, String player2) {
		if (StringUtils.isEmpty(player1)) {
			this.showErrorDialog("先手を選択してください。");
			return;
		}
		if (StringUtils.isEmpty(player2)) {
			this.showErrorDialog("後手を選択してください。");
			return;
		}
		this.players.clear();

		final Class<? extends ArtificalIntelligence> blackAI = this.aiMap.get(player1);
		final Class<? extends ArtificalIntelligence> whiteAI = this.aiMap.get(player2);
		try {
			final ArtificalIntelligence ai = blackAI.newInstance();
			this.players.put(ReversiColor.BLACK, ai);
			if (ai instanceof Human) {
				((Human) ai).setEvent(this.clickEventForBlack);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			this.showErrorDialog(MessageFormat.format("{0}のAI({1})をインスタンス化できません。", ReversiColor.BLACK.getDisplayName(),
					blackAI.getName()));
			throw new RuntimeException(e);
		}
		try {
			final ArtificalIntelligence ai = whiteAI.newInstance();
			this.players.put(ReversiColor.WHITE, ai);
			if (ai instanceof Human) {
				((Human) ai).setEvent(this.clickEventForWhite);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			this.showErrorDialog(MessageFormat.format("{0}のAI({1})をインスタンス化できません。", ReversiColor.WHITE.getDisplayName(),
					whiteAI.getName()));
			throw new RuntimeException(e);
		}
		this.repaintBoard();
		this.addHistory(new ReversiHistory(ReversiColor.fromValue(this.currentTurn), this.board));
		this.service = Executors.newSingleThreadExecutor();
		this.service.execute(new ReversiExecutor(this));
		this.buttonStart.setDisable(true);
	}
}
