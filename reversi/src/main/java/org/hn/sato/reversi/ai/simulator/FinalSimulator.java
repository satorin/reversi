package org.hn.sato.reversi.ai.simulator;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.EvalResult;

/**
 * 盤面の最終読みを行うシミュレーター
 *
 * @author s-kotaki
 *
 */
public class FinalSimulator extends AbstractEvalSimulator {

	@Override
	public final EvalResult eval(ReversiBoard board, int baseColor) {
		final SimulatorEvalResult result = new SimulatorEvalResult(board.clone(), baseColor);
		result.fixed[0] = board.count(ReversiColor.BLACK.getValue());
		result.fixed[1] = board.count(ReversiColor.WHITE.getValue());
		result.setEvaluation((baseColor == ReversiColor.BLACK.getValue()) ? result.fixed[0] - result.fixed[1]
				: result.fixed[1] - result.fixed[0]);
		return result;
	}
}
