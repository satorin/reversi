package org.hn.sato.reversi.bit;

/**
 * ビットマスク
 *
 * @author s-kotaki
 *
 */
public enum BitMask {

	/**
	 * '0'を示すビットマスク(0000000000000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	ZERO(0x0000000000000000L),
	/**
	 * '1'を示すビットマスク(0000000000000000000000000000000000000000000000000000000000000001)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●
	 */
	ONE(0x0000000000000001L),
	/**
	 * '-1'を示すビットマスク(1000000000000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	MIN(0x8000000000000000L),
	/**
	 * ４つ隅のビットマスク(1000000100000000000000000000000000000000000000000000000010000001)
	 * <p>
	 * ●□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□●
	 */
	ALL_CORNERS(0x8100000000000081L),
	/**
	 * ４つのⅩのビットマスク(0000000001000010000000000000000000000000000000000100001000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □●□□□□●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●□□□□●□<br>
	 * □□□□□□□□
	 */
	ALL_XS(0x0042000000004200L),
	/**
	 * ８つのＣのビットマスク(0100001010000001000000000000000000000000000000001000000101000010)
	 * <p>
	 * □●□□□□●□<br>
	 * ●□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□●<br>
	 * □●□□□□●□
	 */
	ALL_CS(0x4281000000008142L),
	/**
	 * 左上(A1)と右上(H1)の隅のビットマスク(1000000100000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LINE_CORNERS(0x8100000000000000L),
	/**
	 * 左下(A8)と右下(H8)の隅のビットマスク(0000000000000000000000000000000000000000000000000000000010000001)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□●
	 */
	LOWER_LINE_CORNERS(0x0000000000000081L),
	/**
	 * 左上(A1)と左下(A8)の隅のビットマスク(1000000000000000000000000000000000000000000000000000000010000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□
	 */
	LEFT_LINE_CORNERS(0x8000000000000080L),
	/**
	 * 右上(H1)と右下(H8)の隅のビットマスク(0000000100000000000000000000000000000000000000000000000000000001)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●
	 */
	RIGHT_LINE_CORNERS(0x0100000000000001L),
	/**
	 * 上辺のビットマスク(1111111100000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●●●●●●●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LINE(0xff00000000000000L),
	/**
	 * 上辺のビットマスク反転(0000000011111111111111111111111111111111111111111111111111111111)
	 * <p>
	 * □□□□□□□□<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●
	 */
	UPPER_LINE_REVERSE(0x00ffffffffffffffL),
	/**
	 * 上辺２段のビットマスク(1111111111111111000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LINE_DOUBLE(0xffff000000000000L),
	/**
	 * 上辺３段のビットマスク(1111111111111111111111110000000000000000000000000000000000000000)
	 * <p>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LINE_TRIPLE(0xffffff0000000000L),
	/**
	 * 上辺２段目のビットマスク(0000000011111111000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●●●●●●●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LINE_SECOND(0x00ff000000000000L),
	/**
	 * 下辺のビットマスク(0000000000000000000000000000000000000000000000000000000011111111)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●●●●●●●
	 */
	LOWER_LINE(0x00000000000000ffL),
	/**
	 * 下辺のビットマスク反転(1111111111111111111111111111111111111111111111111111111100000000)
	 * <p>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * □□□□□□□□
	 */
	LOWER_LINE_REVERSE(0xffffffffffffff00L),
	/**
	 * 下辺２段のビットマスク(0000000000000000000000000000000000000000000000001111111111111111)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●
	 */
	LOWER_LINE_DOUBLE(0x000000000000ffffL),
	/**
	 * 下辺３段のビットマスク(0000000000000000000000000000000000000000111111111111111111111111)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●
	 */
	LOWER_LINE_TRIPLE(0x0000000000ffffffL),
	/**
	 * 下辺２段目のビットマスク(0000000000000000000000000000000000000000000000001111111100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●●●●●●●<br>
	 * □□□□□□□□
	 */
	LOWER_LINE_SECOND(0x000000000000ff00L),
	/**
	 * 左辺のビットマスク(1000000010000000100000001000000010000000100000001000000010000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□
	 */
	LEFT_LINE(0x8080808080808080L),
	/**
	 * 左辺のビットマスク反転(0111111101111111011111110111111101111111011111110111111101111111)
	 * <p>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●
	 */
	LEFT_LINE_REVERSE(0x7f7f7f7f7f7f7f7fL),
	/**
	 * 左辺２段のビットマスク(1100000011000000110000001100000011000000110000001100000011000000)
	 * <p>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□
	 */
	LEFT_LINE_DOUBLE(0xc0c0c0c0c0c0c0c0L),
	/**
	 * 左辺３段のビットマスク(1110000011100000111000001110000011100000111000001110000011100000)
	 * <p>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□<br>
	 * ●●●□□□□□
	 */
	LEFT_LINE_TRIPLE(0xe0e0e0e0e0e0e0e0L),
	/**
	 * 左辺２段目のビットマスク(0100000001000000010000000100000001000000010000000100000001000000)
	 * <p>
	 * □●□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●□□□□□□
	 */
	LEFT_LINE_SECOND(0x4040404040404040L),
	/**
	 * 右辺のビットマスク(0000000100000001000000010000000100000001000000010000000100000001)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●
	 */
	RIGHT_LINE(0x0101010101010101L),
	/**
	 * 右辺のビットマスク反転(1111111011111110111111101111111011111110111111101111111011111110)
	 * <p>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□
	 */
	RIGHT_LINE_REVERSE(0xfefefefefefefefeL),
	/**
	 * 右辺２段のビットマスク(0000001100000011000000110000001100000011000000110000001100000011)
	 * <p>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●
	 */
	RIGHT_LINE_DOUBLE(0x0303030303030303L),
	/**
	 * 右辺３段のビットマスク(0000011100000111000001110000011100000111000001110000011100000111)
	 * <p>
	 * □□□□□●●●<br>
	 * □□□□□●●●<br>
	 * □□□□□●●●<br>
	 * □□□□□●●●<br>
	 * □□□□□●●●<br>
	 * □□□□□●●●<br>
	 * □□□□□●●●<br>
	 * □□□□□●●●
	 */
	RIGHT_LINE_TRIPLE(0x0707070707070707L),
	/**
	 * 右辺２段目のビットマスク(0000001000000010000000100000001000000010000000100000001000000010)
	 * <p>
	 * □□□□□□●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□●□
	 */
	RIGHT_LINE_SECOND(0x0202020202020202L),
	/**
	 * 上下のマスク反転(0000000011111111111111111111111111111111111111111111111100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * ●●●●●●●●<br>
	 * □□□□□□□□
	 */
	UPPER_LOWER_REVERSE(0x00ffffffffffff00L),
	/**
	 * 左右のマスク反転(0111111001111110011111100111111001111110011111100111111001111110)
	 * <p>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□
	 */
	LEFT_RIGHT_REVERSE(0x7e7e7e7e7e7e7e7eL),
	/**
	 * 斜め(上下左右)のマスク(1111111110000001100000011000000110000001100000011000000111111111)
	 * <p>
	 * ●●●●●●●●<br>
	 * ●□□□□□□●<br>
	 * ●□□□□□□●<br>
	 * ●□□□□□□●<br>
	 * ●□□□□□□●<br>
	 * ●□□□□□□●<br>
	 * ●□□□□□□●<br>
	 * ●●●●●●●●
	 */
	DIAGONAL(0xff818181818181ffL),
	/**
	 * 斜め(上下左右)のマスク反転(0000000001111110011111100111111001111110011111100111111000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □●●●●●●□<br>
	 * □□□□□□□□
	 */
	DIAGONAL_REVERSE(0x007e7e7e7e7e7e00L),
	/**
	 * 斜め(上下左右)のマスク２段反転(0000000000000000001111000011110000111100001111000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●●●●□□<br>
	 * □□●●●●□□<br>
	 * □□●●●●□□<br>
	 * □□●●●●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	DIAGONAL_DOUBLE_REVERSE(0x00003c3c3c3c0000L),
	/**
	 * 右上のマスク反転(0000000011111110111111101111111011111110111111101111111011111110)
	 * <p>
	 * □□□□□□□□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□
	 */
	UPPER_RIGHT_REVERSE(0x00fefefefefefefeL),
	/**
	 * 左上のマスク反転(0000000001111111011111110111111101111111011111110111111101111111)
	 * <p>
	 * □□□□□□□□<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●
	 */
	UPPER_LEFT_REVERSE(0x007f7f7f7f7f7f7fL),
	/**
	 * 右下のマスク反転(1111111011111110111111101111111011111110111111101111111000000000)
	 * <p>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * ●●●●●●●□<br>
	 * □□□□□□□□
	 */
	LOWER_RIGHT_REVERSE(0xfefefefefefefe00L),
	/**
	 * 左下のマスク反転(0111111101111111011111110111111101111111011111110111111100000000)
	 * <p>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □●●●●●●●<br>
	 * □□□□□□□□
	 */
	LOWER_LEFT_REVERSE(0x7f7f7f7f7f7f7f00L),
	/**
	 * 左上隅３つのマスク(1100000010000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●●□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LEFT_3CORNERS(0xc080000000000000L),
	/**
	 * 左上隅２段目３つのマスク1(0000000011000000100000000000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LEFT_3CORNERS_SECOND_TYPE1(0x00c0800000000000L),
	/**
	 * 左上隅２段目３つのマスク2(0000000011000000100000000000000000000000000000000000000000000000)
	 * <p>
	 * □●●□□□□□<br>
	 * □●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_LEFT_3CORNERS_SECOND_TYPE2(0x0060400000000000L),
	/**
	 * 左下隅３つのマスク(0000000000000000000000000000000000000000000000001000000011000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●●□□□□□□
	 */
	LOWER_LEFT_3CORNERS(0x00000000000080c0L),
	/**
	 * 左下隅２段目３つのマスク1(0000000000000000000000000000000000000000100000001100000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * □□□□□□□□
	 */
	LOWER_LEFT_3CORNERS_SECOND_TYPE1(0x000000000080c000L),
	/**
	 * 左下隅２段目３つのマスク2(0000000000000000000000000000000000000000000000000100000001100000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●□□□□□□<br>
	 * □●●□□□□□
	 */
	LOWER_LEFT_3CORNERS_SECOND_TYPE2(0x0000000000004060L),
	/**
	 * 右上隅３つのマスク(0000001100000001000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□□●●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_RIGHT_3CORNERS(0x0301000000000000L),
	/**
	 * 右上隅２段目３つのマスク1(0000000000000011000000010000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□●●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_RIGHT_3CORNERS_SECOND_TYPE1(0x0003010000000000L),
	/**
	 * 右上隅２段目３つのマスク2(0000011000000010000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□●●□<br>
	 * □□□□□□●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_RIGHT_3CORNERS_SECOND_TYPE2(0x0602000000000000L),
	/**
	 * 右下隅３つのマスク(0000000000000000000000000000000000000000000000000000000100000011)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□●●
	 */
	LOWER_RIGHT_3CORNERS(0x0000000000000103L),
	/**
	 * 右下隅２段目３つのマスク1(0000000000000000000000000000000000000000000000010000001100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□□
	 */
	LOWER_RIGHT_3CORNERS_SECOND_TYPE1(0x0000000000010300L),
	/**
	 * 右下隅２段目３つのマスク2(0000000000000000000000000000000000000000000000000000001000000110)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●□<br>
	 * □□□□□●●□
	 */
	LOWER_RIGHT_3CORNERS_SECOND_TYPE2(0x0000000000000206L),
	/**
	 * 上辺の山のマスク(0111111000000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * □●●●●●●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_MOUNTAIN(0x7e00000000000000L),
	/**
	 * 上辺のピュア山のマスク(0111111000111100000000000000000000000000000000000000000000000000)
	 * <p>
	 * □●●●●●●□<br>
	 * □□●●●●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_MOUNTAIN_PURE(0x7e3c000000000000L),
	/**
	 * 上辺の山のマスク反転(1000000100000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_MOUNTAIN_REVERSE(0x8100000000000000L),
	/**
	 * 下辺の山のマスク(0000000000000000000000000000000000000000000000000000000001111110)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●●●●●●□
	 */
	LOWER_MOUNTAIN(0x000000000000007eL),
	/**
	 * 下辺のピュア山のマスク(0000000000000000000000000000000000000000000000000011110001111110)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●●●●□□<br>
	 * □●●●●●●□
	 */
	LOWER_MOUNTAIN_PURE(0x0000000000003c7eL),
	/**
	 * 下辺の山のマスク反転(0000000000000000000000000000000000000000000000000000000010000001)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□●
	 */
	LOWER_MOUNTAIN_REVERSE(0x0000000000000081L),
	/**
	 * 左辺の山のマスク(0000000010000000100000001000000010000000100000001000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_MOUNTAIN(0x0080808080808000L),
	/**
	 * 左辺のピュア山のマスク(0000000010000000110000001100000011000000110000001000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_MOUNTAIN_PURE(0x0080c0c0c0c08000L),
	/**
	 * 左辺の山のマスク反転(1000000000000000000000000000000000000000000000000000000010000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□
	 */
	LEFT_MOUNTAIN_REVERSE(0x8000000000000080L),
	/**
	 * 右辺の山のマスク(0000000000000001000000010000000100000001000000010000000100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□
	 */
	RIGHT_MOUNTAIN(0x0001010101010100L),
	/**
	 * 右辺のピュア山のマスク(0000000000000001000000110000001100000011000000110000000100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□
	 */
	RIGHT_MOUNTAIN_PURE(0x0001030303030100L),
	/**
	 * 右辺の山のマスク反転(0000000100000000000000000000000000000000000000000000000000000001)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●
	 */
	RIGHT_MOUNTAIN_REVERSE(0x0100000000000001L),
	/**
	 * 上辺の翼のマスク1(0111110000000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * □●●●●●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_WING_TYPE1(0x7c00000000000000L),
	/**
	 * 上辺のピュア翼のマスク1(0111110000111100000000000000000000000000000000000000000000000000)
	 * <p>
	 * □●●●●●□□<br>
	 * □□●●●●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_WING_TYPE1_PURE(0x7c3c000000000000L),
	/**
	 * 上辺の翼のマスク1の反転(1000001100000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●□□□□□●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_WING_TYPE1_REVERSE(0x8300000000000000L),
	/**
	 * 上辺の翼のマスク2(0011111000000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□●●●●●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_WING_TYPE2(0x3e00000000000000L),
	/**
	 * 上辺のピュア翼のマスク2(0011111000111100000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□●●●●●□<br>
	 * □□●●●●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_WING_TYPE2_PURE(0x3e3c000000000000L),
	/**
	 * 上辺の翼のマスク2の反転(1100000100000000000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●●□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_WING_TYPE2_REVERSE(0xc100000000000000L),
	/**
	 * 下辺の翼のマスク1(0000000000000000000000000000000000000000000000000000000000111110)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●●●●●□
	 */
	LOWER_WING_TYPE1(0x000000000000003eL),
	/**
	 * 下辺のピュア翼のマスク1(0000000000000000000000000000000000000000000000000011110000111110)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●●●●□□<br>
	 * □□●●●●●□
	 */
	LOWER_WING_TYPE1_PURE(0x0000000000003c3eL),
	/**
	 * 下辺の翼のマスク1の反転(0000000000000000000000000000000000000000000000000000000011000001)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●□□□□□●
	 */
	LOWER_WING_TYPE1_REVERSE(0x00000000000000c1L),
	/**
	 * 下辺の翼のマスク2(0000000000000000000000000000000000000000000000000000000001111100)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●●●●●□□
	 */
	LOWER_WING_TYPE2(0x000000000000007cL),
	/**
	 * 下辺のピュア翼のマスク2(0000000000000000000000000000000000000000000000000011110001111100)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●●●●□□<br>
	 * □●●●●●□□
	 */
	LOWER_WING_TYPE2_PURE(0x0000000000003c7cL),
	/**
	 * 下辺の翼のマスク2の反転(0000000000000000000000000000000000000000000000000000000010000011)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□●●
	 */
	LOWER_WING_TYPE2_REVERSE(0x0000000000000083L),
	/**
	 * 左辺の翼のマスク1(0000000000000000100000001000000010000000100000001000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_WING_TYPE1(0x0000808080808000L),
	/**
	 * 左辺のピュア翼のマスク1(0000000000000000110000001100000011000000110000001000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_WING_TYPE1_PURE(0x0000c0c0c0c08000L),
	/**
	 * 左辺の翼のマスク1の反転(1000000010000000000000000000000000000000000000000000000010000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□
	 */
	LEFT_WING_TYPE1_REVERSE(0x8080000000000080L),
	/**
	 * 左辺の翼のマスク2(0000000010000000100000001000000010000000100000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_WING_TYPE2(0x0080808080800000L),
	/**
	 * 左辺のピュア翼のマスク2(0000000010000000110000001100000011000000110000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_WING_TYPE2_PURE(0x0080c0c0c0c00000L),
	/**
	 * 左辺の翼のマスク2の反転(1000000000000000000000000000000000000000000000001000000010000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□
	 */
	LEFT_WING_TYPE2_REVERSE(0x8000000000008080L),
	/**
	 * 右辺の翼のマスク1(0000000000000001000000010000000100000001000000010000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	RIGHT_WING_TYPE1(0x0001010101010000L),
	/**
	 * 右辺のピュア翼のマスク1(0000000000000001000000110000001100000011000000110000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	RIGHT_WING_TYPE1_PURE(0x0001030303030000L),
	/**
	 * 右辺の翼のマスク1の反転(0000000100000000000000000000000000000000000000000000000100000001)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●
	 */
	RIGHT_WING_TYPE1_REVERSE(0x0100000000000101L),
	/**
	 * 右辺の翼のマスク2(0000000000000000000000010000000100000001000000010000000100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□
	 */
	RIGHT_WING_TYPE2(0x0000010101010100L),
	/**
	 * 右辺のピュア翼のマスク2(0000000000000000000000110000001100000011000000110000000100000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□
	 */
	RIGHT_WING_TYPE2_PURE(0x0000030303030100L),
	/**
	 * 右辺の翼のマスク2の反転(0000000100000001000000000000000000000000000000000000000000000001)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●
	 */
	RIGHT_WING_TYPE2_REVERSE(0x0101000000000001L),
	/**
	 * 上辺のブロックのマスク(0011110000100100000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□●●●●□□<br>
	 * □□●□□●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_BLOCK(0x3c24000000000000L),
	/**
	 * 上辺のピュアブロックのマスク(0011110000111100000000000000000000000000000000000000000000000000)
	 * <p>
	 * □□●●●●□□<br>
	 * □□●●●●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_BLOCK_PURE(0x3c3c000000000000L),
	/**
	 * 上辺のブロックのマスク反転(1100001101000010000000000000000000000000000000000000000000000000)
	 * <p>
	 * ●●□□□□●●<br>
	 * □●□□□□●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	UPPER_BLOCK_REVERSE(0xc342000000000000L),
	/**
	 * 下辺のブロックのマスク(0000000000000000000000000000000000000000000000000010010000111100)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●□□●□□<br>
	 * □□●●●●□□
	 */
	LOWER_BLOCK(0x000000000000243cL),
	/**
	 * 下辺のピュアブロックのマスク(0000000000000000000000000000000000000000000000000011110000111100)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●●●●□□<br>
	 * □□●●●●□□
	 */
	LOWER_BLOCK_PURE(0x0000000000003c3cL),
	/**
	 * 下辺のブロックのマスク反転(0000000000000000000000000000000000000000000000000100001011000011)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●□□□□●□<br>
	 * ●●□□□□●●
	 */
	LOWER_BLOCK_REVERSE(0x00000000000042c3L),
	/**
	 * 左辺のブロックのマスク(0000000000000000110000001000000010000000110000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_BLOCK(0x0000c08080c00000L),
	/**
	 * 左辺のピュアブロックのマスク(0000000000000000110000001100000011000000110000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	LEFT_BLOCK_PURE(0x0000c0c0c0c00000L),
	/**
	 * 左辺のブロックのマスク反転(1000000011000000000000000000000000000000000000001100000010000000)
	 * <p>
	 * ●□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●●□□□□□□<br>
	 * ●□□□□□□□
	 */
	LEFT_BLOCK_REVERSE(0x80c000000000c080L),
	/**
	 * 右辺のブロックのマスク(0000000000000000000000110000000100000001000000110000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●●<br>
	 * □□□□□□□●<br>
	 * □□□□□□□●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	RIGHT_BLOCK(0x0000030101030000L),
	/**
	 * 右辺のピュアブロックのマスク(0000000000000000000000110000001100000011000000110000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	RIGHT_BLOCK_PURE(0x0000030303030000L),
	/**
	 * 右辺のブロックのマスク反転(0000000100000011000000000000000000000000000000000000001100000001)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□●●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●●<br>
	 * □□□□□□□●
	 */
	RIGHT_BLOCK_REVERSE(0x0103000000000301L),
	/**
	 * ホワイトライン(0000000000000000001000000001000000001000000001000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●□□□□□<br>
	 * □□□●□□□□<br>
	 * □□□□●□□□<br>
	 * □□□□□●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	WHITE_LINE(0x0000201008040000L),
	/**
	 * ホワイトライン反転(1000000000000000000000000000000000000000000000000000000000000001)
	 * <p>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●
	 */
	WHITE_LINE_REVERSE(0x8000000000000001L),
	/**
	 * ブラックライン(0000000000000000000001000000100000010000001000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□●□□<br>
	 * □□□□●□□□<br>
	 * □□□●□□□□<br>
	 * □□●□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	BLACK_LINE(0x0000040810200000L),
	/**
	 * ブラックライン反転(0000000100000000000000000000000000000000000000000000000010000000)
	 * <p>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□
	 */
	BLACK_LINE_REVERSE(0x0100000000000080L),
	/**
	 * Ⅹライン(0000000000000000001001000001100000011000001001000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●□□●□□<br>
	 * □□□●●□□□<br>
	 * □□□●●□□□<br>
	 * □□●□□●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	X_LINE(0x0000241818240000L),
	/**
	 * Ⅹライン反転(0000000000000000001001000000000000000000001001000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□●□□●□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	X_LINE_REVERSE(0x0000240000240000L),
	/**
	 * Ｃラインタイプ１(0000000000100000000100000000100000000100000000100000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□●□□□□□<br>
	 * □□□●□□□□<br>
	 * □□□□●□□□<br>
	 * □□□□□●□□<br>
	 * □□□□□□●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	C_LINE_TYPE1(0x0020100804020000L),
	/**
	 * Ｃラインタイプ１反転(0100000000000000000000000000000000000000000000000000000100000000)
	 * <p>
	 * □●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□
	 */
	C_LINE_TYPE1_REVERSE(0x4000000000000100L),
	/**
	 * Ｃラインタイプ２(0000000000000100000010000001000000100000010000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□●□□<br>
	 * □□□□●□□□<br>
	 * □□□●□□□□<br>
	 * □□●□□□□□<br>
	 * □●□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	C_LINE_TYPE2(0x0004081020400000L),
	/**
	 * Ｃラインタイプ２反転(0000001000000000000000000000000000000000000000001000000000000000)
	 * <p>
	 * □□□□□□●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□
	 */
	C_LINE_TYPE2_REVERSE(0x0200000000008000L),
	/**
	 * Ｃラインタイプ３(0000000000000000010000000010000000010000000010000000010000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●□□□□□□<br>
	 * □□●□□□□□<br>
	 * □□□●□□□□<br>
	 * □□□□●□□□<br>
	 * □□□□□●□□<br>
	 * □□□□□□□□
	 */
	C_LINE_TYPE3(0x0000402010080400L),
	/**
	 * Ｃラインタイプ３反転(0000000010000000000000000000000000000000000000000000000000000010)
	 * <p>
	 * □□□□□□□□<br>
	 * ●□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●□
	 */
	C_LINE_TYPE3_REVERSE(0x0080000000000002L),
	/**
	 * Ｃラインタイプ４(0000000000000000000000100000010000001000000100000010000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□●□<br>
	 * □□□□□●□□<br>
	 * □□□□●□□□<br>
	 * □□□●□□□□<br>
	 * □□●□□□□□<br>
	 * □□□□□□□□
	 */
	C_LINE_TYPE4(0x0000020408102000L),
	/**
	 * Ｃラインタイプ４反転(0000000000000001000000000000000000000000000000000000000001000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□□□□□□●<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □●□□□□□□
	 */
	C_LINE_TYPE4_REVERSE(0x0001000000000040L),
	/**
	 * Ｃライン反転(0000000000100100010000100000000000000000000000000000000000000000)
	 * <p>
	 * □□□□□□□□<br>
	 * □□●□□●□□<br>
	 * □●□□□□●□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□<br>
	 * □□□□□□□□
	 */
	C_LINE_REVERSE(0x0024420000000000L),
	;

	/** マスク */
	public final long mask;

	/**
	 * コンストラクタ
	 *
	 * @param mask マスク
	 */
	BitMask(long mask) {
		this.mask = mask;
	}

	/**
	 * バイナリ文字列表現を返す
	 *
	 * @return バイナリ文字列表現
	 */
	public String toBinaryString() {
		return BitBoardUtil.toBitString(this.mask);
	}

	/**
	 * HEX文字列表現を返す
	 *
	 * @return HEX文字列表現
	 */
	public String toHexString() {
		return BitBoardUtil.toHexString(this.mask);
	}

	/**
	 * 盤面上でどのような配置かの文字列表現を返す
	 *
	 * @return 盤面上での配置文字列
	 */
	public String toBoardString() {
		return new BitBoard(this.mask, 0L).toString();
	}
}
