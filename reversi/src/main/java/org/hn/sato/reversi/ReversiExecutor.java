package org.hn.sato.reversi;

import java.text.MessageFormat;

import org.hn.sato.reversi.ai.ArtificalIntelligence;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.ai.Human;
import org.hn.sato.reversi.logger.ReversiLogger;

/**
 * 対局実行スレッド
 *
 * @author s-kotaki
 *
 */
public class ReversiExecutor implements Runnable {

	/** ロガー */
	private static final ReversiLogger LOGGER = ReversiLogger.getInstance();
	/** リバーシメイン */
	private Reversi reversi;

	/**
	 * コンストラクタ
	 *
	 * @param reversi リバーシメイン
	 */
	public ReversiExecutor(Reversi reversi) {
		this.reversi = reversi;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (Thread.interrupted()) {
					// 停止検知
					throw new InterruptedException();
				}
				final boolean blackFlip = this.reversi.board.canFlip(ReversiColor.BLACK.getValue());
				final boolean whiteFlip = this.reversi.board.canFlip(ReversiColor.WHITE.getValue());
				if (!blackFlip && !whiteFlip) {
					// 双方パス
					break;
				}
				final long start = System.currentTimeMillis();
				final ReversiColor color = ReversiColor.fromValue(this.reversi.currentTurn);
				if ((color.equals(ReversiColor.BLACK) && blackFlip) || (color.equals(ReversiColor.WHITE) && whiteFlip)) {
					final ArtificalIntelligence intelligence = this.reversi.players.get(color);
					if (!(intelligence instanceof Human)) {
						this.reversi.setLabelThinking(MessageFormat.format("CPU({0})思考中…", color.getDisplayName()));
						Thread.sleep(500L);
					} else {
						this.reversi.buttonUndo.setDisable(false);
					}
					final EvalResult result = intelligence.execute(this.reversi.board.clone(), color,
							(color == ReversiColor.BLACK) ? this.reversi.depth1 : this.reversi.depth2,
							(color == ReversiColor.BLACK) ? this.reversi.finalDepth1 : this.reversi.finalDepth2);

					this.reversi.buttonUndo.setDisable(true);

					if (Thread.interrupted()) {
						// 停止検知
						throw new InterruptedException();
					}

					// 着手前の盤面
					final ReversiBoard oldBoard = this.reversi.board.clone();

					this.reversi.board.flip(result.getPoint(), color.getValue());
					this.reversi.setLabelCurrentTurn(
							MessageFormat.format("{0}：{1}", color.getDisplayName(), result.getPoint()));
					this.reversi.setLabelCurrentEval(Double.toString(result.getEvaluation()));
					this.reversi.setLabelThinking(null);

					this.reversi.repaintBoard(result.getPoint(), color, oldBoard, this.reversi.board);
					this.reversi.addHistory(new ReversiHistory(color, this.reversi.board));
				} else {
					this.reversi.setLabelThinking(MessageFormat.format("{0}はパスです", color.getDisplayName()));
					Thread.sleep(500L);
				}
				final long end = System.currentTimeMillis();
				LOGGER.out(MessageFormat.format("{0}の思考時間：{1}ms",
						ReversiColor.fromValue(this.reversi.currentTurn).getDisplayName(), (end - start)));

				this.reversi.currentTurn = ~this.reversi.currentTurn + 1;
			}

			final ArtificalIntelligence black = this.reversi.players.get(ReversiColor.BLACK);
			LOGGER.out(ReversiColor.BLACK + "の集計");
			LOGGER.out(MessageFormat.format("α枝狩り回数累計：{0}", black.getAlphaCutCount()));
			LOGGER.out(MessageFormat.format("β枝狩り回数累計：{0}", black.getBetaCutCount()));
			LOGGER.out(MessageFormat.format("αβ枝狩り回数累計：{0}", (black.getAlphaCutCount() + black.getBetaCutCount())));
			LOGGER.out(MessageFormat.format("評価回数累計：{0}", black.getEvalCount()));

			final ArtificalIntelligence white = this.reversi.players.get(ReversiColor.WHITE);
			LOGGER.out(ReversiColor.WHITE + "の集計");
			LOGGER.out(MessageFormat.format("α枝狩り回数累計：{0}", white.getAlphaCutCount()));
			LOGGER.out(MessageFormat.format("β枝狩り回数累計：{0}", white.getBetaCutCount()));
			LOGGER.out(MessageFormat.format("αβ枝狩り回数累計：{0}", (white.getAlphaCutCount() + white.getBetaCutCount())));
			LOGGER.out(MessageFormat.format("評価回数累計：{0}", white.getEvalCount()));

			// AIの評価値をクリア
			this.reversi.players.values().stream().forEach(p -> p.resetEvaluation());

			this.reversi.finish();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} catch (Throwable t) {
			t.printStackTrace();
			this.reversi.showErrorDialog(t.toString());
		} finally {
			this.reversi.destroy();
		}
	}

}
