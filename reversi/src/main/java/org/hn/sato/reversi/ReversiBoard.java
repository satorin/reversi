package org.hn.sato.reversi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 盤面
 *
 * @author s-kotaki
 *
 */
public class ReversiBoard extends ReversiAbstractBoard implements Cloneable, Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 3213782649076447968L;
	/** 盤面サイズ */
	public static final int SIZE = 8;
	/** 盤面 */
	private int[][] board;

	/**
	 * コンストラクタ
	 */
	public ReversiBoard() {
		this.board = new int[SIZE][SIZE];
		this.init();
	}

	/**
	 * 盤面の文字列情報(黒:● 白:○ 空:□)からビットボードを生成する
	 *
	 * @param boardString 盤面の文字列情報
	 */
	public ReversiBoard(String boardString) {
		this.board = new int[SIZE][SIZE];
		int index = 0;
		for (char c : boardString.toCharArray()) {
			if (c == '●') {
				this.board[index % 8][index / 8] = ReversiColor.BLACK.getValue();
				index++;
			} else if (c == '○') {
				this.board[index % 8][index / 8] = ReversiColor.WHITE.getValue();
				index++;
			} else if (c == '□') {
				index++;
			}
		}
	}

	/**
	 * 盤面を初期化する
	 */
	public void init() {
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				this.put(x, y, ReversiColor.NONE.getValue());
			}
		}

		this.put(ReversiPoint.D4, ReversiColor.WHITE.getValue());
		this.put(ReversiPoint.D5, ReversiColor.BLACK.getValue());
		this.put(ReversiPoint.E4, ReversiColor.BLACK.getValue());
		this.put(ReversiPoint.E5, ReversiColor.WHITE.getValue());
	}

	/**
	 * 盤面を設定する
	 *
	 * @param board 盤面
	 */
	public void setBoard(int[][] board) {
		this.board = board;
	}

	/**
	 * 盤面を返却する
	 *
	 * @return 盤面
	 */
	public int[][] getBoard() {
		return this.board.clone();
	}

	/**
	 * 盤面に色を設定する
	 *
	 * @param point 位置
	 * @param color 色
	 */
	public void put(ReversiPoint point, int color) {
		this.put(point.x, point.y, color);
	}

	/**
	 * 盤面に色を設定する
	 *
	 * @param x x軸の位置
	 * @param y y軸の位置
	 * @param color 色
	 */
	public void put(int x, int y, int color) {
		this.board[x][y] = color;
	}

	/**
	 * 指定した位置の色を返す
	 *
	 * @param point 位置
	 * @return 色
	 */
	public int getColor(ReversiPoint point) {
		return getColor(point.x, point.y);
	}

	/**
	 * x座標、y座標で指定した位置の色を返す
	 *
	 * @param x x座標の位置
	 * @param y y座標の位置
	 * @return 色
	 */
	public int getColor(int x, int y) {
		return this.board[x][y];
	}

	/**
	 * 指定した位置の色が指定した色と同じ色か判定する
	 *
	 * @param point 位置
	 * @param color 色
	 * @return 同じ色の場合 true
	 */
	public boolean isSameColor(ReversiPoint point, int color) {
		final int boardColor = this.getColor(point);
		return boardColor != ReversiColor.NONE.getValue() && boardColor == color;
	}

	/**
	 * 指定した位置の色が指定した色と反対の色(黒の場合白、白の場合黒)か判定する
	 *
	 * @param point 位置
	 * @param color 色
	 * @return 反対の色の場合 true
	 */
	public boolean isOpponentColor(ReversiPoint point, int color) {
		final int boardColor = this.getColor(point);
		return boardColor != ReversiColor.NONE.getValue() && boardColor != color;
	}

	/**
	 * 着手
	 * <p>
	 * 盤面は変更されます。
	 *
	 * @param point ポイント
	 * @param color 色
	 * @return 反転された位置のリスト
	 */
	public List<ReversiPoint> flip(ReversiPoint point, int color) {
		if (!this.canFlip(point, color)) {
			throw new IllegalArgumentException(
					ReversiColor.fromValue(color).name() + " は " + point + " に着手できません。\n" + this);
		}
		final List<ReversiPoint> flipPointList = new ArrayList<>();
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x == 0 && y == 0) {
					this.put(point, color);
				} else {
					final ReversiPoint nextPoint = point.move(x, y);
					if (!nextPoint.equals(ReversiPoint.OUT_OF_RANGE) && this.isOpponentColor(nextPoint, color)
							&& this.checkDirection(nextPoint, x, y, color)) {
						this.flipDirection(nextPoint, x, y, color, flipPointList);
					}
				}
			}
		}
		return flipPointList;
	}

	/**
	 * 盤面を指定した軸方向に反転する
	 * <p>
	 * このメソッドは指定した方向への反転が可能なことが担保されている前提での実装
	 *
	 * @param point ポイント
	 * @param amountX x座標の増分
	 * @param amountY y座標の増分
	 * @param color 色
	 * @param flipPointList 反転された位置のリスト
	 */
	private void flipDirection(ReversiPoint point, int amountX, int amountY, int color,
			List<ReversiPoint> flipPointList) {
		this.put(point, color);
		flipPointList.add(point);
		final ReversiPoint nextPoint = point.move(amountX, amountY);
		if (this.isOpponentColor(nextPoint, color)) {
			this.flipDirection(nextPoint, amountX, amountY, color, flipPointList);
		}
	}

	/**
	 * 着手可能かを返す
	 * <p>
	 * 盤面は変更されません。
	 *
	 * @param color 色
	 * @return 着手可能な場合 true
	 */
	public boolean canFlip(int color) {
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				if (this.canFlip(ReversiPoint.fromValue(x, y), color)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 着手可能数を返す
	 * <p>
	 * 盤面は変更されません。
	 *
	 * @param color 次に着手する色
	 * @return 着手可能数
	 */
	public int getFlipCount(int color) {
		int count = 0;
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				if (this.canFlip(ReversiPoint.fromValue(x, y), color)) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 指定した色の数を返す
	 *
	 * @param color 色
	 * @return カウント
	 */
	public int count(int color) {
		int count = 0;
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				if (this.getColor(x, y) == color) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 指定した位置に着手可能か判定する
	 * <p>
	 * 盤面は変更されません。
	 *
	 * @param point 位置
	 * @param color 色
	 * @return 着手可能な場合 true
	 */
	public boolean canFlip(ReversiPoint point, int color) {
		if (this.getColor(point) != ReversiColor.NONE.getValue()) {
			return false;
		}
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x == 0 && y == 0) {
					continue;
				}
				final ReversiPoint nextPoint = point.move(x, y);
				if (!ReversiPoint.OUT_OF_RANGE.equals(nextPoint) && this.isOpponentColor(nextPoint, color)) {
					final boolean result = this.checkDirection(nextPoint, x, y, color);
					if (result) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 単一方向への着手可能チェックを行う
	 *
	 * @param point 位置
	 * @param amountX x座標への増分
	 * @param amountY x座標への増分
	 * @param color 色
	 * @return 着手可能な場合 true
	 */
	private boolean checkDirection(ReversiPoint point, int amountX, int amountY, int color) {
		final ReversiPoint nextPoint = point.move(amountX, amountY);
		if (ReversiPoint.OUT_OF_RANGE.equals(nextPoint)) {
			return false;
		}
		final int checkColor = this.getColor(nextPoint);
		if (checkColor == ReversiColor.NONE.getValue()) {
			return false;
		}
		if (this.isOpponentColor(nextPoint, color)) {
			return this.checkDirection(nextPoint, amountX, amountY, color);
		}
		return true;
	}

	/**
	 * 着手可能な位置を配列で返す
	 * <p>
	 * 盤面は変更されません。
	 *
	 * @param color 次に着手する色
	 * @return 位置情報の配列
	 */
	public List<ReversiPoint> getFlipPointList(int color) {
		final List<ReversiPoint> flipPointList = new LinkedList<>();
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				final ReversiPoint point = ReversiPoint.fromValue(x, y);
				if (this.canFlip(point, color)) {
					flipPointList.add(point);
				}
			}
		}
		return flipPointList;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(SIZE * SIZE);
		// 盤面を描画するときだけ y -> x の順でループする
		for (int y = 0; y < ReversiBoard.SIZE; y++) {
			for (int x = 0; x < ReversiBoard.SIZE; x++) {
				sb.append(ReversiColor.fromValue(this.getColor(ReversiPoint.fromValue(x, y))));
			}
			sb.append("\n");
		}
		return sb.toString().replaceFirst("\n$", "");
	}

	@Override
	public ReversiBoard clone() {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			new ObjectOutputStream(baos).writeObject(this);
			return (ReversiBoard) new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray())).readObject();
		} catch (ClassNotFoundException | IOException e) {
			throw new RuntimeException(e);
		}
	}
}
