package org.hn.sato.reversi.ai.simulator;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ai.EvalResult;

/**
 * シミュレーターインターフェース
 *
 * @author s-kotaki
 *
 */
public interface Simulator {

	/**
	 * シミュレーションを実行する
	 *
	 * @param board 盤面
	 * @param color 着手色
	 * @param baseColor 基準色
	 * @param depth 探索深度
	 * @param alpha α
	 * @param beta β
	 * @return 評価結果
	 * @throws InterruptedException 割り込み
	 */
	EvalResult execute(ReversiBoard board, int color, int baseColor, int depth, double alpha, double beta) throws InterruptedException;

	/**
	 * 評価結果を返却する
	 *
	 * @param board 盤面
	 * @param baseColor 基準色
	 * @return 評価結果
	 */
	EvalResult eval(ReversiBoard board, int baseColor);

	/**
	 * 評価回数を返す
	 *
	 * @return 評価回数
	 */
	long getEvalCount();

	/**
	 * α枝狩り回数を返す
	 *
	 * @return α枝狩り回数
	 */
	long getAlphaCutCount();

	/**
	 * β枝狩り回数を返す
	 *
	 * @return β枝狩り回数
	 */
	long getBetaCutCount();

	/**
	 * αβ枝狩り回数を返す
	 *
	 * @return αβ枝狩り回数
	 */
	long getAlphaBetaCutCount();
}
