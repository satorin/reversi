package org.hn.sato.reversi;

import org.hn.sato.reversi.bit.BitBoard;

import lombok.Data;

/**
 * 履歴
 *
 * @author s-kotaki
 *
 */
@Data
public class ReversiHistory {

	/** 色 */
	private ReversiColor color;
	/** 盤面 */
	private BitBoard board;

	/**
	 * コンストラクタ
	 *
	 * @param color 色
	 * @param board 盤面
	 */
	public ReversiHistory(ReversiColor color, ReversiBoard board) {
		this.color = color;
		this.board = new BitBoard(board);
	}
}
