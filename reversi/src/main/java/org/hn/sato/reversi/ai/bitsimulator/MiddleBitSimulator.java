package org.hn.sato.reversi.ai.bitsimulator;

import java.util.Arrays;

import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitEvalResult;
import org.hn.sato.reversi.bit.BitMask;
import org.hn.sato.reversi.bit.SortableBitPoint;

/**
 * 中盤の戦略シミュレーター
 *
 * @author s-kotaki
 *
 */
public class MiddleBitSimulator extends AbstractBitSimulator {

	/** 確定石評価値 */
	private static final double VAL_FIXED = 15;
	/** 自石評価値 */
	private static final double VAL_DISC = -0.7;
	/** 余裕手評価値 */
	private static final double VAL_ADVANTAGE = 2.95;
	/** 隅評価値 */
	private static final double VAL_CORNER = 21.7;
	/** Ⅹ評価値 */
	private static final double VAL_X = -33.4;
	/** 単独Ｃ評価値 */
	private static final double VAL_C_UNI = -37.8;
	/** Ｃ評価値 */
	private static final double VAL_C = -8.9;
	/** 山(ピュア)評価値 */
	private static final double VAL_YAMA_PURE = 5;
	/** 山評価値 */
	private static final double VAL_YAMA = 2;
	/** 翼(ピュア)評価値 */
	private static final double VAL_TSUBASA_PURE = -10;
	/** 翼評価値 */
	private static final double VAL_TSUBASA = -5;
	/** ブロック(ピュア)評価値 */
	private static final double VAL_BLOCK_PURE = 2;
	/** ブロック評価値 */
	private static final double VAL_BLOCK = 1;
	/** 開放度評価値 */
	private static final double VAL_OPEN = -2.88;
	/** 手数評価値 */
	private static final double VAL_FLIP = 1.73;
	/** 辺の個数評価値 */
	private static final double VAL_LINE_COUNT = -1.73;
	/** 辺の形体評価値 */
	private static final double VAL_LINE_SHAPE = 2.93;

	@Override
	public final EvalResult eval(BitBoard board, BitBoard before, int color, int baseColor) {

		final BitEvalResult result = new BitEvalResult(board, baseColor);

		// 空白
		final long none = board.getNone();
		final int countNone = Long.bitCount(none);
		// 係数１(現在空白数の10% 序盤5.4～終盤0.9まで変動)
		final double coefficient1 = countNone * 0.1;
		// 係数２(2.3～4.55まで変動)
		final double coefficient2 = 5 * (1 - countNone * 0.01);
		// 係数３(50.6～100.1まで変動)
		final double coefficient3 = (100 - countNone) * 1.10;
		// 係数４(1～0まで変動)
		final double coefficient4 = countNone / 64.0;

		// 確定石の評価
		final long[] fixed = BitEval.evalFixedDisc(board);
		// 着手の評価
		final long[][] flips = BitEval.evalFlip(board);
		final long blackFlipCount = Long.bitCount(flips[0][1]);
		final long whiteFlipCount = Long.bitCount(flips[1][1]);
		// 隅の評価
		final long[] corners = BitEval.evalCorner(board);
		// Ⅹの評価
		final long[] xs = BitEval.evalX(board, fixed);
		// 山の評価
		final long[][] mountains = BitEval.evalMountain(board);
		// 翼の評価
		final long[][] wings = BitEval.evalWing(board, flips);
		// ブロックの評価
		final long[][] blocks = BitEval.evalBlock(board);
		// Ｃの評価
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		// 開放度の評価
		final long[] open = BitEval.evalOpen(board);
		// 辺の総合評価
		final int[][] lines = BitEval.evalLine(board, mountains, wings, blocks, fixed, cs, flips);

		// 黒の確定石の評価
		result.fixed[0] = Long.bitCount(fixed[0]) * VAL_FIXED;
		// 黒の数
		result.disc[0] = Long.bitCount(board.blackDiscs & ~fixed[0]) * VAL_DISC;
		// 黒の余裕手評価
		result.advantage[0] = (Long.bitCount(flips[0][2]) * Long.bitCount(flips[0][3]) * VAL_ADVANTAGE) / coefficient1;
		// 黒の隅の評価
		result.corner[0] = Long.bitCount(corners[0]) * VAL_CORNER * coefficient1;
		// 黒のⅩの評価
		result.x[0] = Long.bitCount(xs[0]) * VAL_X;
		// 黒のＣの評価
		result.c[0] = Long.bitCount(cs[0][0]) * VAL_C + Long.bitCount(cs[0][1]) * VAL_C_UNI;
		// 黒の山の評価
		result.mountain[0] = (Long.bitCount(mountains[0][0] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA
				+ (Long.bitCount(mountains[0][1] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA_PURE;
		// 黒の翼の評価
		result.wing[0] = Long.bitCount(wings[0][0] & BitMask.ALL_CS.mask) * VAL_TSUBASA
				+ Long.bitCount(wings[0][1] & BitMask.ALL_CS.mask) * VAL_TSUBASA_PURE;
		// 黒のブロックの評価
		result.block[0] = (Long.bitCount(blocks[0][0]) >>> 2) * VAL_BLOCK + (Long.bitCount(blocks[0][1]) >>> 3) * VAL_BLOCK_PURE;
		// 黒の開放度評価
		result.open[0] = (Long.bitCount(open[0]) * VAL_OPEN) * coefficient4;
		// 黒の手数の評価
		result.flip[0] = blackFlipCount * Long.bitCount(flips[0][3]) * VAL_FLIP / coefficient1;
		// 相手の手数が少ないほど高い評価になるようにする
		if (whiteFlipCount < blackFlipCount) {
			// 黒のほうが手数が多い
			result.flip[0] += coefficient3 / (whiteFlipCount + 1);
		}
		// 隅着手評価
		if (color == ReversiColor.BLACK.getValue()) {
			// 黒の前回の隅着手可能位置
			final long beforeFlip = before.getFlipPoint(ReversiColor.BLACK.getValue()) & BitMask.ALL_CORNERS.mask;
			// 黒の隅取得可否(以前から打てるところのみ評価対象とする)
			result.enableGetCorner[0] = Long.bitCount(flips[0][0] & BitMask.ALL_CORNERS.mask & beforeFlip) * VAL_CORNER;
		} else {
			// 黒の隅取得可否(全て評価対象)
			result.enableGetCorner[0] = Long.bitCount(flips[0][0] & BitMask.ALL_CORNERS.mask) * VAL_CORNER;
		}
		// 黒の辺の総合評価
		result.line[0] = lines[0][0] * VAL_LINE_COUNT + lines[0][1] * VAL_LINE_SHAPE;

		// 白の確定石の評価
		result.fixed[1] = Long.bitCount(fixed[1]) * VAL_FIXED;
		// 白の数
		result.disc[1] = Long.bitCount(board.whiteDiscs & ~fixed[1]) * VAL_DISC;
		// 白の余裕手評価
		result.advantage[1] = (Long.bitCount(flips[1][2]) * Long.bitCount(flips[1][3]) * VAL_ADVANTAGE) / coefficient1;
		// 白の隅の評価
		result.corner[1] = Long.bitCount(corners[1]) * VAL_CORNER * coefficient1;
		// 白のⅩの評価
		result.x[1] = Long.bitCount(xs[1]) * VAL_X;
		// 白のＣの評価
		result.c[1] = Long.bitCount(cs[1][0]) * VAL_C + Long.bitCount(cs[1][1]) * VAL_C_UNI;
		// 白の山の評価
		result.mountain[1] = (Long.bitCount(mountains[1][0] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA
				+ (Long.bitCount(mountains[1][1] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA_PURE;
		// 白の翼の評価
		result.wing[1] = Long.bitCount(wings[1][0] & BitMask.ALL_CS.mask) * VAL_TSUBASA
				+ Long.bitCount(wings[1][1] & BitMask.ALL_CS.mask) * VAL_TSUBASA_PURE;
		// 白のブロックの評価
		result.block[1] = (Long.bitCount(blocks[1][0]) >>> 2) * VAL_BLOCK + (Long.bitCount(blocks[1][1]) >>> 3) * VAL_BLOCK_PURE;
		// 白の開放度評価
		result.open[1] = (Long.bitCount(open[1]) * VAL_OPEN) * coefficient4;
		// 白の手数の評価
		result.flip[1] = whiteFlipCount * Long.bitCount(flips[1][3]) * VAL_FLIP / coefficient1;
		// 相手の手数が少ないほど高い評価になるようにする
		if (blackFlipCount < whiteFlipCount) {
			// 白のほうが手数が多い
			result.flip[0] += coefficient3 / (blackFlipCount + 1);
		}
		// 隅着手評価
		if (color == ReversiColor.WHITE.getValue()) {
			// 白の前回の隅着手可能位置
			final long beforeFlip = before.getFlipPoint(ReversiColor.WHITE.getValue()) & BitMask.ALL_CORNERS.mask;
			// 白の隅取得可否(以前から打てるところのみ評価対象とする)
			result.enableGetCorner[1] = Long.bitCount(flips[1][0] & BitMask.ALL_CORNERS.mask & beforeFlip) * VAL_CORNER;
		} else {
			// 白の隅取得可否(全て評価対象)
			result.enableGetCorner[1] = Long.bitCount(flips[1][0] & BitMask.ALL_CORNERS.mask) * VAL_CORNER;
		}
		// 白の辺の総合評価
		result.line[1] = lines[1][0] * VAL_LINE_COUNT + lines[1][1] * VAL_LINE_SHAPE;

		result.setEvaluation(BitEval.stddev(result.calcEvaluation(), baseColor));

		return result;
	}

	@Override
	public long[] sortedFlip(final BitBoard board, final int color) {
		final long flipPoint = board.getFlipPoint(color);
		final long corners = flipPoint & BitMask.ALL_CORNERS.mask;
		final long normals = flipPoint & ~corners;

		// 開放度順のソート
		final long[] normalArray = Arrays.stream(BitBoardUtil.toPointArray(normals)).mapToObj(point -> {
			final int openRatio = BitEval.calcOpenRatio(board, point, color);
			return new SortableBitPoint(openRatio, point);
		}).sorted((a, b) -> Double.compare(a.sortKey, b.sortKey)).mapToLong(point -> point.point).toArray();
		final long[] cornerArray = BitBoardUtil.toPointArray(corners);
		final long[] pointArray = new long[Long.bitCount(flipPoint)];
		System.arraycopy(cornerArray, 0, pointArray, 0, cornerArray.length);
		System.arraycopy(normalArray, 0, pointArray, cornerArray.length, normalArray.length);

		// １手打って確定石の差異が最も大きくなるもの順にソートする
		return Arrays.stream(pointArray).mapToObj(point -> {
			final BitBoard newBoard = board.flip(point, color);
			final long[] fixed = BitEval.evalFixedDisc(newBoard);
			final int gap = (ReversiColor.BLACK.getValue() == color) ? Long.bitCount(fixed[0]) - Long.bitCount(fixed[1])
					: Long.bitCount(fixed[1]) - Long.bitCount(fixed[0]);
			return new SortableBitPoint(gap, point);
		}).sorted((a, b) -> Double.compare(b.sortKey, a.sortKey)).mapToLong(point -> point.point).toArray();
	}
}
