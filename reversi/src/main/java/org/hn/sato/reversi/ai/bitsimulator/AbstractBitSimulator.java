package org.hn.sato.reversi.ai.bitsimulator;

import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitEvalResult;

import lombok.Getter;

/**
 * 盤面評価を行うシミュレーター
 *
 * @author s-kotaki
 *
 */
public abstract class AbstractBitSimulator implements BitSimulator {

	/** 評価回数 */
	@Getter
	private long evalCount;
	/** αによる枝狩り回数 */
	@Getter
	private long alphaCutCount;
	/** βによる枝狩り回数 */
	@Getter
	private long betaCutCount;
	/** キャッシュヒット回数 */
	@Getter
	private long cacheHitCount;

	@Override
	public final EvalResult execute(BitBoard board, BitBoard before, int color, int baseColor, int depth, double alpha,
			double beta) throws InterruptedException {

		if (board.countNone() == 0) {
			// 最終局面まで到達した場合
			this.evalCount++;
			if (this instanceof FinalBitSimulator) {
				return this.eval(board, before, color, baseColor);
			}
			return new FinalBitSimulator().eval(board, before, color, baseColor);
		}

		int turn = color;
		final boolean pass = board.isPass(turn);
		if (depth < 1) {
			this.evalCount++;
			final int evalColor = !pass ? color : -color;
			return this.eval(board, before, evalColor, baseColor);
		}

		if (pass) {
			// パスの場合
			turn = -turn;
			if (board.isPass(turn)) {
				// 双方パスの場合
				this.evalCount++;
				if (this instanceof FinalBitSimulator) {
					return this.eval(board, before, color, baseColor);
				}
				return new FinalBitSimulator().eval(board, before, color, baseColor);
			}
		}

		EvalResult evalResult;
		if (turn == baseColor) {
			evalResult = BitEvalResult.min(board, baseColor);
		} else {
			evalResult = BitEvalResult.max(board, baseColor);
		}

		final int newDepth = depth - 1;
		for (long point : this.sortedFlip(board, turn)) {
			final BitBoard newBoard = board.flip(point, turn);
			final EvalResult result = this.execute(newBoard, board, -turn, baseColor, newDepth, alpha, beta);
			final double eval = result.getEvaluation();
			// Min-Max法による評価判定
			if (turn == baseColor) {
				if (beta < eval) {
					// βカット
					betaCutCount++;
					return result;
				}
				if (evalResult.getEvaluation() < eval) {
					// αを更新(大きければ)
					alpha = Math.max(alpha, eval);
					// 現時点での自分の最善手評価結果を保持
					evalResult = result;
				}
			} else {
				if (eval < alpha) {
					// αカット
					alphaCutCount++;
					return result;
				}
				if (eval < evalResult.getEvaluation()) {
					// βを更新(小さければ)
					beta = Math.min(beta, eval);
					// 現時点での相手の最善手評価結果を保持
					evalResult = result;
				}
			}
		}
		return evalResult;
	}

	@Override
	public long getAlphaBetaCutCount() {
		return this.alphaCutCount + this.betaCutCount;
	}
}
