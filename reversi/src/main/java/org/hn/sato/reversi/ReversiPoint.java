package org.hn.sato.reversi;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 盤面位置情報
 *
 * @author s-kotaki
 *
 */
public enum ReversiPoint {

	/** A1(0, 0) */
	A1(0, 0),
	/** A2(0, 1) */
	A2(0, 1),
	/** A3(0, 2) */
	A3(0, 2),
	/** A4(0, 3) */
	A4(0, 3),
	/** A5(0, 4) */
	A5(0, 4),
	/** A6(0, 5) */
	A6(0, 5),
	/** A7(0, 6) */
	A7(0, 6),
	/** A8(0, 7) */
	A8(0, 7),
	/** B1(1, 0) */
	B1(1, 0),
	/** B2(1, 1) */
	B2(1, 1),
	/** B3(1, 2) */
	B3(1, 2),
	/** B4(1, 3) */
	B4(1, 3),
	/** B5(1, 4) */
	B5(1, 4),
	/** B6(1, 5) */
	B6(1, 5),
	/** B7(1, 6) */
	B7(1, 6),
	/** B8(1, 7) */
	B8(1, 7),
	/** C1(2, 0) */
	C1(2, 0),
	/** C2(2, 1) */
	C2(2, 1),
	/** C3(2, 2) */
	C3(2, 2),
	/** C4(2, 3) */
	C4(2, 3),
	/** C5(2, 4) */
	C5(2, 4),
	/** C6(2, 5) */
	C6(2, 5),
	/** C7(2, 6) */
	C7(2, 6),
	/** C8(2, 7) */
	C8(2, 7),
	/** D1(3, 0) */
	D1(3, 0),
	/** D2(3, 1) */
	D2(3, 1),
	/** D3(3, 2) */
	D3(3, 2),
	/** D4(3, 3) */
	D4(3, 3),
	/** D5(3, 4) */
	D5(3, 4),
	/** D6(3, 5) */
	D6(3, 5),
	/** D7(3, 6) */
	D7(3, 6),
	/** D8(3, 7) */
	D8(3, 7),
	/** E1(4, 0) */
	E1(4, 0),
	/** E2(4, 1) */
	E2(4, 1),
	/** E3(4, 2) */
	E3(4, 2),
	/** E4(4, 3) */
	E4(4, 3),
	/** E5(4, 4) */
	E5(4, 4),
	/** E6(4, 5) */
	E6(4, 5),
	/** E7(4, 6) */
	E7(4, 6),
	/** E8(4, 7) */
	E8(4, 7),
	/** F1(5, 0) */
	F1(5, 0),
	/** F2(5, 1) */
	F2(5, 1),
	/** F3(5, 2) */
	F3(5, 2),
	/** F4(5, 3) */
	F4(5, 3),
	/** F5(5, 4) */
	F5(5, 4),
	/** F6(5, 5) */
	F6(5, 5),
	/** F7(5, 6) */
	F7(5, 6),
	/** F8(5, 7) */
	F8(5, 7),
	/** G1(6, 0) */
	G1(6, 0),
	/** G2(6, 1) */
	G2(6, 1),
	/** G3(6, 2) */
	G3(6, 2),
	/** G4(6, 3) */
	G4(6, 3),
	/** G5(6, 4) */
	G5(6, 4),
	/** G6(6, 5) */
	G6(6, 5),
	/** G7(6, 6) */
	G7(6, 6),
	/** G8(6, 7) */
	G8(6, 7),
	/** H1(7, 0) */
	H1(7, 0),
	/** H2(7, 1) */
	H2(7, 1),
	/** H3(7, 2) */
	H3(7, 2),
	/** H4(7, 3) */
	H4(7, 3),
	/** H5(7, 4) */
	H5(7, 4),
	/** H6(7, 5) */
	H6(7, 5),
	/** H7(7, 6) */
	H7(7, 6),
	/** H8(7, 7) */
	H8(7, 7),
	/** 範囲外 */
	OUT_OF_RANGE(-1, -1);

	/** キャッシュ */
	private static final Map<Long, ReversiPoint> cache = new HashMap<>();

	static {
		Arrays.stream(ReversiPoint.values()).forEach(p -> cache.put(((long) p.x) << 32 | p.y, p));
	}

	/** 横軸 */
	public final int x;
	/** 縦軸 */
	public final int y;

	/**
	 * コンストラクタ
	 *
	 * @param x x軸
	 * @param y y軸
	 */
	ReversiPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * 現在の位置をx, yで指定した分移動した位置を返却する
	 *
	 * @param amountX x軸の移動分
	 * @param amountY y軸の移動分
	 * @return 移動した位置
	 */
	public ReversiPoint move(int amountX, int amountY) {
		if (amountX == 0 && amountY == 0) {
			return this;
		}
		int x = this.x + amountX;
		int y = this.y + amountY;
		return fromValue(x, y);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.name());
		sb.append("(");
		sb.append(this.x);
		sb.append(",");
		sb.append(this.y);
		sb.append(")");
		return sb.toString();
	}

	/**
	 * x軸、y軸の数値情報から位置情報を返却する
	 *
	 * @param x x軸の位置
	 * @param y y軸の位置
	 * @return 位置情報
	 */
	public static ReversiPoint fromValue(int x, int y) {
		final long key = ((long) x) << 32 | y;
		if (cache.containsKey(key)) {
			return cache.get(key);
		}
		return ReversiPoint.OUT_OF_RANGE;
	}

	/**
	 * x軸の位置、y軸の位置が盤面上の妥当な範囲にあるか判定する
	 *
	 * @param x x軸の位置
	 * @param y y軸の位置
	 * @return 0 <= x <= 7, 0 <= y <= 7 を満たす場合 true
	 */
	public static boolean isValidRange(int x, int y) {
		return (0 <= x && x <= 7) && (0 <= y && y <= 7);
	}
}
