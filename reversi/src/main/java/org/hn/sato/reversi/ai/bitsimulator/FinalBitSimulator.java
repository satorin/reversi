package org.hn.sato.reversi.ai.bitsimulator;

import java.util.Arrays;

import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitEvalResult;
import org.hn.sato.reversi.bit.SortableBitPoint;

/**
 * 盤面の最終読みを行うシミュレーター
 *
 * @author s-kotaki
 *
 */
public class FinalBitSimulator extends AbstractBitSimulator {

	@Override
	public final EvalResult eval(BitBoard board, BitBoard before, int color, int baseColor) {
		final BitEvalResult result = new BitEvalResult(board, baseColor);
		final int black = board.countBlack();
		final int white = board.countWhite();
		result.fixed[0] = black;
		result.fixed[1] = white;
		result.setEvaluation(BitEval.finalCount(black, white, baseColor));
		return result;
	}

	@Override
	public long[] sortedFlip(BitBoard board, int color) {
		// １手打って確定石の差異が最も大きくなるもの順にソートする
		return Arrays.stream(BitBoardUtil.toPointArray(board.getFlipPoint(color))).mapToObj(point -> {
			final BitBoard newBoard = board.flip(point, color);
			final long[] fixed = BitEval.evalFixedDisc(newBoard);
			final int gap = (ReversiColor.BLACK.getValue() == color) ? Long.bitCount(fixed[0]) - Long.bitCount(fixed[1]) : Long.bitCount(fixed[1]) - Long.bitCount(fixed[0]);
			return new SortableBitPoint(gap, point);
		}).sorted((a, b) -> Double.compare(b.sortKey, a.sortKey)).mapToLong(point -> point.point).toArray();
	}
}
