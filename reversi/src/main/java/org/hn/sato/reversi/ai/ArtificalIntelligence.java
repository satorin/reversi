package org.hn.sato.reversi.ai;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;

/**
 * AIインターフェース
 *
 * @author s-kotaki
 *
 */
public interface ArtificalIntelligence {

	/**
	 * 着手
	 *
	 * @param board 盤面
	 * @param color 色
	 * @param depth 読みの深さ
	 * @param finalDepth 最終読みの深さ
	 * @return 評価結果
	 * @throws InterruptedException 割り込み
	 */
	EvalResult execute(ReversiBoard board, ReversiColor color, int depth, int finalDepth) throws InterruptedException;

	/**
	 * 【検証用】評価回数を返す
	 *
	 * @return 評価回数
	 */
	long getEvalCount();

	/**
	 * 【検証用】αカット回数を返す
	 *
	 * @return αカット回数
	 */
	long getAlphaCutCount();

	/**
	 * 【検証用】βカット回数を返す
	 *
	 * @return βカット回数
	 */
	long getBetaCutCount();

	/**
	 * 評価情報をクリアする
	 */
	void resetEvaluation();
}
