package org.hn.sato.reversi.bit;

import java.io.Serializable;

import lombok.ToString;

/**
 * 着手位置ソート用クラス
 *
 * @author s-kotaki
 *
 */
@ToString
public class SortableBitPoint implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = -7185324396492059507L;
	/** ソートキー */
	public double sortKey;
	/** 着手位置 */
	public long point;

	/**
	 * コンストラクタ
	 *
	 * @param sortKey 評価値
	 * @param point 着手位置
	 */
	public SortableBitPoint(double sortKey, long point) {
		this.sortKey = sortKey;
		this.point = point;
	}
}
