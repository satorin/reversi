package org.hn.sato.reversi.ai.bitsimulator;

import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitMask;
import org.hn.sato.reversi.bit.BitPoint;

/**
 * 評価関数
 *
 * @author s-kotaki
 *
 */
public final class BitEval {

	/**
	 * 指定した位置に指定した色で着手した場合の開放度を計算する
	 * <p>
	 * 純粋な開放度
	 *
	 * @param board 盤面
	 * @param point 着手位置
	 * @param color 色
	 * @return 開放度評価値
	 */
	public static int calcOpenRatio(BitBoard board, long point, int color) {
		final BitBoard newBoard = board.flip(point, color);
		final long moved = board.blackDiscs ^ newBoard.blackDiscs & ~point;
		final long none = newBoard.getNone();

		// 開放度
		long open = 0L;

		// 左上方向
		open |= (moved & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		open |= (moved & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		open |= (moved & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		open |= (moved & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		open |= (moved & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		open |= (moved & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		open |= (moved & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		open |= (moved & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;

		return Long.bitCount(open & none);
	}

	/**
	 * 指定した位置に指定した色で着手した場合の開放度を計算する
	 * <p>
	 * 開放位置がⅩの場合は開放度に加算しない
	 *
	 * @param board 盤面
	 * @param point 着手位置
	 * @param color 色
	 * @return 開放度評価値
	 */
	public static int calcOpenRatioEx(BitBoard board, long point, int color) {
		final BitBoard newBoard = board.flip(point, color);
		final long moved = board.blackDiscs ^ newBoard.blackDiscs & ~point;
		final long none = newBoard.getNone();

		// 開放度
		long open = 0L;

		// 左上方向
		open |= (moved & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		open |= (moved & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		open |= (moved & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		open |= (moved & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		open |= (moved & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		open |= (moved & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		open |= (moved & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		open |= (moved & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;

		// Ⅹ打ちは除外
		return Long.bitCount(open & none & ~BitMask.ALL_XS.mask);
	}

	/**
	 * 指定した位置に指定した色で着手した場合の開放度を計算する
	 * <p>
	 * 開放位置がⅩ、相手が着手できない場合は開放度に加算せず、返した石の数を加算する
	 *
	 * @param board 盤面
	 * @param point 着手位置
	 * @param color 色
	 * @return 開放度評価値
	 */
	public static int calcOpenRatioEx2(BitBoard board, long point, int color) {
		final BitBoard newBoard = board.flip(point, color);
		final long moved = board.blackDiscs ^ newBoard.blackDiscs & ~point;
		final long none = newBoard.getNone();

		// 開放度
		long open = 0L;

		// 左上方向
		open |= (moved & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		open |= (moved & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		open |= (moved & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		open |= (moved & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		open |= (moved & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		open |= (moved & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		open |= (moved & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		open |= (moved & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;

		// 相手の着手可能位置
		final long opFlip = newBoard.getFlipPoint(-color);

		// Ⅹ打ちと相手が着手できない位置は除外
		return Long.bitCount(open & none & ~BitMask.ALL_XS.mask & ~opFlip) + Long.bitCount(moved);
	}

	/**
	 * 確定石の評価関数
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @return 確定石評価値 [0]:黒 [1]:白
	 */
	public static long[] evalFixedDisc(BitBoard board) {
		long blackFixed = 0L;
		long whiteFixed = 0L;
		final long exist = board.blackDiscs | board.whiteDiscs;
		long mask;
		long t;

		// 上辺
		if ((exist & BitMask.UPPER_LINE.mask) == BitMask.UPPER_LINE.mask) {
			// 上辺１段が全て埋まっている場合
			blackFixed |= board.blackDiscs & BitMask.UPPER_LINE.mask;
			whiteFixed |= board.whiteDiscs & BitMask.UPPER_LINE.mask;
		} else {
			// 上辺が埋まっていない場合
			// 左上隅⇒右上隅
			for (t = BitPoint.A1.mask; (board.blackDiscs & t) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
			for (t = BitPoint.A1.mask; (board.whiteDiscs & t) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
			// 右上隅⇒左上隅
			for (t = BitPoint.H1.mask; (board.blackDiscs & t) != 0; t = t << 1) {
				blackFixed |= t;
			}
			for (t = BitPoint.H1.mask; (board.whiteDiscs & t) != 0; t = t << 1) {
				whiteFixed |= t;
			}
		}
		// 下辺
		if ((exist & BitMask.LOWER_LINE.mask) == BitMask.LOWER_LINE.mask) {
			// 下辺が全て埋まっている場合
			blackFixed |= board.blackDiscs & BitMask.LOWER_LINE.mask;
			whiteFixed |= board.whiteDiscs & BitMask.LOWER_LINE.mask;
		} else {
			// 下辺が埋まっていない場合
			// 左下隅⇒右下隅
			for (t = BitPoint.A8.mask; (board.blackDiscs & t) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
			for (t = BitPoint.A8.mask; (board.whiteDiscs & t) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
			// 右下隅⇒左下隅
			for (t = BitPoint.H8.mask; (board.blackDiscs & t) != 0; t = t << 1) {
				blackFixed |= t;
			}
			for (t = BitPoint.H8.mask; (board.whiteDiscs & t) != 0; t = t << 1) {
				whiteFixed |= t;
			}
		}
		// 左辺
		if ((exist & BitMask.LEFT_LINE.mask) == BitMask.LEFT_LINE.mask) {
			// 左辺が全て埋まっている場合
			blackFixed |= board.blackDiscs & BitMask.LEFT_LINE.mask;
			whiteFixed |= board.whiteDiscs & BitMask.LEFT_LINE.mask;
		} else {
			// 左辺が埋まっていない場合
			// 左上隅⇒左下隅
			for (t = BitPoint.A1.mask; (board.blackDiscs & t) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
			for (t = BitPoint.A1.mask; (board.whiteDiscs & t) != 0; t = t >>> 8) {
				whiteFixed |= t;
			}
			// 左下隅⇒左上隅
			for (t = BitPoint.A8.mask; (board.blackDiscs & t) != 0; t = t << 8) {
				blackFixed |= t;
			}
			for (t = BitPoint.A8.mask; (board.whiteDiscs & t) != 0; t = t << 8) {
				whiteFixed |= t;
			}
		}
		// 右辺
		if ((exist & BitMask.RIGHT_LINE.mask) == BitMask.RIGHT_LINE.mask) {
			// 右辺が全て埋まっている場合
			blackFixed |= board.blackDiscs & BitMask.RIGHT_LINE.mask;
			whiteFixed |= board.whiteDiscs & BitMask.RIGHT_LINE.mask;
		} else {
			// 右辺が埋まっていない場合
			// 右上隅⇒右下隅
			for (t = BitPoint.H1.mask; (board.blackDiscs & t) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
			for (t = BitPoint.H1.mask; (board.whiteDiscs & t) != 0; t = t >>> 8) {
				whiteFixed |= t;
			}
			// 右下隅⇒右上隅
			for (t = BitPoint.H8.mask; (board.blackDiscs & t) != 0; t = t << 8) {
				blackFixed |= t;
			}
			for (t = BitPoint.H8.mask; (board.whiteDiscs & t) != 0; t = t << 8) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed & BitMask.UPPER_LEFT_3CORNERS.mask) == BitMask.UPPER_LEFT_3CORNERS.mask) {
			// ２段目左上隅:黒
			// 左⇒右(B2から右に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.H2.mask;
			for (t = BitPoint.B2.mask; ((mask & t) & (blackFixed & (t << 7)) >>> 7) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
			// 上⇒下(B2から下に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.B8.mask;
			for (t = BitPoint.B2.mask; ((mask & t) & (blackFixed & (t >>> 7)) << 7) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed & BitMask.UPPER_LEFT_3CORNERS.mask) == BitMask.UPPER_LEFT_3CORNERS.mask) {
			// ２段目左上隅:白
			// 左⇒右(B2から右に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.H2.mask;
			for (t = BitPoint.B2.mask; ((mask & t) & (whiteFixed & (t << 7)) >>> 7) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
			// 上⇒下(B2から下に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.B8.mask;
			for (t = BitPoint.B2.mask; ((mask & t) & (whiteFixed & (t >>> 7)) << 7) != 0; t = t >>> 8) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed & BitMask.UPPER_RIGHT_3CORNERS.mask) == BitMask.UPPER_RIGHT_3CORNERS.mask) {
			// ２段目右上隅:黒
			// 右⇒左(G2から左に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.A2.mask;
			for (t = BitPoint.G2.mask; ((mask & t) & (blackFixed & (t << 9)) >>> 9) != 0; t = t << 1) {
				blackFixed |= t;
			}
			// 上⇒下(G2から下に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.G8.mask;
			for (t = BitPoint.G2.mask; ((mask & t) & (blackFixed & (t >>> 8)) << 8) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed & BitMask.UPPER_RIGHT_3CORNERS.mask) == BitMask.UPPER_RIGHT_3CORNERS.mask) {
			// ２段目右上隅:白
			// 右⇒左(G2から左に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.A2.mask;
			for (t = BitPoint.G2.mask; ((mask & t) & (whiteFixed & (t << 9)) >>> 9) != 0; t = t << 1) {
				whiteFixed |= t;
			}
			// 上⇒下(G2から下に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.G8.mask;
			for (t = BitPoint.G2.mask; ((mask & t) & (whiteFixed & (t >>> 8)) << 8) != 0; t = t >>> 8) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed & BitMask.LOWER_LEFT_3CORNERS.mask) == BitMask.LOWER_LEFT_3CORNERS.mask) {
			// ２段目左下隅:黒
			// 左⇒右(B7から右に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.H7.mask;
			for (t = BitPoint.B7.mask; ((mask & t) & (blackFixed & (t >>> 9)) << 9) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
			// 下⇒上(B7から上に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.B1.mask;
			for (t = BitPoint.B7.mask; ((mask & t) & (blackFixed & (t << 9)) >>> 9) != 0; t = t << 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed & BitMask.LOWER_LEFT_3CORNERS.mask) == BitMask.LOWER_LEFT_3CORNERS.mask) {
			// ２段目左下隅:白
			// 左⇒右(B7から右に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.H7.mask;
			for (t = BitPoint.B7.mask; ((mask & t) & (whiteFixed & (t >>> 9)) << 9) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
			// 下⇒上(B7から上に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.B1.mask;
			for (t = BitPoint.B7.mask; ((mask & t) & (whiteFixed & (t << 9)) >>> 9) != 0; t = t << 8) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed & BitMask.LOWER_RIGHT_3CORNERS.mask) == BitMask.LOWER_RIGHT_3CORNERS.mask) {
			// ２段目右下隅:黒
			// 右⇒左(G7から左に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.A7.mask;
			for (t = BitPoint.G7.mask; ((mask & t) & (blackFixed & (t >>> 7)) << 7) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
			// 下⇒上(G7から上に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.G1.mask;
			for (t = BitPoint.G7.mask; ((mask & t) & (blackFixed & (t << 7)) >>> 7) != 0; t = t << 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed & BitMask.LOWER_RIGHT_3CORNERS.mask) == BitMask.LOWER_RIGHT_3CORNERS.mask) {
			// ２段目右下隅:白
			// 右⇒左(G7から左に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.A7.mask;
			for (t = BitPoint.G7.mask; ((mask & t) & (whiteFixed & (t >>> 7)) << 7) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
			// 下⇒上(G7から上に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.G1.mask;
			for (t = BitPoint.G7.mask; ((mask & t) & (whiteFixed & (t << 7)) >>> 7) != 0; t = t << 8) {
				whiteFixed |= t;
			}
		}

		if ((blackFixed
				& BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE1.mask) == BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目左上隅1:黒
			// 左⇒右(B3から右に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.H3.mask;
			for (t = BitPoint.B3.mask; ((mask & t) & (blackFixed & (t << 7)) >>> 7) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE1.mask) == BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目左上隅1:白
			// 左⇒右(B3から右に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.H3.mask;
			for (t = BitPoint.B3.mask; ((mask & t) & (whiteFixed & (t << 7)) >>> 7) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE2.mask) == BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目左上隅2:黒
			// 上⇒下(C2から下に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.C8.mask;
			for (t = BitPoint.C2.mask; ((mask & t) & (blackFixed & (t >>> 7)) << 7) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE2.mask) == BitMask.UPPER_LEFT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目左上隅2:白
			// 上⇒下(C2から下に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.C8.mask;
			for (t = BitPoint.C2.mask; ((mask & t) & (whiteFixed & (t >>> 7)) << 7) != 0; t = t >>> 8) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE1.mask) == BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目右上隅1:黒
			// 右⇒左(G3から左に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.A3.mask;
			for (t = BitPoint.G3.mask; ((mask & t) & (blackFixed & (t << 9)) >>> 9) != 0; t = t << 1) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE1.mask) == BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目右上隅1:白
			// 右⇒左(G3から左に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.A3.mask;
			for (t = BitPoint.G3.mask; ((mask & t) & (blackFixed & (t << 9)) >>> 9) != 0; t = t << 1) {
				blackFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE2.mask) == BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目右上隅2:黒
			// 上⇒下(F2から下に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.F8.mask;
			for (t = BitPoint.F2.mask; ((mask & t) & (blackFixed & (t >>> 8)) << 8) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE2.mask) == BitMask.UPPER_RIGHT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目右上隅2:白
			// 上⇒下(F2から下に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.F8.mask;
			for (t = BitPoint.F2.mask; ((mask & t) & (blackFixed & (t >>> 8)) << 8) != 0; t = t >>> 8) {
				blackFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE1.mask) == BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目左下隅1:黒
			// 左⇒右(B6から右に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.H6.mask;
			for (t = BitPoint.B6.mask; ((mask & t) & (blackFixed & (t >>> 9)) << 9) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE1.mask) == BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目左下隅1:白
			// 左⇒右(B6から右に向かって処理。対象の右下が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.H6.mask;
			for (t = BitPoint.B6.mask; ((mask & t) & (whiteFixed & (t >>> 9)) << 9) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE2.mask) == BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目左下隅2:黒
			// 下⇒上(C7から上に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.C1.mask;
			for (t = BitPoint.C7.mask; ((mask & t) & (blackFixed & (t << 9)) >>> 9) != 0; t = t << 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE2.mask) == BitMask.LOWER_LEFT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目左下隅2:白
			// 下⇒上(C7から上に向かって処理。対象の左上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.C1.mask;
			for (t = BitPoint.C7.mask; ((mask & t) & (whiteFixed & (t << 9)) >>> 9) != 0; t = t << 8) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE1.mask) == BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目右下隅1:黒
			// 右⇒左(G6から左に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.A6.mask;
			for (t = BitPoint.G6.mask; ((mask & t) & (blackFixed & (t >>> 7)) << 7) != 0; t = t >>> 1) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE1.mask) == BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE1.mask) {
			// ３段目右下隅1:白
			// 右⇒左(G6から左に向かって処理。対象の左下が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.A6.mask;
			for (t = BitPoint.G6.mask; ((mask & t) & (whiteFixed & (t >>> 7)) << 7) != 0; t = t >>> 1) {
				whiteFixed |= t;
			}
		}
		if ((blackFixed
				& BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE2.mask) == BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目右下隅2:黒
			// 下⇒上(F7から上に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.blackDiscs & ~BitPoint.F1.mask;
			for (t = BitPoint.F7.mask; ((mask & t) & (blackFixed & (t << 7)) >>> 7) != 0; t = t << 8) {
				blackFixed |= t;
			}
		} else if ((whiteFixed
				& BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE2.mask) == BitMask.LOWER_RIGHT_3CORNERS_SECOND_TYPE2.mask) {
			// ３段目右下隅2:白
			// 下⇒上(F7から上に向かって処理。対象の右上が自石の確定石だった場合は確定石と判断)
			mask = board.whiteDiscs & ~BitPoint.F1.mask;
			for (t = BitPoint.F7.mask; ((mask & t) & (whiteFixed & (t << 7)) >>> 7) != 0; t = t << 8) {
				whiteFixed |= t;
			}
		}

		// 空位置からの全方位検索
		final long none = ~exist;
		if (Long.bitCount(none) < 13) {
			// 空が12個以下のときのみこの処理を行う
			// 13個以上空があるときは、方位検索してもほぼ確定石を特定できないため
			long direction = 0L;
			// 左
			t = (none & BitMask.LEFT_LINE_REVERSE.mask) << 1;
			t |= (t & BitMask.LEFT_LINE_REVERSE.mask) << 1;
			t |= (t & BitMask.LEFT_LINE_REVERSE.mask) << 1;
			t |= (t & BitMask.LEFT_LINE_REVERSE.mask) << 1;
			t |= (t & BitMask.LEFT_LINE_REVERSE.mask) << 1;
			t |= (t & BitMask.LEFT_LINE_REVERSE.mask) << 1;
			direction |= t;

			// 左上
			t = (none & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
			t |= (t & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
			t |= (t & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
			t |= (t & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
			t |= (t & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
			t |= (t & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
			direction |= t;

			// 上
			t = (none & BitMask.UPPER_LINE_REVERSE.mask) << 8;
			t |= (t & BitMask.UPPER_LINE_REVERSE.mask) << 8;
			t |= (t & BitMask.UPPER_LINE_REVERSE.mask) << 8;
			t |= (t & BitMask.UPPER_LINE_REVERSE.mask) << 8;
			t |= (t & BitMask.UPPER_LINE_REVERSE.mask) << 8;
			t |= (t & BitMask.UPPER_LINE_REVERSE.mask) << 8;
			direction |= t;

			// 右上
			t = (none & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
			t |= (t & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
			t |= (t & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
			t |= (t & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
			t |= (t & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
			t |= (t & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
			direction |= t;

			// 右
			t = (none & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
			t |= (t & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
			t |= (t & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
			t |= (t & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
			t |= (t & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
			t |= (t & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
			direction |= t;

			// 右下
			t = (none & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
			t |= (t & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
			t |= (t & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
			t |= (t & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
			t |= (t & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
			t |= (t & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
			direction |= t;

			// 下
			t = (none & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
			t |= (t & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
			t |= (t & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
			t |= (t & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
			t |= (t & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
			t |= (t & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
			direction |= t;

			// 左下
			t = (none & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
			t |= (t & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
			t |= (t & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
			t |= (t & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
			t |= (t & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
			t |= (t & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
			direction |= t;

			//			System.out.println("全方位(黒)：\n" + BitBoardUtil.toBoardString(board.blackDiscs & ~direction));
			//			System.out.println("全方位(白)：\n" + BitBoardUtil.toBoardString(board.whiteDiscs & ~direction));
			blackFixed |= board.blackDiscs & ~direction;
			whiteFixed |= board.whiteDiscs & ~direction;
		}

		return new long[] { blackFixed, whiteFixed };
	}

	/**
	 * Ⅹラインを評価する
	 * <p>
	 * Ⅹラインの個数を返す
	 *
	 * @param board 盤面
	 * @return Ⅹライン評価 [0]:黒 [1]:白
	 */
	public static int[] evalXLine(BitBoard board) {
		final long none = board.getNone();
		int blackXLine = 0;
		int whiteXLine = 0;

		if ((none & BitMask.WHITE_LINE_REVERSE.mask) != 0) {
			if ((board.blackDiscs & BitMask.WHITE_LINE.mask) == BitMask.WHITE_LINE.mask) {
				blackXLine++;
			} else if ((board.whiteDiscs & BitMask.WHITE_LINE.mask) == BitMask.WHITE_LINE.mask) {
				whiteXLine++;
			}
		}
		if ((none & BitMask.BLACK_LINE_REVERSE.mask) != 0) {
			if ((board.blackDiscs & BitMask.BLACK_LINE.mask) == BitMask.BLACK_LINE.mask) {
				blackXLine++;
			} else if ((board.whiteDiscs & BitMask.BLACK_LINE.mask) == BitMask.BLACK_LINE.mask) {
				whiteXLine++;
			}
		}
		return new int[] { blackXLine, whiteXLine };
	}

	/**
	 * Ｃラインを評価する
	 * <p>
	 * Ｃラインの個数を返す
	 *
	 * @param board 盤面
	 * @return Ｃライン評価 [0]:黒 [1]:白
	 */
	public static int[] evalCLine(BitBoard board) {
		final long exist = board.blackDiscs | board.whiteDiscs;
		final long none = ~exist;
		int blackCLine = 0;
		int whiteCLine = 0;

		if ((none & BitMask.C_LINE_TYPE1_REVERSE.mask) != 0) {
			// C2 から右下
			if ((board.blackDiscs & BitMask.C_LINE_TYPE1.mask) == BitMask.C_LINE_TYPE1.mask) {
				blackCLine++;
			} else if ((board.whiteDiscs & BitMask.C_LINE_TYPE1.mask) == BitMask.C_LINE_TYPE1.mask) {
				whiteCLine++;
			}
		}
		if ((none & BitMask.C_LINE_TYPE2_REVERSE.mask) != 0) {
			// F2 から左下
			if ((board.blackDiscs & BitMask.C_LINE_TYPE2.mask) == BitMask.C_LINE_TYPE2.mask) {
				blackCLine++;
			} else if ((board.whiteDiscs & BitMask.C_LINE_TYPE2.mask) == BitMask.C_LINE_TYPE2.mask) {
				whiteCLine++;
			}
		}
		if ((none & BitMask.C_LINE_TYPE3_REVERSE.mask) != 0) {
			// B3 から右下
			if ((board.blackDiscs & BitMask.C_LINE_TYPE3.mask) == BitMask.C_LINE_TYPE3.mask) {
				blackCLine++;
			} else if ((board.whiteDiscs & BitMask.C_LINE_TYPE3.mask) == BitMask.C_LINE_TYPE3.mask) {
				whiteCLine++;
			}
		}
		if ((none & BitMask.C_LINE_TYPE4_REVERSE.mask) != 0) {
			// G3 から左下
			if ((board.blackDiscs & BitMask.C_LINE_TYPE4.mask) == BitMask.C_LINE_TYPE4.mask) {
				blackCLine++;
			} else if ((board.whiteDiscs & BitMask.C_LINE_TYPE4.mask) == BitMask.C_LINE_TYPE4.mask) {
				whiteCLine++;
			}
		}
		return new int[] { blackCLine, whiteCLine };
	}

	/**
	 * 手数の評価関数
	 *
	 * @param board 盤面
	 * @return 手数評価値 [0][0]:黒の純粋手数 [0][1]:黒の手数(悪手排除) [0][2]:黒の余裕手 [0][3]:黒の要石
	 *                     [1][0]:白の純粋手数 [1][1]:白の手数(悪手排除) [1][2]:白の余裕手 [1][3]:白の要石
	 */
	public static long[][] evalFlip(BitBoard board) {
		long netBlackFlip = board.getFlipPoint(ReversiColor.BLACK.getValue());
		long netWhiteFlip = board.getFlipPoint(ReversiColor.WHITE.getValue());

		long blackFlip = netBlackFlip & ~BitMask.ALL_XS.mask;
		long whiteFlip = netWhiteFlip & ~BitMask.ALL_XS.mask;

		// 要石
		long blackPivot = 0L;
		long whitePivot = 0L;

		// 次の１手を打って、その結果相手の隅着手が増える手については評価から除外する
		for (long point : BitBoardUtil.toPointArray(blackFlip)) {
			final BitBoard newBoard = board.flip(point, ReversiColor.BLACK.getValue());
			final long white = newBoard.getFlipPoint(ReversiColor.WHITE.getValue());
			if (((whiteFlip ^ white) & white & BitMask.ALL_CORNERS.mask) != 0) {
				blackFlip &= ~point;
			}
			long moved = board.blackDiscs ^ newBoard.blackDiscs & ~point;
			blackPivot |= moved & (point << 9);
			blackPivot |= moved & (point << 8);
			blackPivot |= moved & (point << 7);
			blackPivot |= moved & (point << 1);
			blackPivot |= moved & (point >>> 1);
			blackPivot |= moved & (point >>> 7);
			blackPivot |= moved & (point >>> 8);
			blackPivot |= moved & (point >>> 9);
		}

		for (long point : BitBoardUtil.toPointArray(whiteFlip)) {
			final BitBoard newBoard = board.flip(point, ReversiColor.WHITE.getValue());
			final long black = newBoard.getFlipPoint(ReversiColor.BLACK.getValue());
			if (((blackFlip ^ black) & black & BitMask.ALL_CORNERS.mask) != 0) {
				whiteFlip &= ~point;
			}
			long moved = board.whiteDiscs ^ newBoard.whiteDiscs & ~point;
			whitePivot |= moved & (point << 9);
			whitePivot |= moved & (point << 8);
			whitePivot |= moved & (point << 7);
			whitePivot |= moved & (point << 1);
			whitePivot |= moved & (point >>> 1);
			whitePivot |= moved & (point >>> 7);
			whitePivot |= moved & (point >>> 8);
			whitePivot |= moved & (point >>> 9);
		}

		// 余裕手の算出
		final long blackAdvantageFlip = blackFlip & ~whiteFlip & BitMask.DIAGONAL.mask;
		final long whiteAdvantageFlip = whiteFlip & ~blackFlip & BitMask.DIAGONAL.mask;

		return new long[][] { { netBlackFlip, blackFlip, blackAdvantageFlip, blackPivot },
				{ netWhiteFlip, whiteFlip, whiteAdvantageFlip, whitePivot } };
	}

	/**
	 * 隅の評価関数
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @return 隅評価値 [0]:黒 [1]:白
	 */
	public static long[] evalCorner(BitBoard board) {
		return new long[] { board.blackDiscs & BitMask.ALL_CORNERS.mask, board.whiteDiscs & BitMask.ALL_CORNERS.mask };
	}

	/**
	 * Ⅹの評価関数
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @param fixed 確定石盤面
	 * @return Ⅹ評価値 [0]:黒 [1]:白
	 */
	public static long[] evalX(BitBoard board, long[] fixed) {

		final long none = board.getNone();

		// A1が空、B2が自石、B2が確定でない場合
		// A8が空、B7が自石、B7が確定でない場合
		// H1が空、G2が自石、G2が確定でない場合
		// H8が空、G7が自石、G7が確定でない場合
		final long blackX = ((none & BitPoint.A1.mask) >>> 9 & (board.blackDiscs & BitPoint.B2.mask & ~fixed[0]))
				| ((none & BitPoint.A8.mask) << 7 & (board.blackDiscs & BitPoint.B7.mask & ~fixed[0]))
				| ((none & BitPoint.H1.mask) >>> 7 & (board.blackDiscs & BitPoint.G2.mask & ~fixed[0]))
				| ((none & BitPoint.H8.mask) << 9 & (board.blackDiscs & BitPoint.G7.mask & ~fixed[0]));
		final long whiteX = ((none & BitPoint.A1.mask) >>> 9 & (board.whiteDiscs & BitPoint.B2.mask & ~fixed[1]))
				| ((none & BitPoint.A8.mask) << 7 & (board.whiteDiscs & BitPoint.B7.mask & ~fixed[1]))
				| ((none & BitPoint.H1.mask) >>> 7 & (board.whiteDiscs & BitPoint.G2.mask & ~fixed[1]))
				| ((none & BitPoint.H8.mask) << 9 & (board.whiteDiscs & BitPoint.G7.mask & ~fixed[1]));

		return new long[] { blackX, whiteX };
	}

	/**
	 * Ｃの評価関数
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @param fixed 確定石盤面
	 * @param mountains 山評価
	 * @param wings 翼評価
	 * @param blocks ブロック評価
	 * @return Ｃ評価値 [0][0]:黒Ｃ [0][1]:黒単独Ｃ [1][0]:白Ｃ [1][1]:白単独Ｃ
	 */
	public static long[][] evalC(BitBoard board, long[] fixed, long[][] mountains, long[][] wings, long[][] blocks) {

		final long exist = board.blackDiscs | board.whiteDiscs;
		final long none = ~exist;
		long blackC = 0L;
		long blackUniC = 0L;
		long whiteC = 0L;
		long whiteUniC = 0L;

		// 空の隅
		final long blankCorner = none & BitMask.ALL_CORNERS.mask;
		// ビット演算用
		long t = 0L;
		// 上
		t |= exist & (blankCorner << 8);
		// 下
		t |= exist & (blankCorner >>> 8);
		// 左
		t |= exist & BitMask.RIGHT_LINE_REVERSE.mask & (blankCorner << 1);
		// 右
		t |= exist & BitMask.LEFT_LINE_REVERSE.mask & (blankCorner >>> 1);

		// 上辺単独Ｃ打ち
		if (Long.bitCount(exist & BitMask.UPPER_LINE.mask) == 1) {
			blackUniC |= t & BitMask.UPPER_LINE.mask & board.blackDiscs;
			whiteUniC |= t & BitMask.UPPER_LINE.mask & board.whiteDiscs;
		}
		// 下辺単独Ｃ打ち
		if (Long.bitCount(exist & BitMask.LOWER_LINE.mask) == 1) {
			blackUniC |= t & BitMask.LOWER_LINE.mask & board.blackDiscs;
			whiteUniC |= t & BitMask.LOWER_LINE.mask & board.whiteDiscs;
		}
		// 左辺単独Ｃ打ち
		if (Long.bitCount(exist & BitMask.LEFT_LINE.mask) == 1) {
			blackUniC |= t & BitMask.LEFT_LINE.mask & board.blackDiscs;
			whiteUniC |= t & BitMask.LEFT_LINE.mask & board.whiteDiscs;
		}
		// 右辺単独Ｃ打ち
		if (Long.bitCount(exist & BitMask.RIGHT_LINE.mask) == 1) {
			blackUniC |= t & BitMask.RIGHT_LINE.mask & board.blackDiscs;
			whiteUniC |= t & BitMask.RIGHT_LINE.mask & board.whiteDiscs;
		}

		blackC = t & board.blackDiscs & ~fixed[0];
		whiteC = t & board.whiteDiscs & ~fixed[1];

		// 山・翼に評価されたＣ打ちはＣの評価からは除外する
		blackC &= ~(mountains[0][0] | mountains[0][1] | wings[0][0] | wings[0][1]);
		whiteC &= ~(mountains[1][0] | mountains[1][1] | wings[1][0] | wings[1][1]);

		return new long[][] { { blackC, blackUniC }, { whiteC, whiteUniC } };
	}

	/**
	 * 山の評価関数
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @return 山評価値 [0][0]:黒の山 [0][1]:黒のピュア [1][0]:白の山 [1][1]:白のピュア
	 */
	public static long[][] evalMountain(BitBoard board) {
		final long none = board.getNone();
		long blackMountain = 0L;
		long blackMountainPure = 0L;
		long whiteMountain = 0L;
		long whiteMountainPure = 0L;

		// 上辺
		if ((none & BitMask.UPPER_MOUNTAIN_REVERSE.mask) == BitMask.UPPER_MOUNTAIN_REVERSE.mask) {
			if ((board.blackDiscs & BitMask.UPPER_MOUNTAIN_PURE.mask) == BitMask.UPPER_MOUNTAIN_PURE.mask) {
				blackMountainPure |= BitMask.UPPER_MOUNTAIN_PURE.mask;
			} else if ((board.blackDiscs & BitMask.UPPER_MOUNTAIN.mask) == BitMask.UPPER_MOUNTAIN.mask) {
				blackMountain |= BitMask.UPPER_MOUNTAIN.mask;
			} else if ((board.whiteDiscs & BitMask.UPPER_MOUNTAIN_PURE.mask) == BitMask.UPPER_MOUNTAIN_PURE.mask) {
				whiteMountainPure |= BitMask.UPPER_MOUNTAIN_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.UPPER_MOUNTAIN.mask) == BitMask.UPPER_MOUNTAIN.mask) {
				whiteMountain |= BitMask.UPPER_MOUNTAIN.mask;
			}
		}
		// 左辺
		if ((none & BitMask.LEFT_MOUNTAIN_REVERSE.mask) == BitMask.LEFT_MOUNTAIN_REVERSE.mask) {
			if ((board.blackDiscs & BitMask.LEFT_MOUNTAIN_PURE.mask) == BitMask.LEFT_MOUNTAIN_PURE.mask) {
				blackMountainPure |= BitMask.LEFT_MOUNTAIN_PURE.mask;
			} else if ((board.blackDiscs & BitMask.LEFT_MOUNTAIN.mask) == BitMask.LEFT_MOUNTAIN.mask) {
				blackMountain |= BitMask.LEFT_MOUNTAIN.mask;
			} else if ((board.whiteDiscs & BitMask.LEFT_MOUNTAIN_PURE.mask) == BitMask.LEFT_MOUNTAIN_PURE.mask) {
				whiteMountainPure |= BitMask.LEFT_MOUNTAIN_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.LEFT_MOUNTAIN.mask) == BitMask.LEFT_MOUNTAIN.mask) {
				whiteMountain |= BitMask.LEFT_MOUNTAIN.mask;
			}
		}
		// 右辺
		if ((none & BitMask.RIGHT_MOUNTAIN_REVERSE.mask) == BitMask.RIGHT_MOUNTAIN_REVERSE.mask) {
			if ((board.blackDiscs & BitMask.RIGHT_MOUNTAIN_PURE.mask) == BitMask.RIGHT_MOUNTAIN_PURE.mask) {
				blackMountainPure |= BitMask.RIGHT_MOUNTAIN_PURE.mask;
			} else if ((board.blackDiscs & BitMask.RIGHT_MOUNTAIN.mask) == BitMask.RIGHT_MOUNTAIN.mask) {
				blackMountain |= BitMask.RIGHT_MOUNTAIN.mask;
			} else if ((board.whiteDiscs & BitMask.RIGHT_MOUNTAIN_PURE.mask) == BitMask.RIGHT_MOUNTAIN_PURE.mask) {
				whiteMountainPure |= BitMask.RIGHT_MOUNTAIN_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.RIGHT_MOUNTAIN.mask) == BitMask.RIGHT_MOUNTAIN.mask) {
				whiteMountain |= BitMask.RIGHT_MOUNTAIN.mask;
			}
		}
		// 下辺
		if ((none & BitMask.LOWER_MOUNTAIN_REVERSE.mask) == BitMask.LOWER_MOUNTAIN_REVERSE.mask) {
			if ((board.blackDiscs & BitMask.LOWER_MOUNTAIN_PURE.mask) == BitMask.LOWER_MOUNTAIN_PURE.mask) {
				blackMountainPure |= BitMask.LOWER_MOUNTAIN_PURE.mask;
			} else if ((board.blackDiscs & BitMask.LOWER_MOUNTAIN.mask) == BitMask.LOWER_MOUNTAIN.mask) {
				blackMountain |= BitMask.LOWER_MOUNTAIN.mask;
			} else if ((board.whiteDiscs & BitMask.LOWER_MOUNTAIN_PURE.mask) == BitMask.LOWER_MOUNTAIN_PURE.mask) {
				whiteMountainPure |= BitMask.LOWER_MOUNTAIN_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.LOWER_MOUNTAIN.mask) == BitMask.LOWER_MOUNTAIN.mask) {
				whiteMountain |= BitMask.LOWER_MOUNTAIN.mask;
			}
		}
		return new long[][] { { blackMountain, blackMountainPure }, { whiteMountain, whiteMountainPure } };
	}

	/**
	 * 翼の評価関数
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @param flips 着手可能位置
	 * @return 翼評価値 [0][0]:黒の翼 [0][1]:黒のピュア [1][0]:白の翼 [1][1]:白のピュア
	 */
	public static long[][] evalWing(BitBoard board, long[][] flips) {
		final long none = board.getNone();
		long blackWing = 0L;
		long blackWingPure = 0L;
		long whiteWing = 0L;
		long whiteWingPure = 0L;

		// 上辺
		if ((none & BitMask.UPPER_WING_TYPE1_REVERSE.mask) == BitMask.UPPER_WING_TYPE1_REVERSE.mask) {
			// タイプ１とマッチ
			if ((board.blackDiscs & BitMask.UPPER_WING_TYPE1_PURE.mask) == BitMask.UPPER_WING_TYPE1_PURE.mask) {
				if ((flips[1][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					blackWingPure |= BitMask.UPPER_WING_TYPE1_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.UPPER_WING_TYPE1.mask) == BitMask.UPPER_WING_TYPE1.mask) {
				if ((flips[1][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					blackWing |= BitMask.UPPER_WING_TYPE1.mask;
				}
			} else if ((board.whiteDiscs & BitMask.UPPER_WING_TYPE1_PURE.mask) == BitMask.UPPER_WING_TYPE1_PURE.mask) {
				if ((flips[0][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					whiteWingPure |= BitMask.UPPER_WING_TYPE1_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.UPPER_WING_TYPE1.mask) == BitMask.UPPER_WING_TYPE1.mask) {
				if ((flips[0][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					whiteWing |= BitMask.UPPER_WING_TYPE1.mask;
				}
			}
		} else if ((none & BitMask.UPPER_WING_TYPE2_REVERSE.mask) == BitMask.UPPER_WING_TYPE2_REVERSE.mask) {
			// タイプ２とマッチ
			if ((board.blackDiscs & BitMask.UPPER_WING_TYPE2_PURE.mask) == BitMask.UPPER_WING_TYPE2_PURE.mask) {
				if ((flips[1][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					blackWingPure |= BitMask.UPPER_WING_TYPE2_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.UPPER_WING_TYPE2.mask) == BitMask.UPPER_WING_TYPE2.mask) {
				if ((flips[1][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					blackWing |= BitMask.UPPER_WING_TYPE2.mask;
				}
			} else if ((board.whiteDiscs & BitMask.UPPER_WING_TYPE2_PURE.mask) == BitMask.UPPER_WING_TYPE2_PURE.mask) {
				if ((flips[0][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					whiteWingPure |= BitMask.UPPER_WING_TYPE2_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.UPPER_WING_TYPE2.mask) == BitMask.UPPER_WING_TYPE2.mask) {
				if ((flips[0][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					whiteWing |= BitMask.UPPER_WING_TYPE2.mask;
				}
			}
		}
		// 左辺
		if ((none & BitMask.LEFT_WING_TYPE1_REVERSE.mask) == BitMask.LEFT_WING_TYPE1_REVERSE.mask) {
			// タイプ１とマッチ
			if ((board.blackDiscs & BitMask.LEFT_WING_TYPE1_PURE.mask) == BitMask.LEFT_WING_TYPE1_PURE.mask) {
				if ((flips[1][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					blackWingPure |= BitMask.LEFT_WING_TYPE1_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.LEFT_WING_TYPE1.mask) == BitMask.LEFT_WING_TYPE1.mask) {
				if ((flips[1][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					blackWing |= BitMask.LEFT_WING_TYPE1.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LEFT_WING_TYPE1_PURE.mask) == BitMask.LEFT_WING_TYPE1_PURE.mask) {
				if ((flips[0][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					whiteWingPure |= BitMask.LEFT_WING_TYPE1_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LEFT_WING_TYPE1.mask) == BitMask.LEFT_WING_TYPE1.mask) {
				if ((flips[0][0] & BitPoint.B2.mask) != 0 || (none & BitPoint.B2.mask) == 0) {
					whiteWing |= BitMask.LEFT_WING_TYPE1.mask;
				}
			}
		} else if ((none & BitMask.LEFT_WING_TYPE2_REVERSE.mask) == BitMask.LEFT_WING_TYPE2_REVERSE.mask) {
			// タイプ２とマッチ
			if ((board.blackDiscs & BitMask.LEFT_WING_TYPE2_PURE.mask) == BitMask.LEFT_WING_TYPE2_PURE.mask) {
				if ((flips[1][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					blackWingPure |= BitMask.LEFT_WING_TYPE2_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.LEFT_WING_TYPE2.mask) == BitMask.LEFT_WING_TYPE2.mask) {
				if ((flips[1][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					blackWing |= BitMask.LEFT_WING_TYPE2.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LEFT_WING_TYPE2_PURE.mask) == BitMask.LEFT_WING_TYPE2_PURE.mask) {
				if ((flips[0][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					whiteWingPure |= BitMask.LEFT_WING_TYPE2_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LEFT_WING_TYPE2.mask) == BitMask.LEFT_WING_TYPE2.mask) {
				if ((flips[0][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					whiteWing |= BitMask.LEFT_WING_TYPE2.mask;
				}
			}
		}
		// 右辺
		if ((none & BitMask.RIGHT_WING_TYPE1_REVERSE.mask) == BitMask.RIGHT_WING_TYPE1_REVERSE.mask) {
			// タイプ１とマッチ
			if ((board.blackDiscs & BitMask.RIGHT_WING_TYPE1_PURE.mask) == BitMask.RIGHT_WING_TYPE1_PURE.mask) {
				if ((flips[1][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					blackWingPure |= BitMask.RIGHT_WING_TYPE1_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.RIGHT_WING_TYPE1.mask) == BitMask.RIGHT_WING_TYPE1.mask) {
				if ((flips[1][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					blackWing |= BitMask.RIGHT_WING_TYPE1.mask;
				}
			} else if ((board.whiteDiscs & BitMask.RIGHT_WING_TYPE1_PURE.mask) == BitMask.RIGHT_WING_TYPE1_PURE.mask) {
				if ((flips[0][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					whiteWingPure |= BitMask.RIGHT_WING_TYPE1_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.RIGHT_WING_TYPE1.mask) == BitMask.RIGHT_WING_TYPE1.mask) {
				if ((flips[0][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					whiteWing |= BitMask.RIGHT_WING_TYPE1.mask;
				}
			}
		} else if ((none & BitMask.RIGHT_WING_TYPE2_REVERSE.mask) == BitMask.RIGHT_WING_TYPE2_REVERSE.mask) {
			// タイプ２とマッチ
			if ((board.blackDiscs & BitMask.RIGHT_WING_TYPE2_PURE.mask) == BitMask.RIGHT_WING_TYPE2_PURE.mask) {
				if ((flips[1][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					blackWingPure |= BitMask.RIGHT_WING_TYPE2_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.RIGHT_WING_TYPE2.mask) == BitMask.RIGHT_WING_TYPE2.mask) {
				if ((flips[1][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					blackWing |= BitMask.RIGHT_WING_TYPE2.mask;
				}
			} else if ((board.whiteDiscs & BitMask.RIGHT_WING_TYPE2_PURE.mask) == BitMask.RIGHT_WING_TYPE2_PURE.mask) {
				if ((flips[0][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					whiteWingPure |= BitMask.RIGHT_WING_TYPE2_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.RIGHT_WING_TYPE2.mask) == BitMask.RIGHT_WING_TYPE2.mask) {
				if ((flips[0][0] & BitPoint.G2.mask) != 0 || (none & BitPoint.G2.mask) == 0) {
					whiteWing |= BitMask.RIGHT_WING_TYPE2.mask;
				}
			}
		}
		// 下辺
		if ((none & BitMask.LOWER_WING_TYPE1_REVERSE.mask) == BitMask.LOWER_WING_TYPE1_REVERSE.mask) {
			// タイプ１とマッチ
			if ((board.blackDiscs & BitMask.LOWER_WING_TYPE1_PURE.mask) == BitMask.LOWER_WING_TYPE1_PURE.mask) {
				if ((flips[1][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					blackWingPure |= BitMask.LOWER_WING_TYPE1_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.LOWER_WING_TYPE1.mask) == BitMask.LOWER_WING_TYPE1.mask) {
				if ((flips[1][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					blackWing |= BitMask.LOWER_WING_TYPE1.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LOWER_WING_TYPE1_PURE.mask) == BitMask.LOWER_WING_TYPE1_PURE.mask) {
				if ((flips[0][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					whiteWingPure |= BitMask.LOWER_WING_TYPE1_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LOWER_WING_TYPE1.mask) == BitMask.LOWER_WING_TYPE1.mask) {
				if ((flips[0][0] & BitPoint.B7.mask) != 0 || (none & BitPoint.B7.mask) == 0) {
					whiteWing |= BitMask.LOWER_WING_TYPE1.mask;
				}
			}
		} else if ((none & BitMask.LOWER_WING_TYPE2_REVERSE.mask) == BitMask.LOWER_WING_TYPE2_REVERSE.mask) {
			// タイプ２とマッチ
			if ((board.blackDiscs & BitMask.LOWER_WING_TYPE2_PURE.mask) == BitMask.LOWER_WING_TYPE2_PURE.mask) {
				if ((flips[1][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					blackWingPure |= BitMask.LOWER_WING_TYPE2_PURE.mask;
				}
			} else if ((board.blackDiscs & BitMask.LOWER_WING_TYPE2.mask) == BitMask.LOWER_WING_TYPE2.mask) {
				if ((flips[1][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					blackWing |= BitMask.LOWER_WING_TYPE2.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LOWER_WING_TYPE2_PURE.mask) == BitMask.LOWER_WING_TYPE2_PURE.mask) {
				if ((flips[0][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					whiteWingPure |= BitMask.LOWER_WING_TYPE2_PURE.mask;
				}
			} else if ((board.whiteDiscs & BitMask.LOWER_WING_TYPE2.mask) == BitMask.LOWER_WING_TYPE2.mask) {
				if ((flips[0][0] & BitPoint.G7.mask) != 0 || (none & BitPoint.G7.mask) == 0) {
					whiteWing |= BitMask.LOWER_WING_TYPE2.mask;
				}
			}
		}
		return new long[][] { { blackWing, blackWingPure }, { whiteWing, whiteWingPure } };
	}

	/**
	 * ブロックを評価する
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @return ブロックの評価値 [0][0]:黒のブロック [0][1]:黒のピュア [1][0]:白のブロック [1][1]:白のピュア
	 */
	public static long[][] evalBlock(BitBoard board) {
		final long exist = board.blackDiscs | board.whiteDiscs;
		final long none = ~exist;
		long blackBlock = 0L;
		long blackBlockPure = 0L;
		long whiteBlock = 0L;
		long whiteBlockPure = 0L;

		if ((none & BitMask.UPPER_BLOCK_REVERSE.mask) == BitMask.UPPER_BLOCK_REVERSE.mask) {
			// 上辺のブロック
			if ((board.blackDiscs & BitMask.UPPER_BLOCK_PURE.mask) == BitMask.UPPER_BLOCK_PURE.mask) {
				// 黒のピュア
				blackBlockPure |= BitMask.UPPER_BLOCK_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.UPPER_BLOCK_PURE.mask) == BitMask.UPPER_BLOCK_PURE.mask) {
				// 白のピュア
				whiteBlockPure |= BitMask.UPPER_BLOCK_PURE.mask;
			} else if ((board.blackDiscs & BitMask.UPPER_BLOCK.mask) == BitMask.UPPER_BLOCK.mask) {
				// 黒のブロック
				blackBlock |= BitMask.UPPER_BLOCK.mask;
			} else if ((board.whiteDiscs & BitMask.UPPER_BLOCK.mask) == BitMask.UPPER_BLOCK.mask) {
				// 白のブロック
				whiteBlock |= BitMask.UPPER_BLOCK.mask;
			}
		}
		if ((none & BitMask.LOWER_BLOCK_REVERSE.mask) == BitMask.LOWER_BLOCK_REVERSE.mask) {
			// 下辺のブロック
			if ((board.blackDiscs & BitMask.LOWER_BLOCK_PURE.mask) == BitMask.LOWER_BLOCK_PURE.mask) {
				// 黒のピュア
				blackBlockPure |= BitMask.LOWER_BLOCK_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.LOWER_BLOCK_PURE.mask) == BitMask.LOWER_BLOCK_PURE.mask) {
				// 白のピュア
				whiteBlockPure |= BitMask.LOWER_BLOCK_PURE.mask;
			} else if ((board.blackDiscs & BitMask.LOWER_BLOCK.mask) == BitMask.LOWER_BLOCK.mask) {
				// 黒がブロック
				blackBlock |= BitMask.LOWER_BLOCK.mask;
			} else if ((board.whiteDiscs & BitMask.LOWER_BLOCK.mask) == BitMask.LOWER_BLOCK.mask) {
				// 白がブロック
				whiteBlock = BitMask.LOWER_BLOCK.mask;
			}
		}
		if ((none & BitMask.LEFT_BLOCK_REVERSE.mask) == BitMask.LEFT_BLOCK_REVERSE.mask) {
			// 左辺のブロック
			if ((board.blackDiscs & BitMask.LEFT_BLOCK_PURE.mask) == BitMask.LEFT_BLOCK_PURE.mask) {
				blackBlockPure |= BitMask.LEFT_BLOCK_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.LEFT_BLOCK_PURE.mask) == BitMask.LEFT_BLOCK_PURE.mask) {
				whiteBlockPure |= BitMask.LEFT_BLOCK_PURE.mask;
			} else if ((board.blackDiscs & BitMask.LEFT_BLOCK.mask) == BitMask.LEFT_BLOCK.mask) {
				// 黒がブロック
				blackBlock |= BitMask.LEFT_BLOCK.mask;
			} else if ((board.whiteDiscs & BitMask.LEFT_BLOCK.mask) == BitMask.LEFT_BLOCK.mask) {
				// 白がブロック
				whiteBlock |= BitMask.LEFT_BLOCK.mask;
			}
		}
		if ((none & BitMask.RIGHT_BLOCK_REVERSE.mask) == BitMask.RIGHT_BLOCK_REVERSE.mask) {
			// 右辺のブロック
			if ((board.blackDiscs & BitMask.RIGHT_BLOCK_PURE.mask) == BitMask.RIGHT_BLOCK_PURE.mask) {
				blackBlockPure |= BitMask.RIGHT_BLOCK_PURE.mask;
			} else if ((board.whiteDiscs & BitMask.RIGHT_BLOCK_PURE.mask) == BitMask.RIGHT_BLOCK_PURE.mask) {
				whiteBlockPure |= BitMask.RIGHT_BLOCK_PURE.mask;
			} else if ((board.blackDiscs & BitMask.RIGHT_BLOCK.mask) == BitMask.RIGHT_BLOCK.mask) {
				// 黒がブロック
				blackBlock |= BitMask.RIGHT_BLOCK.mask;
			} else if ((board.whiteDiscs & BitMask.RIGHT_BLOCK.mask) == BitMask.RIGHT_BLOCK.mask) {
				// 白がブロック
				whiteBlock |= BitMask.RIGHT_BLOCK.mask;
			}
		}
		return new long[][] { { blackBlock, blackBlockPure }, { whiteBlock, whiteBlockPure } };
	}

	private static final int STRONG_ADVANTAGE = 40;
	private static final int GOOD_ADVANTAGE = 25;
	private static final int ADVANTAGE = 15;
	private static final int FINE = 10;
	private static final int PLUS = 5;
	private static final int A_LITTLE_PLUS = 3;
	private static final int A_LITTLE_MINUS = -3;
	private static final int MINUS = -5;
	private static final int CRAPPY = -10;
	private static final int DISADVANTAGE = -15;
	private static final int BAD_DISADVANTAGE = -25;
	private static final int TERRIBLE_DISADVANTAGE = -40;

	/**
	 * 辺の総合評価
	 * <p>
	 * 評価の値を返す
	 *
	 * @param board 盤面
	 * @param mountains 山評価
	 * @param wings 翼評価
	 * @param blocks ブロック評価
	 * @param fixed 確定石
	 * @param cs Ｃ打ち評価
	 * @param flips 手数評価
	 * @return 辺の評価値 [0][0]:黒の辺個数 [0][1]:黒の辺評価 [1][0]:白の辺個数 [1][1]:白の辺評価
	 */
	public static int[][] evalLine(BitBoard board, long[][] mountains, long[][] wings, long[][] blocks, long[] fixed,
			long[][] cs, long[][] flips) {
		final long blackWingComposition = wings[0][0] | wings[0][1];
		final long whiteWingComposition = wings[1][0] | wings[1][1];
		final long blackMountainComposition = mountains[0][0] | mountains[0][1];
		final long whiteMountainComposition = mountains[1][0] | mountains[1][1];
		final long blackBlockComposition = blocks[0][0] | blocks[0][1];
		final long whiteBlockComposition = blocks[1][0] | blocks[1][1];
		final long none = board.getNone();
		final long exist = ~none;

		int blackEval = 0;
		int whiteEval = 0;

		// ************* 辺の所有評価 *************
		final long blackMaskedLine = board.blackDiscs & ~fixed[0] & BitMask.DIAGONAL.mask;
		final long whiteMaskedLine = board.whiteDiscs & ~fixed[1] & BitMask.DIAGONAL.mask;

		// ************* 翼と山の関連評価 *************
		// 上辺
		if ((board.blackDiscs & BitPoint.B1.mask) != 0) {
			// 黒がB1を持っている
			if ((mountains[0][1] & BitPoint.B1.mask) != 0) {
				// 上辺が黒の山(ピュア)
				// 左辺
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += STRONG_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += GOOD_ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += ADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += STRONG_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += GOOD_ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += ADVANTAGE;
				}
			} else if ((mountains[0][0] & BitPoint.B1.mask) != 0) {
				// 上辺が黒の山
				// 左辺
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += GOOD_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += FINE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += GOOD_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += FINE;
				}
			} else if ((wings[0][1] & BitPoint.B1.mask) != 0) {
				// 上辺がB1を持つ黒の翼(ピュア)
				// 左辺
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
			} else if ((wings[0][0] & BitPoint.B1.mask) != 0) {
				// 上辺がB1を持つ黒の翼
				// 左辺
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
			}
		} else if ((board.whiteDiscs & BitPoint.B1.mask) != 0) {
			// 白がB1を持っている
			if ((mountains[1][1] & BitPoint.B1.mask) != 0) {
				// 上辺が白の山(ピュア)
				// 左辺
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += STRONG_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += GOOD_ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += FINE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += STRONG_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += GOOD_ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += FINE;
				}
			} else if ((mountains[1][0] & BitPoint.B1.mask) != 0) {
				// 上辺が白の山
				// 左辺
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += GOOD_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += PLUS;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += GOOD_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += PLUS;
				}
			} else if ((wings[1][1] & BitPoint.B1.mask) != 0) {
				// 上辺がB1を持つ白の翼(ピュア)
				// 左辺
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
			} else if ((wings[1][0] & BitPoint.B1.mask) != 0) {
				// 上辺がB1を持つ白の翼
				// 左辺
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
			}
		} else if ((board.blackDiscs & BitPoint.G1.mask) != 0) {
			// 黒がG1を持っている
			if ((wings[0][1] & BitPoint.G1.mask) != 0) {
				// 上辺がG1を持つ黒の翼(ピュア)
				// 左辺
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
			} else if ((wings[0][0] & BitPoint.G1.mask) != 0) {
				// 上辺がG1を持つ黒の翼
				// 左辺
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += PLUS;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += CRAPPY;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += PLUS;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += CRAPPY;
				}
			}
		} else if ((board.whiteDiscs & BitPoint.G1.mask) != 0) {
			// 白がG1を持っている
			if ((wings[1][1] & BitPoint.G1.mask) != 0) {
				// 上辺がG1を持つ白の翼(ピュア)
				// 左辺
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
			} else if ((wings[1][0] & BitPoint.G1.mask) != 0) {
				// 上辺がG1を持つ白の翼
				// 左辺
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += PLUS;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += CRAPPY;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += PLUS;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += CRAPPY;
				}
			}
		} else if ((blackBlockComposition & BitPoint.C1.mask) != 0) {
			// 上辺が黒のブロック(ピュア/通常)
			// 左辺
			if ((blackMountainComposition & BitPoint.A2.mask) != 0 || (blackWingComposition & BitPoint.A2.mask) != 0) {
				// A2を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
			// 右辺
			if ((blackMountainComposition & BitPoint.H2.mask) != 0 || (blackWingComposition & BitPoint.H2.mask) != 0) {
				// H2を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
		} else if ((whiteBlockComposition & BitPoint.C1.mask) != 0) {
			// 上辺が白のブロック(ピュア/通常)
			// 左辺
			if ((whiteMountainComposition & BitPoint.A2.mask) != 0 || (whiteWingComposition & BitPoint.A2.mask) != 0) {
				// A2を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
			// 右辺
			if ((whiteMountainComposition & BitPoint.H2.mask) != 0 || (whiteWingComposition & BitPoint.H2.mask) != 0) {
				// H2を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
		} else {
			// 上辺が山・翼・ブロックに該当しない
			// 左辺
			if ((board.blackDiscs & BitPoint.A2.mask) != 0 || (board.blackDiscs & BitPoint.A7.mask) != 0) {
				if ((mountains[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					blackEval += FINE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += MINUS;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += MINUS;
				}
			} else if ((board.whiteDiscs & BitPoint.A2.mask) != 0 || (board.whiteDiscs & BitPoint.A7.mask) != 0) {
				if ((mountains[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺が山
					whiteEval += FINE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += MINUS;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += MINUS;
				}
			}
			// 右辺
			if ((board.blackDiscs & BitPoint.H2.mask) != 0 || (board.blackDiscs & BitPoint.H7.mask) != 0) {
				if ((mountains[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					blackEval += FINE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += MINUS;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += MINUS;
				}
			} else if ((board.whiteDiscs & BitPoint.H2.mask) != 0 || (board.whiteDiscs & BitPoint.H7.mask) != 0) {
				if ((mountains[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺が山
					whiteEval += FINE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += MINUS;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += MINUS;
				}
			}
		}

		// 下辺
		if ((board.blackDiscs & BitPoint.B8.mask) != 0) {
			// 黒がB8を持っている
			if ((mountains[0][1] & BitPoint.B8.mask) != 0) {
				// 下辺が黒の山(ピュア)
				// 左辺
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += STRONG_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += GOOD_ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += ADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += STRONG_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					blackEval += GOOD_ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += ADVANTAGE;
				}
			} else if ((mountains[0][0] & BitPoint.B8.mask) != 0) {
				// 下辺が黒の山
				// 左辺
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += GOOD_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += FINE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += GOOD_ADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					blackEval += ADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += A_LITTLE_PLUS;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += PLUS;
				} else {
					blackEval += FINE;
				}
			} else if ((wings[0][1] & BitPoint.B8.mask) != 0) {
				// 下辺がB8を持つ黒の翼(ピュア)
				// 左辺
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
			} else if ((wings[0][0] & BitPoint.B8.mask) != 0) {
				// 下辺がB8を持つ黒の翼
				// 左辺
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
			}
		} else if ((board.whiteDiscs & BitPoint.B8.mask) != 0) {
			// 白がB8を持っている
			if ((mountains[1][1] & BitPoint.B8.mask) != 0) {
				// 下辺が白の山(ピュア)
				// 左辺
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += STRONG_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += GOOD_ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += FINE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += STRONG_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					whiteEval += GOOD_ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += FINE;
				}
			} else if ((mountains[1][0] & BitPoint.B8.mask) != 0) {
				// 下辺が白の山
				// 左辺
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += GOOD_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += PLUS;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += GOOD_ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					whiteEval += ADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += A_LITTLE_PLUS;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += PLUS;
				} else {
					whiteEval += PLUS;
				}
			} else if ((wings[1][1] & BitPoint.B8.mask) != 0) {
				// 下辺がB8を持つ白の翼(ピュア)
				// 左辺
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
			} else if ((wings[1][0] & BitPoint.B8.mask) != 0) {
				// 下辺がB8を持つ白の翼
				// 左辺
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
			}
		} else if ((board.blackDiscs & BitPoint.G8.mask) != 0) {
			// 黒がG8を持っている
			if ((wings[0][1] & BitPoint.G8.mask) != 0) {
				// 下辺がG8を持つ黒の翼(ピュア)
				// 左辺
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += BAD_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += DISADVANTAGE;
				}
			} else if ((wings[0][0] & BitPoint.G8.mask) != 0) {
				// 下辺がG8を持つ黒の翼
				// 左辺
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += PLUS;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += CRAPPY;
				}
				// 右辺
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					blackEval += PLUS;
				} else if ((mountains[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					blackEval += PLUS;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					blackEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					blackEval += BAD_DISADVANTAGE;
				} else {
					blackEval += CRAPPY;
				}
			}
		} else if ((board.whiteDiscs & BitPoint.G8.mask) != 0) {
			// 白がG8を持っている
			if ((wings[1][1] & BitPoint.G8.mask) != 0) {
				// 下辺がG8を持つ白の翼(ピュア)
				// 左辺
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += BAD_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += DISADVANTAGE;
				}
			} else if ((wings[1][0] & BitPoint.G8.mask) != 0) {
				// 下辺がG8を持つ白の翼
				// 左辺
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += PLUS;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += CRAPPY;
				}
				// 右辺
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺が山(ピュア)
					whiteEval += PLUS;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺が山
					whiteEval += PLUS;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 右辺がH7を持つ翼
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼(ピュア)
					whiteEval += TERRIBLE_DISADVANTAGE;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += BAD_DISADVANTAGE;
				} else {
					whiteEval += CRAPPY;
				}
			}
		} else if ((blackBlockComposition & BitPoint.C8.mask) != 0) {
			// 下辺が黒のブロック(ピュア/通常)
			// 左辺
			if ((blackMountainComposition & BitPoint.A7.mask) != 0 || (blackWingComposition & BitPoint.A7.mask) != 0) {
				// A7を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
			// 右辺
			if ((blackMountainComposition & BitPoint.H7.mask) != 0 || (blackWingComposition & BitPoint.H7.mask) != 0) {
				// H7を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
		} else if ((whiteBlockComposition & BitPoint.C8.mask) != 0) {
			// 下辺が白のブロック(ピュア/通常)
			// 左辺
			if ((whiteMountainComposition & BitPoint.A7.mask) != 0 || (whiteWingComposition & BitPoint.A7.mask) != 0) {
				// A7を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
			// 右辺
			if ((whiteMountainComposition & BitPoint.H7.mask) != 0 || (whiteWingComposition & BitPoint.H7.mask) != 0) {
				// H7を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
		} else {
			// 上辺が山・翼に該当しない
			// 左辺
			if ((board.blackDiscs & BitPoint.A2.mask) != 0 || (board.blackDiscs & BitPoint.A7.mask) != 0) {
				if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += ADVANTAGE;
				} else if ((mountains[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					blackEval += FINE;
				} else if ((wings[0][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					blackEval += MINUS;
				} else if ((wings[0][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					blackEval += MINUS;
				}
			} else if ((board.whiteDiscs & BitPoint.A2.mask) != 0 || (board.whiteDiscs & BitPoint.A7.mask) != 0) {
				if ((mountains[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺が山
					whiteEval += FINE;
				} else if ((wings[1][1] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.A7.mask) != 0) {
					// 左辺がA7を持つ翼
					whiteEval += MINUS;
				} else if ((wings[1][1] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.A2.mask) != 0) {
					// 左辺がA2を持つ翼
					whiteEval += MINUS;
				}
			}
			// 右辺
			if ((board.blackDiscs & BitPoint.H2.mask) != 0 || (board.blackDiscs & BitPoint.H7.mask) != 0) {
				if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 左辺が山(ピュア)
					blackEval += ADVANTAGE;
				} else if ((mountains[0][1] & BitPoint.H7.mask) != 0) {
					// 左辺が山
					blackEval += FINE;
				} else if ((wings[0][1] & BitPoint.H7.mask) != 0) {
					// 左辺がH7を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.H7.mask) != 0) {
					// 左辺がH7を持つ翼
					blackEval += MINUS;
				} else if ((wings[0][1] & BitPoint.H2.mask) != 0) {
					// 左辺がH2を持つ翼(ピュア)
					blackEval += CRAPPY;
				} else if ((wings[0][0] & BitPoint.H2.mask) != 0) {
					// 左辺がH2を持つ翼
					blackEval += MINUS;
				}
			} else if ((board.whiteDiscs & BitPoint.H2.mask) != 0 || (board.whiteDiscs & BitPoint.H7.mask) != 0) {
				if ((mountains[1][1] & BitPoint.H7.mask) != 0) {
					// 左辺が山(ピュア)
					whiteEval += ADVANTAGE;
				} else if ((mountains[1][0] & BitPoint.H7.mask) != 0) {
					// 左辺が山
					whiteEval += FINE;
				} else if ((wings[1][1] & BitPoint.H7.mask) != 0) {
					// 左辺がH7を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.H7.mask) != 0) {
					// 左辺がH7を持つ翼
					whiteEval += MINUS;
				} else if ((wings[1][1] & BitPoint.H2.mask) != 0) {
					// 左辺がH2を持つ翼(ピュア)
					whiteEval += CRAPPY;
				} else if ((wings[1][0] & BitPoint.H2.mask) != 0) {
					// 右辺がH2を持つ翼
					whiteEval += MINUS;
				}
			}
		}

		if ((blackBlockComposition & BitPoint.A3.mask) != 0) {
			// 左辺が黒のブロック(ピュア/通常)
			// 上辺
			if ((blackMountainComposition & BitPoint.B1.mask) != 0 || (blackWingComposition & BitPoint.B1.mask) != 0) {
				// B1を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
			// 下辺
			if ((blackMountainComposition & BitPoint.B8.mask) != 0 || (blackWingComposition & BitPoint.B8.mask) != 0) {
				// B8を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
		} else if ((whiteBlockComposition & BitPoint.A3.mask) != 0) {
			// 左辺が白のブロック(ピュア/通常)
			// 上辺
			if ((whiteMountainComposition & BitPoint.B1.mask) != 0 || (whiteWingComposition & BitPoint.B1.mask) != 0) {
				// B1を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
			// 下辺
			if ((whiteMountainComposition & BitPoint.B8.mask) != 0 || (whiteWingComposition & BitPoint.B8.mask) != 0) {
				// B8を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
		}

		if ((blackBlockComposition & BitPoint.H3.mask) != 0) {
			// 右辺が黒のブロック(ピュア/通常)
			// 上辺
			if ((blackMountainComposition & BitPoint.G1.mask) != 0 || (blackWingComposition & BitPoint.G1.mask) != 0) {
				// G1を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
			// 下辺
			if ((blackMountainComposition & BitPoint.G8.mask) != 0 || (blackWingComposition & BitPoint.G8.mask) != 0) {
				// G8を含む辺を持つ
				blackEval += DISADVANTAGE;
			}
		} else if ((whiteBlockComposition & BitPoint.H3.mask) != 0) {
			// 右辺が白のブロック(ピュア/通常)
			// 上辺
			if ((whiteMountainComposition & BitPoint.G1.mask) != 0 || (whiteWingComposition & BitPoint.G1.mask) != 0) {
				// G1を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
			// 下辺
			if ((whiteMountainComposition & BitPoint.G8.mask) != 0 || (whiteWingComposition & BitPoint.G8.mask) != 0) {
				// G8を含む辺を持つ
				whiteEval += DISADVANTAGE;
			}
		} else {
			// ブロックでない場合Ａの頭の評価を行う
			// 左上隅
			if ((none & BitPoint.A1.mask) != 0 && (none & BitPoint.A2.mask) != 0 && (none & BitPoint.A3.mask) == 0) {
				if ((board.blackDiscs & BitPoint.A3.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.B3.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.A3.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.B3.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			if ((none & BitPoint.A1.mask) != 0 && (none & BitPoint.B1.mask) != 0 && (none & BitPoint.C1.mask) == 0) {
				if ((board.blackDiscs & BitPoint.C1.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.C2.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.C1.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.C2.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			// 右上隅
			if ((none & BitPoint.H1.mask) != 0 && (none & BitPoint.H2.mask) != 0 && (none & BitPoint.H3.mask) == 0) {
				if ((board.blackDiscs & BitPoint.H3.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.G3.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.H3.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.G3.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			if ((none & BitPoint.H1.mask) != 0 && (none & BitPoint.G1.mask) != 0 && (none & BitPoint.F1.mask) == 0) {
				if ((board.blackDiscs & BitPoint.F1.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.F2.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.F1.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.F2.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			// 左下隅
			if ((none & BitPoint.A8.mask) != 0 && (none & BitPoint.A7.mask) != 0 && (none & BitPoint.A6.mask) == 0) {
				if ((board.blackDiscs & BitPoint.A6.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.B6.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.A6.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.B6.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			if ((none & BitPoint.A8.mask) != 0 && (none & BitPoint.B8.mask) != 0 && (none & BitPoint.C8.mask) == 0) {
				if ((board.blackDiscs & BitPoint.C8.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.C7.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.C8.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.C7.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			// 右下隅
			if ((none & BitPoint.H8.mask) != 0 && (none & BitPoint.H7.mask) != 0 && (none & BitPoint.H6.mask) == 0) {
				if ((board.blackDiscs & BitPoint.H6.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.G6.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.H6.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.G6.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
			if ((none & BitPoint.H8.mask) != 0 && (none & BitPoint.G8.mask) != 0 && (none & BitPoint.F8.mask) == 0) {
				if ((board.blackDiscs & BitPoint.F8.mask) != 0) {
					// 黒Ａ打ち
					if ((board.whiteDiscs & BitPoint.F7.mask) != 0) {
						blackEval += FINE;
					}
				} else if ((board.whiteDiscs & BitPoint.F8.mask) != 0) {
					// 白Ａ打ち
					if ((board.blackDiscs & BitPoint.F7.mask) != 0) {
						whiteEval += FINE;
					}
				}
			}
		}

		// 双方Ｃ打ち
		if ((cs[0][0] & BitMask.UPPER_LINE.mask) != 0 && (cs[1][0] & BitMask.UPPER_LINE.mask) != 0) {
			// 上辺が双方Ｃ打ち
			final long blank = none & BitMask.UPPER_MOUNTAIN.mask;
			if (Long.bitCount(blank) == 1) {
				// １マス空き
				if (!board.canFlip(blank, ReversiColor.BLACK.getValue())) {
					// 黒着手不可能
					blackEval += DISADVANTAGE;
				}
				if (!board.canFlip(blank, ReversiColor.WHITE.getValue())) {
					// 白着手不可能
					whiteEval += DISADVANTAGE;
				}
			}
		}
		if ((cs[0][0] & BitMask.LOWER_LINE.mask) != 0 && (cs[1][0] & BitMask.LOWER_LINE.mask) != 0) {
			// 下辺が双方Ｃ打ち
			final long blank = none & BitMask.LOWER_MOUNTAIN.mask;
			if (Long.bitCount(blank) == 1) {
				// １マス空き
				if (!board.canFlip(blank, ReversiColor.BLACK.getValue())) {
					// 黒着手不可能
					blackEval += DISADVANTAGE;
				}
				if (!board.canFlip(blank, ReversiColor.WHITE.getValue())) {
					// 白着手不可能
					whiteEval += DISADVANTAGE;
				}
			}
		}
		if ((cs[0][0] & BitMask.LEFT_LINE.mask) != 0 && (cs[1][0] & BitMask.LEFT_LINE.mask) != 0) {
			// 左辺が双方Ｃ打ち
			final long blank = none & BitMask.LEFT_MOUNTAIN.mask;
			if (Long.bitCount(blank) == 1) {
				// １マス空き
				if (!board.canFlip(blank, ReversiColor.BLACK.getValue())) {
					// 黒着手不可能
					blackEval += DISADVANTAGE;
				}
				if (!board.canFlip(blank, ReversiColor.WHITE.getValue())) {
					// 白着手不可能
					whiteEval += DISADVANTAGE;
				}
			}
		}
		if ((cs[0][0] & BitMask.RIGHT_LINE.mask) != 0 && (cs[1][0] & BitMask.RIGHT_LINE.mask) != 0) {
			// 右辺が双方Ｃ打ち
			final long blank = none & BitMask.RIGHT_MOUNTAIN.mask;
			if (Long.bitCount(blank) == 1) {
				// １マス空き
				if (!board.canFlip(blank, ReversiColor.BLACK.getValue())) {
					// 黒着手不可能
					blackEval += DISADVANTAGE;
				}
				if (!board.canFlip(blank, ReversiColor.WHITE.getValue())) {
					// 白着手不可能
					whiteEval += DISADVANTAGE;
				}
			}
		}

		// TODO こういうのは別評価関数に切り出す
		// 辺の１マス空きに着手できるか
		// マスク
		long mask;
		// 窪みの位置
		long pocket = 0L;
		// 上辺
		mask = exist & BitMask.UPPER_LINE.mask;
		pocket |= none & BitMask.UPPER_LINE.mask & (mask << 1) & (mask >>> 1);
		// 下辺
		mask = exist & BitMask.LOWER_LINE.mask;
		pocket |= none & BitMask.LOWER_LINE.mask & (mask << 1) & (mask >>> 1);
		// 左辺
		mask = exist & BitMask.LEFT_LINE.mask;
		pocket |= none & BitMask.LEFT_LINE.mask & (mask << 8) & (mask >>> 8);
		// 右辺
		mask = exist & BitMask.RIGHT_LINE.mask;
		pocket |= none & BitMask.RIGHT_LINE.mask & (mask << 8) & (mask >>> 8);
		while (pocket != 0) {
			final long p = Long.lowestOneBit(pocket);
			if (p != 0) {
				if ((flips[0][0] & p) == 0) {
					blackEval += DISADVANTAGE;
				}
				if ((flips[1][0] & p) == 0) {
					whiteEval += DISADVANTAGE;
				}
				pocket &= ~p;
			}
		}

		return new int[][] { { Long.bitCount(blackMaskedLine), blackEval },
				{ Long.bitCount(whiteMaskedLine), whiteEval } };
	}

	/**
	 * 壁評価
	 *
	 * @param board 盤面
	 * @return 壁評価値 [0]:黒 [1]:白
	 */
	@Deprecated
	public static int[] evalWall(BitBoard board) {
		final long none = board.getNone();
		final long exist = ~none;
		int blackEval = 0;
		int whiteEval = 0;
		long mask;
		long t;

		// 上から
		mask = BitMask.UPPER_LINE.mask;
		for (t = exist & mask; t == 0; mask >>>= 8) {
			t = exist & mask;
		}
		blackEval += Math.pow(Long.bitCount(t & board.blackDiscs), 2);
		whiteEval += Math.pow(Long.bitCount(t & board.whiteDiscs), 2);

		// 下から
		mask = BitMask.LOWER_LINE.mask;
		for (t = exist & mask; t == 0; mask <<= 8) {
			t = exist & mask;
		}
		blackEval += Math.pow(Long.bitCount(t & board.blackDiscs), 2);
		whiteEval += Math.pow(Long.bitCount(t & board.whiteDiscs), 2);

		// 左から
		mask = BitMask.LEFT_LINE.mask;
		for (t = exist & mask; t == 0; mask >>>= 1) {
			t = exist & mask;
		}
		blackEval += Math.pow(Long.bitCount(t & board.blackDiscs), 2);
		whiteEval += Math.pow(Long.bitCount(t & board.whiteDiscs), 2);

		// 右から
		mask = BitMask.RIGHT_LINE.mask;
		for (t = exist & mask; t == 0; mask <<= 1) {
			t = exist & mask;
		}
		blackEval += Math.pow(Long.bitCount(t & board.blackDiscs), 2);
		whiteEval += Math.pow(Long.bitCount(t & board.whiteDiscs), 2);

		return new int[] { blackEval, whiteEval };
	}

	/**
	 * 包囲評価
	 *
	 * @param board 盤面
	 * @return 包囲評価値 [0]:黒 [1]:白
	 */
	public static long[] evalOpen(BitBoard board) {
		final long none = board.getNone();

		// ビット演算用変数
		long t = 0L;

		// 黒
		// 左上方向
		t |= (board.blackDiscs & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		t |= (board.blackDiscs & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		t |= (board.blackDiscs & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		t |= (board.blackDiscs & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		t |= (board.blackDiscs & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		t |= (board.blackDiscs & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		t |= (board.blackDiscs & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		t |= (board.blackDiscs & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
		// 黒の集計
		final long black = t & none & ~BitMask.ALL_XS.mask;

		t = 0L;
		// 白
		// 左上方向
		t |= (board.whiteDiscs & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		t |= (board.whiteDiscs & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		t |= (board.whiteDiscs & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		t |= (board.whiteDiscs & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		t |= (board.whiteDiscs & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		t |= (board.whiteDiscs & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		t |= (board.whiteDiscs & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		t |= (board.whiteDiscs & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
		// 白の集計
		final long white = t & none & ~BitMask.ALL_XS.mask;

		return new long[] { black, white };
	}

	/**
	 * 完全に囲まれている石の評価値を返す
	 * <p>
	 * 評価盤面のビット列を返す
	 *
	 * @param board 盤面
	 * @return 包囲評価値 [0]:黒 [1]:白
	 */
	public static long[] evalSurrounded(BitBoard board) {
		long t = 0L;
		final long none = board.getNone();
		// 左上方向
		t |= (none & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		t |= (none & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		t |= (none & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		t |= (none & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		t |= (none & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		t |= (none & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		t |= (none & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		t |= (none & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
		// ビット反転
		t = ~t;

		long arroundBlack = 0L;
		final long black = t & board.blackDiscs;
		arroundBlack |= (black & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		arroundBlack |= (black & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		arroundBlack |= (black & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		arroundBlack |= (black & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		arroundBlack |= (black & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		arroundBlack |= (black & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		arroundBlack |= (black & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		arroundBlack |= (black & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
		arroundBlack = arroundBlack & board.whiteDiscs & BitMask.DIAGONAL_DOUBLE_REVERSE.mask;

		long arroundWhite = 0L;
		long white = t & board.whiteDiscs;
		arroundWhite |= (white & BitMask.UPPER_LEFT_REVERSE.mask) << 9;
		// 上方向
		arroundWhite |= (white & BitMask.UPPER_LINE_REVERSE.mask) << 8;
		// 右上方向
		arroundWhite |= (white & BitMask.UPPER_RIGHT_REVERSE.mask) << 7;
		// 左方向
		arroundWhite |= (white & BitMask.LEFT_LINE_REVERSE.mask) << 1;
		// 右方向
		arroundWhite |= (white & BitMask.RIGHT_LINE_REVERSE.mask) >>> 1;
		// 左下方向
		arroundWhite |= (white & BitMask.LOWER_LEFT_REVERSE.mask) >>> 7;
		// 下方向
		arroundWhite |= (white & BitMask.LOWER_LINE_REVERSE.mask) >>> 8;
		// 右下方向
		arroundWhite |= (white & BitMask.LOWER_RIGHT_REVERSE.mask) >>> 9;
		arroundWhite = arroundWhite & board.blackDiscs & BitMask.DIAGONAL_DOUBLE_REVERSE.mask;

		return new long[] { arroundBlack, arroundWhite };
	}

	/**
	 * 隅の着手可能評価
	 *
	 * @param blackFlip 黒の着手可能位置情報
	 * @param whiteFlip 白の着手可能位置情報
	 * @return 隅の着手可能評価 [0]:黒 [1]:白
	 */
	public static int[] evalEnableGetCorner(long blackFlip, long whiteFlip) {
		final int[] enableGetCorner = new int[] { 0, 0 };
		enableGetCorner[0] = Long.bitCount(blackFlip & BitMask.ALL_CORNERS.mask);
		enableGetCorner[1] = Long.bitCount(whiteFlip & BitMask.ALL_CORNERS.mask);
		return enableGetCorner;
	}

	/**
	 * 黒・白の評価値を元に指定した色の偏差値を算出する
	 *
	 * @param evaluation 評価値 [0]:黒 [1]:白
	 * @param baseColor 基準色
	 * @return 偏差値
	 */
	public static double stddev(double[] evaluation, int baseColor) {
		final int coefficient = 500;
		final double average = (evaluation[0] + evaluation[1] + coefficient) * 0.33333333333333333333333d; // 平均
		final double blackDeviation = evaluation[0] - average; // 偏差(黒)
		final double coefficientDeviation = coefficient - average; // 偏差(係数)
		final double whiteDeviation = evaluation[1] - average; // 偏差(白)
		final double dispersion = ((blackDeviation * blackDeviation) + (coefficientDeviation * coefficientDeviation)
				+ (whiteDeviation * whiteDeviation)) * 0.33333333333333333333333d; // 分散
		final double standardDeviation = Math.sqrt(dispersion); // 標準偏差
		final double blackDeviationValue = (blackDeviation / standardDeviation) * 10 + 50; // 偏差値
		final double whiteDeviationValue = (whiteDeviation / standardDeviation) * 10 + 50; // 偏差値
		return (baseColor == ReversiColor.BLACK.getValue())
				? blackDeviationValue / whiteDeviationValue
				: whiteDeviationValue / blackDeviationValue;
	}

	/**
	 * 最終局面の石差を算出する
	 *
	 * @param black 黒の石の数
	 * @param white 白の石の数
	 * @param baseColor 基準色
	 * @return 石差
	 */
	public static int finalCount(int black, int white, int baseColor) {
		return (baseColor == ReversiColor.BLACK.getValue()) ? black - white : white - black;
	}
}
