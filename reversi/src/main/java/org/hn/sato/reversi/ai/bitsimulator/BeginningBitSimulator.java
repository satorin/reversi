package org.hn.sato.reversi.ai.bitsimulator;

import java.util.Arrays;

import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitEvalResult;
import org.hn.sato.reversi.bit.BitMask;
import org.hn.sato.reversi.bit.SortableBitPoint;

/**
 * 序盤の戦略シミュレーター
 *
 * @author s-kotaki
 *
 */
public class BeginningBitSimulator extends AbstractBitSimulator {

	/** 確定石評価値 */
	private static final double VAL_FIXED = 15;
	/** 自石評価値 */
	private static final double VAL_DISC = -1.71;
	/** 余裕手評価値 */
	private static final double VAL_ADVANTAGE = 0.8;
	/** 隅評価値 */
	private static final double VAL_CORNER = 36;
	/** Ⅹ評価値 */
	private static final double VAL_X = -24.4;
	/** 単独Ｃ評価値 */
	private static final double VAL_C_UNI = -32.4;
	/** Ｃ評価値 */
	private static final double VAL_C = -10.4;
//	/** 山(ピュア)評価値 */
//	private static final double VAL_YAMA_PURE = 15.1;
//	/** 山評価値 */
//	private static final double VAL_YAMA = 10.2;
//	/** 翼(ピュア)評価値 */
//	private static final double VAL_TSUBASA_PURE = -20.8;
//	/** 翼評価値 */
//	private static final double VAL_TSUBASA = -15.8;
	/** 開放度評価値 */
	private static final double VAL_OPEN = -2.88;
	/** 包囲評価値 */
	private static final double VAL_SURROUNDED = 1.95;
	/** 手数評価値 */
	private static final double VAL_FLIP = 1.43;
	/** 辺の個数評価値 */
	private static final double VAL_LINE_COUNT = -2.50;
	/** 辺の形体評価値 */
	private static final double VAL_LINE_SHAPE = 1.73;
	/** Ａの頭の評価値 */
	private static final double VAL_HEAD_OF_A = 15;


	@Override
	public final EvalResult eval(BitBoard board, BitBoard before, int color, int baseColor) {

		final BitEvalResult result = new BitEvalResult(board, baseColor);

		// 空白
		final long none = board.getNone();
		final int countNone = Long.bitCount(none);
		// 係数１(現在空白数の10% 序盤5.4～終盤0.9まで変動)
		final double coefficient1 = countNone * 0.1;

		// 確定石の評価
		final long[] fixed = BitEval.evalFixedDisc(board);
		// 手数の評価
		final long[][] flips = BitEval.evalFlip(board);
		// 隅の評価
		final long[] corners = BitEval.evalCorner(board);
		// Ⅹの評価
		final long[] xs = BitEval.evalX(board, fixed);
		// 山の評価
		final long[][] mountains = BitEval.evalMountain(board);
		// 翼の評価
		final long[][] wings = BitEval.evalWing(board, flips);
		// ブロックの評価
		final long[][] blocks = BitEval.evalBlock(board);
		// Ｃの評価
		final long[][] cs = BitEval.evalC(board, fixed, mountains, wings, blocks);
		// 開放度の評価
		final long[] open = BitEval.evalOpen(board);
		// 包囲の評価値
		final long[] surrounded = BitEval.evalSurrounded(board);
		// 辺の総合評価
		final int[][] lines = BitEval.evalLine(board, mountains, wings, blocks, fixed, cs, flips);

		// 隅着手評価
		if (color == ReversiColor.BLACK.getValue()) {
			// 黒の前回の隅着手可能位置
			final long beforeFlip = before.getFlipPoint(ReversiColor.BLACK.getValue()) & BitMask.ALL_CORNERS.mask;
			// 黒の隅取得可否(以前から打てるところのみ評価対象とする)
			result.enableGetCorner[0] = Long.bitCount(flips[0][0] & BitMask.ALL_CORNERS.mask & beforeFlip) * VAL_CORNER;
		} else {
			// 黒の隅取得可否(全て評価対象)
			result.enableGetCorner[0] = Long.bitCount(flips[0][0] & BitMask.ALL_CORNERS.mask) * VAL_CORNER;
		}
		result.fixed[0] = Long.bitCount(fixed[0]) * VAL_FIXED;
		// 黒の数
		result.disc[0] = board.countBlack() * VAL_DISC;
		// 黒の余裕手評価
		result.advantage[0] = Long.bitCount(flips[0][2]) * Long.bitCount(flips[0][3]) * VAL_ADVANTAGE / coefficient1;
		// 黒の隅の評価
		result.corner[0] = Long.bitCount(corners[0]) * VAL_CORNER;
		// 黒のⅩの評価
		result.x[0] = Long.bitCount(xs[0]) * VAL_X;
		// 黒のＣの評価
		result.c[0] = Long.bitCount(cs[0][0]) * VAL_C + Long.bitCount(cs[0][1]) * VAL_C_UNI;
//		// 黒の山の評価
//		result.mountain[0] = (Long.bitCount(mountains[0][0] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA
//				+ (Long.bitCount(mountains[0][1] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA_PURE;
//		// 黒の翼の評価
//		result.wing[0] = Long.bitCount(wings[0][0] & BitMask.ALL_CS.mask) * VAL_TSUBASA
//				+ Long.bitCount(wings[0][1] & BitMask.ALL_CS.mask) * VAL_TSUBASA_PURE;
		// 黒の開放度評価
		result.open[0] = Long.bitCount(open[0]) * VAL_OPEN;
		// 黒の包囲評価
		result.surrounded[0] = Long.bitCount(surrounded[0]) * VAL_SURROUNDED;
		// 黒の手数の評価
		result.flip[0] = Long.bitCount(flips[0][1]) * Long.bitCount(flips[0][3]) * VAL_FLIP / coefficient1;
		// 黒の辺の総合評価
		result.line[0] = lines[0][0] * VAL_LINE_COUNT + lines[0][1] * VAL_LINE_SHAPE;

		// 隅着手評価
		if (color == ReversiColor.WHITE.getValue()) {
			// 白の前回の隅着手可能位置
			final long beforeFlip = before.getFlipPoint(ReversiColor.WHITE.getValue()) & BitMask.ALL_CORNERS.mask;
			// 白の隅取得可否(以前から打てるところのみ評価対象とする)
			result.enableGetCorner[1] = Long.bitCount(flips[1][0] & BitMask.ALL_CORNERS.mask & beforeFlip) * VAL_CORNER;
		} else {
			// 白の隅取得可否(全て評価対象)
			result.enableGetCorner[1] = Long.bitCount(flips[1][0] & BitMask.ALL_CORNERS.mask) * VAL_CORNER;
		}
		result.fixed[1] = Long.bitCount(fixed[1]) * VAL_FIXED;
		// 白の数
		result.disc[1] = board.countWhite() * VAL_DISC;
		// 白の余裕手評価
		result.advantage[1] = Long.bitCount(flips[1][2]) * Long.bitCount(flips[1][3]) * VAL_ADVANTAGE / coefficient1;
		// 白の隅の評価
		result.corner[1] = Long.bitCount(corners[1]) * VAL_CORNER;
		// 白のⅩの評価
		result.x[1] = Long.bitCount(xs[1]) * VAL_X;
		// 白のＣの評価
		result.c[1] = Long.bitCount(cs[1][0]) * VAL_C + Long.bitCount(cs[1][1]) * VAL_C_UNI;
//		// 白の山の評価
//		result.mountain[1] = (Long.bitCount(mountains[1][0] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA
//				+ (Long.bitCount(mountains[1][1] & BitMask.ALL_CS.mask) >>> 1) * VAL_YAMA_PURE;
//		// 白の翼の評価
//		result.wing[1] = Long.bitCount(wings[1][0] & BitMask.ALL_CS.mask) * VAL_TSUBASA
//				+ Long.bitCount(wings[1][1] & BitMask.ALL_CS.mask) * VAL_TSUBASA_PURE;
		// 白の開放度評価
		result.open[1] = Long.bitCount(open[1]) * VAL_OPEN;
		// 白の包囲評価
		result.surrounded[1] = Long.bitCount(surrounded[1]) * VAL_SURROUNDED;
		// 白の手数の評価
		result.flip[1] = Long.bitCount(flips[1][1]) * Long.bitCount(flips[1][3]) * VAL_FLIP / coefficient1;
		// 白の辺の総合評価
		result.line[1] = lines[1][0] * VAL_LINE_COUNT + lines[1][1] * VAL_LINE_SHAPE;

		result.setEvaluation(BitEval.stddev(result.calcEvaluation(), baseColor));

		return result;
	}

	@Override
	public long[] sortedFlip(BitBoard board, int color) {
		final long flipPoint = board.getFlipPoint(color);
		final long corners = flipPoint & BitMask.ALL_CORNERS.mask;
		final long xs = flipPoint & BitMask.ALL_XS.mask;
		final long cs = flipPoint & BitMask.ALL_CS.mask;
		final long normals = flipPoint & ~corners & ~xs & ~cs;

		// 開放度順のソート
		final long[] normalArray = Arrays.stream(BitBoardUtil.toPointArray(normals)).mapToObj(point -> {
			final int openRatio = BitEval.calcOpenRatioEx2(board, point, color);
			return new SortableBitPoint(openRatio, point);
		}).sorted((a, b) -> Double.compare(a.sortKey, b.sortKey)).mapToLong(point -> point.point).toArray();
		final long[] cornerArray = BitBoardUtil.toPointArray(corners);
		final long[] xArray = BitBoardUtil.toPointArray(xs);
		final long[] cArray = BitBoardUtil.toPointArray(cs);
		final long[] pointArray = new long[Long.bitCount(flipPoint)];
		System.arraycopy(cornerArray, 0, pointArray, 0, cornerArray.length);
		System.arraycopy(normalArray, 0, pointArray, cornerArray.length, normalArray.length);
		System.arraycopy(cArray, 0, pointArray, cornerArray.length + normalArray.length, cArray.length);
		System.arraycopy(xArray, 0, pointArray, cornerArray.length + normalArray.length + cArray.length, xArray.length);

		return pointArray;
	}
}
