package org.hn.sato.reversi.ai.simulator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;
import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ReversiPoint;

import lombok.ToString;

/**
 * 評価関数
 *
 * @author s-kotaki
 *
 */
public final class SimulatorEval {

	/**
	 * 着手可能リストを有利な順にソートして返却する
	 * <p>
	 * 以下条件を適用<br>
	 * <ol>
	 * <li>隅に着手できる場合は先頭に配置
	 * <li>Ｃ打ち、Ⅹ打ち以外の着手位置を開放度の小さい順に配置
	 * <li>Ｃ打ちを配置
	 * <li>Ⅹ打ちを末尾に配置
	 * </ol>
	 * <p>
	 * 盤面の状態は変わらない。
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return ソートした着手可能リスト
	 */
	public static List<ReversiPoint> getFlipPointListOrderByPriority(ReversiBoard board, final int color) {

		final List<ReversiPoint> cornerList = new ArrayList<>();
		final List<SortablePoint> sortablePointList = new ArrayList<>();
		final List<ReversiPoint> cList = new ArrayList<>();
		final List<ReversiPoint> xList = new ArrayList<>();
		for (ReversiPoint point : board.getFlipPointList(color)) {
			if (point.equals(ReversiPoint.A1) || point.equals(ReversiPoint.A8) || point.equals(ReversiPoint.H1)
					|| point.equals(ReversiPoint.H8)) {
				// 隅
				cornerList.add(point);
			} else if (point.equals(ReversiPoint.B2) || point.equals(ReversiPoint.B7) || point.equals(ReversiPoint.G2)
					|| point.equals(ReversiPoint.G7)) {
				// Ⅹ
				xList.add(point);
			} else if (point.equals(ReversiPoint.A2) || point.equals(ReversiPoint.A7) || point.equals(ReversiPoint.B1)
					|| point.equals(ReversiPoint.B8) || point.equals(ReversiPoint.G1) || point.equals(ReversiPoint.G8)
					|| point.equals(ReversiPoint.H2) || point.equals(ReversiPoint.H7)) {
				// Ｃ
				cList.add(point);
			} else {
				final int openRatio = SimulatorEval.calcOpenRatio(board, point, color);
				sortablePointList.add(new SortablePoint(openRatio, point));
			}
		}

		// 開放度順のソート
		final List<ReversiPoint> normalList = sortablePointList.stream()
				.sorted((a, b) -> Double.compare(a.sortKey, b.sortKey)).map(p -> p.point).collect(Collectors.toList());

		cornerList.addAll(normalList);
		cornerList.addAll(cList);
		cornerList.addAll(xList);
		return cornerList;
	}

	/**
	 * 反復進化探索を行って着手可能リストを有利な順にソートする
	 * <p>
	 * 盤面の状態は変わらない。
	 *
	 * @param board 盤面
	 * @param color 色
	 * @param baseColor 基準色
	 * @param depth 探索の深さ
	 * @return ソートした着手可能リスト
	 * @throws InterruptedException 割り込み例外
	 */
	public static List<ReversiPoint> getFlipPointListIterativeDeepening(ReversiBoard board, int color,
			int baseColor, int depth) throws InterruptedException {
		final List<SortablePoint> sortablePointList = new ArrayList<>();
		double alpha = Double.NEGATIVE_INFINITY;
		final double beta = Double.POSITIVE_INFINITY;
		for (ReversiPoint point : board.getFlipPointList(color)) {
			final ReversiBoard newBoard = board.clone();
			newBoard.flip(point, color);
			final double value = SimulatorEval.recurseIterativeDeeping(newBoard, -color, baseColor, depth - 1, alpha,
					beta);
			sortablePointList.add(new SortablePoint(Double.valueOf(value), point));
		}
		// 評価値順のソート
		return sortablePointList.stream().sorted((a, b) -> color == baseColor ? Double.compare(b.sortKey, a.sortKey)
				: Double.compare(a.sortKey, b.sortKey)).map(p -> p.point).collect(Collectors.toList());
	}

	private static final double recurseIterativeDeeping(ReversiBoard board, int color, int baseColor, int depth,
			double alpha,
			double beta) throws InterruptedException {

		if (depth < 1) {
			// αβ法では評価の基準は同一
			return SimulatorEval.eval(board, baseColor);
		}

		int turn = color;
		if (!board.canFlip(turn)) {
			// パスの場合
			Thread.sleep(1000);
			turn = -turn;
			if (!board.canFlip(turn)) {
				// 双方パスの場合
				return SimulatorEval.eval(board, baseColor);
			}
		}

		double result = Double.NEGATIVE_INFINITY;

		for (ReversiPoint movablePoint : SimulatorEval.getFlipPointListOrderByPriority(board, turn)) {
			final ReversiBoard newBoard = board.clone();
			newBoard.flip(movablePoint, turn);
			final double value = SimulatorEval.recurseIterativeDeeping(newBoard, -turn, baseColor, depth - 1, alpha,
					beta);
			// Min-Max法による評価判定
			if (turn == baseColor) {
				if (beta < value) {
					// βカット
					return value;
				}
				if (result < value) {
					// αを更新(大きければ)
					alpha = Math.max(alpha, result);
					// 現時点での自分の最善手評価結果を保持
					result = value;
				}
			} else {
				if (value < alpha) {
					// αカット
					return value;
				}
				if (value < result) {
					// βを更新(小さければ)
					beta = Math.min(beta, result);
					// 現時点での相手の最善手評価結果を保持
					result = value;
				}
			}
		}
		return result;
	}

	private static final double eval(ReversiBoard board, int baseColor) {

		final double VAL_FIXED = 10;
		final double VAL_DISC = -2;
		final double VAL_CORNER = 50;
		final double VAL_X = -40;
		final double VAL_C = -25;
		final double VAL_YAMA = 70;
		final double VAL_TSUBASA = -20;
		final double VAL_SURROUNDED = -2;
		final double VAL_MOVABLE = 1.2;

		final boolean checked[][] = new boolean[ReversiBoard.SIZE][ReversiBoard.SIZE];

		double black = 0;
		// 黒の確定石の評価
		black += SimulatorEval.evalFixedDisc(board, ReversiColor.BLACK.getValue(), checked) * VAL_FIXED;
		// 黒の数
		black += board.count(ReversiColor.BLACK.getValue()) * VAL_DISC;
		// 黒の隅の評価
		black += SimulatorEval.evalCorner(board, ReversiColor.BLACK.getValue()) * VAL_CORNER;
		// 黒のⅩの評価
		black += SimulatorEval.evalX(board, ReversiColor.BLACK.getValue(), checked) * VAL_X;
		// 黒のＣの評価
		black += SimulatorEval.evalC(board, ReversiColor.BLACK.getValue(), checked) * VAL_C;
		// 黒の山の評価
		black += SimulatorEval.evalMountain(board, ReversiColor.BLACK.getValue()) * VAL_YAMA;
		// 黒の翼の評価
		black += SimulatorEval.evalWing(board, ReversiColor.BLACK.getValue()) * VAL_TSUBASA;
		// 黒の包囲評価
		black += SimulatorEval.evalSurrounded(board, ReversiColor.BLACK.getValue()) * VAL_SURROUNDED;
		// 黒の手数の評価
		black += SimulatorEval.evalFlipCount(board, ReversiColor.BLACK.getValue()) * VAL_MOVABLE;

		double white = 0;
		// 白の確定石の評価
		white += SimulatorEval.evalFixedDisc(board, ReversiColor.WHITE.getValue(), checked) * VAL_FIXED;
		// 白の数
		white += board.count(ReversiColor.WHITE.getValue()) * VAL_DISC;
		// 白の隅の評価
		white += SimulatorEval.evalCorner(board, ReversiColor.WHITE.getValue()) * VAL_CORNER;
		// 白のⅩの評価
		white += SimulatorEval.evalX(board, ReversiColor.WHITE.getValue(), checked) * VAL_X;
		// 白のＣの評価
		white += SimulatorEval.evalC(board, ReversiColor.WHITE.getValue(), checked) * VAL_C;
		// 白の山の評価
		white += SimulatorEval.evalMountain(board, ReversiColor.WHITE.getValue()) * VAL_YAMA;
		// 白の翼の評価
		white += SimulatorEval.evalWing(board, ReversiColor.WHITE.getValue()) * VAL_TSUBASA;
		// 白の包囲評価
		white += SimulatorEval.evalSurrounded(board, ReversiColor.WHITE.getValue()) * VAL_SURROUNDED;
		// 白の手数の評価
		white += SimulatorEval.evalFlipCount(board, ReversiColor.WHITE.getValue()) * VAL_MOVABLE;

		return (baseColor == ReversiColor.BLACK.getValue()) ? black - white : white - black;
	}

	/**
	 * 着手位置ソート用クラス
	 *
	 * @author s-kotaki
	 *
	 */
	@ToString
	private static class SortablePoint implements Serializable {
		/** serialVersionUID */
		private static final long serialVersionUID = -8407430237506757917L;
		/** ソートキー */
		private double sortKey;
		/** 着手位置 */
		private ReversiPoint point;

		/**
		 * コンストラクタ
		 *
		 * @param sortKey 評価値
		 * @param point 着手位置
		 */
		public SortablePoint(double sortKey, ReversiPoint point) {
			this.sortKey = sortKey;
			this.point = point;
		}
	}

	/**
	 * 開放度を計算する
	 * <p>
	 * 盤面の状態は変わらない。
	 *
	 * @param board 盤面
	 * @param point 着手位置
	 * @param color 色
	 * @return 開放度評価値
	 */
	public static int calcOpenRatio(ReversiBoard board, ReversiPoint point, int color) {
		int evaluation = 0;
		final boolean check[][] = new boolean[ReversiBoard.SIZE][ReversiBoard.SIZE];
		ReversiBoard clone = board.clone();
		final List<ReversiPoint> movedPointList = clone.flip(point, color);
		for (ReversiPoint movedPoint : movedPointList) {
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					final int posX = movedPoint.x + x;
					final int posY = movedPoint.y + y;
					if (ReversiPoint.isValidRange(posX, posY) && !check[posX][posY]) {
						final int nextColor = clone.getColor(posX, posY);
						if (nextColor == ReversiColor.NONE.getValue()) {
							check[posX][posY] = true;
							evaluation++;
						}
					}
				}
			}
		}
		return evaluation;
	}

	/**
	 * 確定石の評価関数
	 *
	 * @param board 盤面
	 * @param color 色
	 * @param fixed 確定石の盤面
	 * @return 確定石評価値
	 */
	public static int evalFixedDisc(ReversiBoard board, int color, boolean[][] fixed) {
		int evaluation = 0;
		int x = 0;
		int y = 0;
		// 上辺
		for (x = 0; x <= 7 && board.getColor(x, 0) != ReversiColor.NONE.getValue(); x++)
			;
		if (x == 8) {
			// 上辺が全て埋まっている場合
			for (x = 0; x <= 7; x++) {
				if (!fixed[x][0] && board.getColor(x, 0) == color) {
					evaluation++;
					fixed[x][0] = true;
				}
			}
		} else {
			// 左上隅
			if (board.getColor(ReversiPoint.A1) == color) {
				if (!fixed[0][0]) {
					evaluation++;
					fixed[0][0] = true;
				}
				// 上ライン(左⇒右)
				for (x = 1; x < 7 && board.getColor(x, 0) == color; x++) {
					if (!fixed[x][0]) {
						evaluation++;
						fixed[x][0] = true;
					}
				}
			}
			// 右上隅
			if (board.getColor(ReversiPoint.H1) == color) {
				if (!fixed[7][0]) {
					evaluation++;
					fixed[7][0] = true;
				}
				// 上ライン(右⇒左)
				for (x = 6; 0 < x && board.getColor(x, 0) == color; x--) {
					if (!fixed[x][0]) {
						evaluation++;
						fixed[x][0] = true;
					}
				}
			}
		}

		// 下辺
		for (x = 0; x <= 7 && board.getColor(x, 7) != ReversiColor.NONE.getValue(); x++)
			;
		if (x == 8) {
			// 下辺全て埋まっている場合
			for (x = 0; x <= 7; x++) {
				if (!fixed[x][7] && board.getColor(x, 7) == color) {
					evaluation++;
					fixed[x][7] = true;
				}
			}
		} else {
			// 左下隅
			if (board.getColor(ReversiPoint.A8) == color) {
				if (!fixed[0][7]) {
					evaluation++;
					fixed[0][7] = true;
				}
				// 下ライン(左⇒右)
				for (x = 1; x < 7 && board.getColor(x, 7) == color; x++) {
					if (!fixed[x][7]) {
						evaluation++;
						fixed[x][7] = true;
					}
				}
			}
			// 右下隅
			if (board.getColor(ReversiPoint.H8) == color) {
				if (!fixed[7][7]) {
					evaluation++;
					fixed[7][7] = true;
				}
				// 下ライン(右⇒左)
				for (x = 6; 0 < x && board.getColor(x, 7) == color; x--) {
					if (!fixed[x][7]) {
						evaluation++;
						fixed[x][7] = true;
					}
				}
			}
		}

		// 左辺
		for (y = 0; y <= 7 && board.getColor(0, y) != ReversiColor.NONE.getValue(); y++)
			;
		if (y == 8) {
			// 左辺が全て埋まっている場合
			for (y = 0; y <= 7; y++) {
				if (!fixed[0][y] && board.getColor(0, y) == color) {
					evaluation++;
					fixed[0][y] = true;
				}
			}
		} else {
			// 左上隅
			if (board.getColor(ReversiPoint.A1) == color) {
				if (!fixed[0][0]) {
					evaluation++;
					fixed[0][0] = true;
				}
				// 左ライン(上⇒下)
				for (y = 1; y < 7 && board.getColor(0, y) == color; y++) {
					if (!fixed[0][y]) {
						evaluation++;
						fixed[0][y] = true;
					}
				}
			}
			// 左下隅
			if (board.getColor(ReversiPoint.A8) == color) {
				if (!fixed[0][7]) {
					evaluation++;
					fixed[0][7] = true;
				}
				// 左ライン(下⇒上)
				for (y = 6; 0 < y && board.getColor(0, y) == color; y--) {
					if (!fixed[0][y]) {
						evaluation++;
						fixed[0][y] = true;
					}
				}
			}

		}

		// 右辺
		for (y = 0; y <= 7 && board.getColor(7, y) != ReversiColor.NONE.getValue(); y++)
			;
		if (y == 8) {
			// 右辺が全て埋まっている場合
			for (y = 0; y <= 7; y++) {
				if (!fixed[7][y] && board.getColor(7, y) == color) {
					evaluation++;
					fixed[7][y] = true;
				}
			}
		} else {
			// 右上隅
			if (board.getColor(ReversiPoint.H1) == color) {
				if (!fixed[7][0]) {
					evaluation++;
					fixed[7][0] = true;
				}
				// 右ライン(上⇒下)
				for (y = 1; y < 7 && board.getColor(7, y) == color; y++) {
					if (!fixed[7][y]) {
						evaluation++;
						fixed[7][y] = true;
					}
				}
			}
			// 右下隅
			if (board.getColor(ReversiPoint.H8) == color) {
				if (!fixed[7][7]) {
					evaluation++;
					fixed[7][7] = true;
				}
				// 右ライン(下⇒上)
				for (y = 6; 0 < y && board.getColor(7, y) == color; y--) {
					if (!fixed[7][y]) {
						evaluation++;
						fixed[7][y] = true;
					}
				}
			}
		}

		// １段内側
		// 左上隅
		if (fixed[0][0] && fixed[0][1] && fixed[1][0]) {
			if (board.getColor(ReversiPoint.B2) == color) {
				// 左⇒右
				for (x = 1; x < 7 && board.getColor(x, 0) == color && board.getColor(x, 1) == color; x++) {
					if (fixed[x][1]) {
						evaluation++;
						fixed[x][1] = true;
					}
				}
				// 上⇒下
				for (y = 1; y < 7 && board.getColor(0, y) == color && board.getColor(1, y) == color; y++) {
					if (fixed[1][y]) {
						evaluation++;
						fixed[1][y] = true;
					}
				}
			}
		}

		// 左下隅
		if (fixed[0][7] && fixed[0][6] && fixed[1][7]) {
			if (board.getColor(ReversiPoint.B7) == color) {
				// 左⇒右
				for (x = 1; x < 7 && board.getColor(x, 7) == color && board.getColor(x, 6) == color; x++) {
					if (fixed[x][6]) {
						evaluation++;
						fixed[x][6] = true;
					}
				}
				// 下⇒上
				for (y = 6; 0 < y && board.getColor(0, y) == color && board.getColor(1, y) == color; y--) {
					if (fixed[1][y]) {
						evaluation++;
						fixed[1][y] = true;
					}
				}
			}
		}

		// 右上隅
		if (fixed[7][0] && fixed[6][0] && fixed[7][1]) {
			if (board.getColor(ReversiPoint.G2) == color) {
				// 右⇒左
				for (x = 6; 0 < x && board.getColor(x, 7) == color && board.getColor(x, 6) == color; x--) {
					if (fixed[x][6]) {
						evaluation++;
						fixed[x][6] = true;
					}
				}
				// 上⇒下
				for (y = 1; y < 7 && board.getColor(7, y) == color && board.getColor(6, y) == color; y++) {
					if (fixed[6][y]) {
						evaluation++;
						fixed[6][y] = true;
					}
				}
			}
		}

		// 右下隅
		if (fixed[7][7] && fixed[6][7] && fixed[7][6]) {
			if (board.getColor(ReversiPoint.G7) == color) {
				// 右⇒左
				for (x = 6; 0 < x && board.getColor(x, 7) == color && board.getColor(x, 6) == color; x--) {
					if (fixed[x][6]) {
						evaluation++;
						fixed[x][6] = true;
					}
				}
				// 下⇒上
				for (y = 6; 0 < y && board.getColor(0, y) == color && board.getColor(1, y) == color; y--) {
					if (fixed[6][y]) {
						evaluation++;
						fixed[6][y] = true;
					}
				}
			}
		}
		return evaluation;
	}

	/**
	 * 隅の評価関数
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return 隅評価値
	 */
	public static int evalCorner(ReversiBoard board, int color) {
		int evaluation = 0;
		if (board.getColor(ReversiPoint.A1) == color) {
			evaluation++;
		}
		if (board.getColor(ReversiPoint.A8) == color) {
			evaluation++;
		}
		if (board.getColor(ReversiPoint.H1) == color) {
			evaluation++;
		}
		if (board.getColor(ReversiPoint.H8) == color) {
			evaluation++;
		}
		return evaluation;
	}

	/**
	 * Xの評価関数
	 *
	 * @param board 盤面
	 * @param color 色
	 * @param fixed 確定石の盤面
	 * @return Ⅹ評価値
	 */
	public static int evalX(ReversiBoard board, int color, boolean[][] fixed) {
		int evaluation = 0;
		if (board.getColor(ReversiPoint.A1) != color && board.getColor(ReversiPoint.B2) == color
				&& !fixed[ReversiPoint.B2.x][ReversiPoint.B2.y]) {
			evaluation++;
		}
		if (board.getColor(ReversiPoint.A8) != color && board.getColor(ReversiPoint.B7) == color
				&& !fixed[ReversiPoint.B7.x][ReversiPoint.B7.y]) {
			evaluation++;
		}
		if (board.getColor(ReversiPoint.H1) != color && board.getColor(ReversiPoint.G2) == color
				&& !fixed[ReversiPoint.G2.x][ReversiPoint.G2.y]) {
			evaluation++;
		}
		if (board.getColor(ReversiPoint.H8) != color && board.getColor(ReversiPoint.G7) == color
				&& !fixed[ReversiPoint.G7.x][ReversiPoint.G7.y]) {
			evaluation++;
		}
		return evaluation;
	}

	/**
	 * Ｃの評価関数
	 *
	 * @param board 盤面
	 * @param color 色
	 * @param fixed 確定石の盤面
	 * @return Ｃ評価値
	 */
	public static int evalC(ReversiBoard board, int color, boolean[][] fixed) {
		int evaluation = 0;
		if (board.getColor(ReversiPoint.A1) != color) {
			if (board.getColor(ReversiPoint.A2) == color && !fixed[ReversiPoint.A2.x][ReversiPoint.A2.y]) {
				evaluation++;
			}
			if (board.getColor(ReversiPoint.B1) == color && !fixed[ReversiPoint.B1.x][ReversiPoint.B1.y]) {
				evaluation++;
			}
		}
		if (board.getColor(ReversiPoint.A8) != color) {
			if (board.getColor(ReversiPoint.A7) == color && !fixed[ReversiPoint.A7.x][ReversiPoint.A7.y]) {
				evaluation++;
			}
			if (board.getColor(ReversiPoint.B8) == color && !fixed[ReversiPoint.B8.x][ReversiPoint.B8.y]) {
				evaluation++;
			}
		}
		if (board.getColor(ReversiPoint.H1) != color) {
			if (board.getColor(ReversiPoint.G1) == color && !fixed[ReversiPoint.G1.x][ReversiPoint.G1.y]) {
				evaluation++;
			}
			if (board.getColor(ReversiPoint.H2) == color && !fixed[ReversiPoint.H2.x][ReversiPoint.H2.y]) {
				evaluation++;
			}
		}
		if (board.getColor(ReversiPoint.H8) != color) {
			if (board.getColor(ReversiPoint.G8) == color && !fixed[ReversiPoint.G8.x][ReversiPoint.G8.y]) {
				evaluation++;
			}
			if (board.getColor(ReversiPoint.H7) == color && !fixed[ReversiPoint.H7.x][ReversiPoint.H7.y]) {
				evaluation++;
			}
		}
		return evaluation;
	}

	/**
	 * 山の評価関数
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return 山評価値
	 */
	public static int evalMountain(ReversiBoard board, int color) {
		int evaluation = 0;
		// 上ライン
		if (board.getColor(ReversiPoint.A1) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.H1) == ReversiColor.NONE.getValue()) {
			// 左⇒右
			int x = 1;
			for (; x < 7 && board.getColor(x, 0) == color; x++)
				;
			if (x == 7) {
				evaluation++;
			}
		}
		// 左ライン
		if (board.getColor(ReversiPoint.A1) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.A8) == ReversiColor.NONE.getValue()) {
			// 上⇒下
			int y = 1;
			for (; y < 7 && board.getColor(0, y) == color; y++)
				;
			if (y == 7) {
				evaluation++;
			}
		}
		// 右ライン
		if (board.getColor(ReversiPoint.H1) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.H8) == ReversiColor.NONE.getValue()) {
			// 上⇒下
			int y = 1;
			for (; y < 7 && board.getColor(7, y) == color; y++)
				;
			if (y == 7) {
				evaluation++;
			}
		}
		// 下ライン
		if (board.getColor(ReversiPoint.A8) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.H8) == ReversiColor.NONE.getValue()) {
			// 左⇒右
			int x = 1;
			for (; x < 7 && board.getColor(x, 7) == color; x++)
				;
			if (x == 7) {
				evaluation++;
			}
		}
		return evaluation;
	}

	/**
	 * 翼の評価関数
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return 翼評価値
	 */
	public static int evalWing(ReversiBoard board, int color) {
		int evaluation = 0;
		// 上ライン
		if (board.getColor(ReversiPoint.A1) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.H1) == ReversiColor.NONE.getValue()) {
			// 左⇒右
			int x = 1;
			for (; x < 7 && board.getColor(x, 0) == color; x++)
				;
			if (x == 6 && board.getColor(ReversiPoint.F2) == color) {
				evaluation++;
			} else {
				// 右⇒左
				x = 6;
				for (; 0 < x && board.getColor(x, 0) == color; x--)
					;
				if (x == 1 && board.getColor(ReversiPoint.C2) == color) {
					evaluation++;
				}
			}
		}
		// 左ライン
		if (board.getColor(ReversiPoint.A1) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.A8) == ReversiColor.NONE.getValue()) {
			// 上⇒下
			int y = 1;
			for (; y < 7 && board.getColor(0, y) == color; y++)
				;
			if (y == 6 && board.getColor(ReversiPoint.B6) == color) {
				evaluation++;
			} else {
				// 下⇒上
				y = 6;
				for (; 0 < y && board.getColor(0, y) == color; y--)
					;
				if (y == 1 && board.getColor(ReversiPoint.B3) == color) {
					evaluation++;
				}
			}
		}
		// 右ライン
		if (board.getColor(ReversiPoint.H1) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.H8) == ReversiColor.NONE.getValue()) {
			// 上⇒下
			int y = 1;
			for (; y < 7 && board.getColor(7, y) == color; y++)
				;
			if (y == 6 && board.getColor(ReversiPoint.G6) == color) {
				evaluation++;
			} else {
				// 下⇒上
				y = 6;
				for (; 0 < y && board.getColor(7, y) == color; y--)
					;
				if (y == 1 && board.getColor(ReversiPoint.G3) == color) {
					evaluation++;
				}
			}
		}
		// 下ライン
		if (board.getColor(ReversiPoint.A8) == ReversiColor.NONE.getValue()
				&& board.getColor(ReversiPoint.H8) == ReversiColor.NONE.getValue()) {
			// 左⇒右
			int x = 1;
			for (; x < 7 && board.getColor(x, 7) == color; x++)
				;
			if (x == 6 && board.getColor(ReversiPoint.F7) == color) {
				evaluation++;
			} else {
				// 右⇒左
				x = 6;
				for (; 0 < x && board.getColor(x, 7) == color; x--)
					;
				if (x == 1 && board.getColor(ReversiPoint.C7) == color) {
					evaluation++;
				}
			}
		}
		return evaluation;
	}

	/**
	 * 包囲評価
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return 包囲評価値
	 */
	public static int evalSurrounded(ReversiBoard board, int color) {
		int evaluation = 0;
		final boolean check[][] = new boolean[ReversiBoard.SIZE][ReversiBoard.SIZE];
		for (int x = 0; x < ReversiBoard.SIZE; x++) {
			for (int y = 0; y < ReversiBoard.SIZE; y++) {
				if (board.getColor(x, y) == color) {
					for (int xx = -1; xx <= 1; xx++) {
						for (int yy = -1; yy <= 1; yy++) {
							final int posX = x + xx;
							final int posY = y + yy;
							if (ReversiPoint.isValidRange(posX, posY) && !check[posX][posY]) {
								final int nextColor = board.getColor(posX, posY);
								if (nextColor == ReversiColor.NONE.getValue()) {
									check[posX][posY] = true;
									evaluation++;
								}
							}
						}
					}
				}
			}
		}
		return evaluation;
	}

	/**
	 * 隅の着手可能評価
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return 隅の着手可能評価
	 */
	public static int evalEnableGetCorner(ReversiBoard board, int color) {
		int enableGetCorner = 0;
		if (board.canFlip(ReversiPoint.A1, color)) {
			enableGetCorner++;
		}
		if (board.canFlip(ReversiPoint.A8, color)) {
			enableGetCorner++;
		}
		if (board.canFlip(ReversiPoint.H1, color)) {
			enableGetCorner++;
		}
		if (board.canFlip(ReversiPoint.H8, color)) {
			enableGetCorner++;
		}
		return enableGetCorner;
	}

	/**
	 * 手数の評価値
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return 手数評価値
	 */
	public static int evalFlipCount(ReversiBoard board, int color) {
		int flipCount = board.getFlipCount(color);
		// Ⅹ打ち可能手については評価値から除外
		if (board.canFlip(ReversiPoint.B2, color)) {
			flipCount--;
		}
		if (board.canFlip(ReversiPoint.B7, color)) {
			flipCount--;
		}
		if (board.canFlip(ReversiPoint.G2, color)) {
			flipCount--;
		}
		if (board.canFlip(ReversiPoint.G7, color)) {
			flipCount--;
		}
		return flipCount;
	}

	/**
	 * 黒・白の評価値を元に指定した色の偏差値を算出する
	 *
	 * @param black 黒の評価値
	 * @param white 白の評価値
	 * @param color 色
	 * @return 偏差値
	 */
	public static double std(double black, double white, int color) {
		final double sum = black + white;
		final double average = sum / 2;
		final double variance = (Math.pow(black - average, 2) + Math.pow(white - average, 2)) / 2;
		final double stdev = Math.sqrt(variance);
		final double target = (color == ReversiColor.BLACK.getValue()) ? black : white;
		return (50 + (10 * (target - average))) / stdev;
	}

	/**
	 * 黒・白の評価値を元に指定した色の偏差値を算出する
	 *
	 * @param black 黒の評価値
	 * @param white 白の評価値
	 * @param color 色
	 * @return 偏差値
	 */
	public static double xxx(double black, double white, int color) {
		SynchronizedSummaryStatistics statistic = new SynchronizedSummaryStatistics();
		statistic.addValue(black);
		statistic.addValue(white);
		double stdev = statistic.getStandardDeviation();
		final double average = statistic.getSum() / 2;
		final double target = (color == ReversiColor.BLACK.getValue()) ? black : white;
		return (50 + (10 * (target - average))) / stdev;
	}
}
