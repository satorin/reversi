package org.hn.sato.reversi.bit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.bitsimulator.BitEval;
import org.hn.sato.reversi.ai.bitsimulator.BitSimulator;
import org.hn.sato.reversi.ai.bitsimulator.MiddleBitSimulator;

/**
 * ビットボードユーティリティ
 *
 * @author s-kotaki
 *
 */
public final class BitBoardUtil {

	/**
	 * 指定した64ビット数値をバイナリ文字列で返す
	 *
	 * @param board 64ビット数値
	 * @return バイナリ文字列
	 */
	public static String toBitString(long board) {
		return StringUtils.leftPad(Long.toBinaryString(board), 64, '0');
	}

	/**
	 * 指定した64ビット数値をHEX文字列で返す
	 *
	 * @param board 64ビット数値
	 * @return HEX文字列
	 */
	public static String toHexString(long board) {
		return StringUtils.leftPad(Long.toHexString(board), 16, '0');
	}

	/**
	 * 指定した64ビット数値が盤面上でどのような並びかを文字列で返す
	 *
	 * @param board 64ビット数値
	 * @return 盤面の文字列表現
	 */
	public static String toBoardString(long board) {
		return new BitBoard(board, 0L).toString();
	}

	/**
	 * この盤面情報をリバーシの盤面オブジェクトに変換する
	 *
	 * @param board ビット盤面
	 * @return 盤面情報
	 */
	public static ReversiBoard toReversiBoard(BitBoard board) {
		final ReversiBoard b = new ReversiBoard();
		// A1が最上位ビット、H8が最下位ビット
		for (int i = 0; i < 64; i++) {
			final long shift = BitMask.MIN.mask >>> i;
			if ((board.blackDiscs & shift) != 0) {
				b.put(i % 8, i / 8, ReversiColor.BLACK.getValue());
			} else if ((board.whiteDiscs & shift) != 0) {
				b.put(i % 8, i / 8, ReversiColor.WHITE.getValue());
			}
		}
		return b;
	}

	/**
	 * 着手可能位置の情報から対象位置のみビットを立てた情報を配列で返す
	 *
	 * @param flipPoint 着手可能位置
	 * @return 着手可能位置の配列
	 */
	public static long[] toPointArray(long flipPoint) {
		final long[] flipPointArray = new long[Long.bitCount(flipPoint)];
		int index = 0;
		while (flipPoint != 0) {
			final long lowest = Long.lowestOneBit(flipPoint);
			flipPointArray[index++] = lowest;
			flipPoint ^= lowest;
		}
		return flipPointArray;
	}

	/**
	 * 着手可能位置の情報から対象位置のリストで返す
	 *
	 * @param flipPoint 着手可能位置
	 * @return 着手可能位置のリスト
	 */
	public static List<BitPoint> toPointList(final long flipPoint) {
		return BitBoardUtil.convertToBitPointList(BitBoardUtil.toPointArray(flipPoint));
	}

	/**
	 * 着手可能位置の情報から対象位置のリストで返す
	 *
	 * @param flipPointArray 着手可能位置配列
	 * @return 着手可能位置のリスト
	 */
	public static List<BitPoint> convertToBitPointList(long[] flipPointArray) {
		return Arrays.stream(flipPointArray).mapToObj(p -> BitPoint.fromMask(p)).collect(Collectors.toList());
	}

	/**
	 * 着手可能リストを有利な順にソートして返却する
	 * <p>
	 * 以下条件を適用<br>
	 * <ol>
	 * <li>隅に着手できる場合は先頭に配置
	 * <li>Ｃ打ち、Ⅹ打ち以外の着手位置を開放度の小さい順に配置
	 * <li>Ｃ打ちを配置
	 * <li>Ⅹ打ちを末尾に配置
	 * </ol>
	 *
	 * @param board 盤面
	 * @param color 色
	 * @return ソートした着手可能リスト
	 */
	public static long[] getFlipPointListOrderByPriority(final BitBoard board, final int color) {
		final long flipPoint = board.getFlipPoint(color);
		final long corners = flipPoint & BitMask.ALL_CORNERS.mask;
		final long xs = flipPoint & BitMask.ALL_XS.mask;
		final long cs = flipPoint & BitMask.ALL_CS.mask;
		final long normals = flipPoint & ~corners & ~xs & ~cs;

		// 開放度順のソート
		final long[] normalArray = Arrays.stream(BitBoardUtil.toPointArray(normals)).mapToObj(point -> {
			final int openRatio = BitEval.calcOpenRatioEx2(board, point, color);
			return new SortableBitPoint(openRatio, point);
		}).sorted((a, b) -> Double.compare(a.sortKey, b.sortKey)).mapToLong(point -> point.point).toArray();
		final long[] cornerArray = BitBoardUtil.toPointArray(corners);
		final long[] xArray = BitBoardUtil.toPointArray(xs);
		final long[] cArray = BitBoardUtil.toPointArray(cs);
		final long[] pointArray = new long[Long.bitCount(flipPoint)];
		System.arraycopy(cornerArray, 0, pointArray, 0, cornerArray.length);
		System.arraycopy(normalArray, 0, pointArray, cornerArray.length, normalArray.length);
		System.arraycopy(cArray, 0, pointArray, cornerArray.length + normalArray.length, cArray.length);
		System.arraycopy(xArray, 0, pointArray, cornerArray.length + normalArray.length + cArray.length, xArray.length);

		return pointArray;
	}

	/**
	 * 反復進化探索を行って着手可能リストを有利な順にソートする
	 *
	 * @param board 盤面
	 * @param color 色
	 * @param baseColor 基準色
	 * @param depth 探索の深さ
	 * @return ソートした着手可能リスト
	 * @throws InterruptedException 割り込み例外
	 */
	@Deprecated
	public static long[] getFlipPointListIterativeDeepening(BitBoard board, final int color,
			final int baseColor, int depth) throws InterruptedException {
		final List<SortableBitPoint> sortablePointList = new ArrayList<>();
		final BitSimulator simulator = new MiddleBitSimulator();
		double alpha = Double.NEGATIVE_INFINITY;
		final double beta = Double.POSITIVE_INFINITY;
		for (long point : board.getFlipPointArray(color)) {
			final BitBoard newBoard = board.flip(point, color);
			final double value = BitBoardUtil.recurseIterativeDeeping(newBoard, board, -color, baseColor, depth - 1, alpha,
					beta, simulator);
			sortablePointList.add(new SortableBitPoint(value, point));
		}
		// 評価値順のソート
		return sortablePointList.stream().sorted((a, b) -> color == baseColor ? Double.compare(b.sortKey, a.sortKey)
				: Double.compare(a.sortKey, b.sortKey)).mapToLong(p -> p.point).toArray();
	}

	/**
	 * 反復深化探索の再帰処理
	 *
	 * @param board 着手後の盤面
	 * @param before 着手前の盤面
	 * @param color 色
	 * @param baseColor 基準色
	 * @param depth 探索の深さ
	 * @param alpha α
	 * @param beta β
	 * @param simulator 評価関数
	 * @return 評価値
	 * @throws InterruptedException 割り込み例外
	 */
	private static final double recurseIterativeDeeping(BitBoard board, BitBoard before, int color, int baseColor, int depth,
			double alpha, double beta, BitSimulator simulator) throws InterruptedException {

		if (depth < 1) {
			return simulator.eval(board, before, color, baseColor).getEvaluation();
		}

		int turn = color;
		if (board.isPass(turn)) {
			// パスの場合
			turn = -turn;
			if (board.isPass(turn)) {
				// 双方パスの場合
				return simulator.eval(board, before, color, baseColor).getEvaluation();
			}
		}

		double result = Double.NEGATIVE_INFINITY;

		for (long point : BitBoardUtil.getFlipPointListOrderByPriority(board, turn)) {
			final BitBoard newBoard = board.flip(point, turn);
			final double value = BitBoardUtil.recurseIterativeDeeping(newBoard, board, -turn, baseColor, depth - 1, alpha,
					beta,
					simulator);
			// Min-Max法による評価判定
			if (turn == baseColor) {
				if (beta < value) {
					// βカット
					return value;
				}
				if (result < value) {
					// αを更新(大きければ)
					alpha = Math.max(alpha, result);
					// 現時点での自分の最善手評価結果を保持
					result = value;
				}
			} else {
				if (value < alpha) {
					// αカット
					return value;
				}
				if (value < result) {
					// βを更新(小さければ)
					beta = Math.min(beta, result);
					// 現時点での相手の最善手評価結果を保持
					result = value;
				}
			}
		}
		return result;
	}

}
