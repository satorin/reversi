package org.hn.sato.reversi.ai.simulator;

import java.util.List;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiPoint;
import org.hn.sato.reversi.ai.EvalResult;

import lombok.Getter;

/**
 * 盤面評価を行うシミュレーター
 *
 * @author s-kotaki
 *
 */
public abstract class AbstractEvalSimulator implements Simulator {

	/** 評価回数 */
	@Getter
	private long evalCount;
	/** αによる枝狩り回数 */
	@Getter
	private long alphaCutCount;
	/** βによる枝狩り回数 */
	@Getter
	private long betaCutCount;

	@Override
	public final EvalResult execute(ReversiBoard board, int color, int baseColor, int depth, double alpha,
			double beta) throws InterruptedException {

		if (depth < 1) {
			this.evalCount++;
			// αβ法では評価の基準は同一
			return this.eval(board, baseColor);
		}

		int turn = color;
		if (!board.canFlip(turn)) {
			// パスの場合
			Thread.sleep(1000);
			turn = -turn;
			if (!board.canFlip(turn)) {
				// 双方パスの場合
				this.evalCount++;
				return this.eval(board, baseColor);
			}
		}

		EvalResult evalResult;
		if (turn == baseColor) {
			evalResult = SimulatorEvalResult.min(board, baseColor);
		} else {
			evalResult = SimulatorEvalResult.max(board, baseColor);
		}

		// 反復深化を行うと負荷が非常に高いので、ここでは開放度による順位付けを行う
		final List<ReversiPoint> movablePointList = SimulatorEval.getFlipPointListOrderByPriority(board, turn);

		for (ReversiPoint point : movablePointList) {
			final ReversiBoard newBoard = board.clone();
			newBoard.flip(point, turn);
			final EvalResult result = this.execute(newBoard, -turn, baseColor, depth - 1, alpha, beta);
			final double eval = result.getEvaluation();
			// Min-Max法による評価判定
			if (turn == baseColor) {
				if (beta < eval) {
					// βカット
					betaCutCount++;
					return result;
				}
				if (evalResult.getEvaluation() < eval) {
					// αを更新(大きければ)
					alpha = Math.max(alpha, eval);
					// 現時点での自分の最善手評価結果を保持
					evalResult = result;
				}
			} else {
				if (eval < alpha) {
					// αカット
					alphaCutCount++;
					return result;
				}
				if (eval < evalResult.getEvaluation()) {
					// βを更新(小さければ)
					beta = Math.min(beta, eval);
					// 現時点での相手の最善手評価結果を保持
					evalResult = result;
				}
			}
		}
		return evalResult;
	}

	@Override
	public long getAlphaBetaCutCount() {
		return this.alphaCutCount + this.betaCutCount;
	}
}
