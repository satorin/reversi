package org.hn.sato.reversi.ai;

import org.hn.sato.reversi.ReversiPoint;

/**
 * 評価結果
 *
 * @author s-kotaki
 *
 */
public interface EvalResult {

	/**
	 * 評価対象の色を返却する
	 *
	 * @return 評価対象の色
	 */
	int getColor();

	/**
	 * 着手位置を設定する
	 *
	 * @param point 着手位置
	 */
	void setPoint(ReversiPoint point);

	/**
	 * 着手位置を返却する
	 *
	 * @return 着手位置
	 */
	ReversiPoint getPoint();

	/**
	 * 評価値を設定する
	 *
	 * @param evaluation 評価値
	 */
	void setEvaluation(double evaluation);

	/**
	 * 評価値を返却する
	 *
	 * @return 評価値
	 */
	double getEvaluation();

}
