package org.hn.sato.reversi.bit;

import java.io.Serializable;

import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.ai.common.AbstractEvalResult;

/**
 * 評価結果オブジェクト
 *
 * @author s-kotaki
 *
 */
public class BitEvalResult extends AbstractEvalResult implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = -2985114372907175361L;

	/**
	 * 最大値を示す評価結果を生成する
	 *
	 * @param board 盤面
	 * @param color 基準色
	 * @return 評価結果
	 */
	public static EvalResult max(BitBoard board, int color) {
		final BitEvalResult result = new BitEvalResult(board, color);
		result.setEvaluation(Double.POSITIVE_INFINITY);
		return result;
	}

	/**
	 * 最小値を示す評価結果を生成する
	 *
	 * @param board 盤面
	 * @param color 基準色
	 * @return 評価結果
	 */
	public static BitEvalResult min(BitBoard board, int color) {
		final BitEvalResult result = new BitEvalResult(board, color);
		result.setEvaluation(Double.NEGATIVE_INFINITY);
		return result;
	}

	/**
	 * コンストラクタ
	 *
	 * @param board 盤面
	 * @param color 基準色
	 */
	public BitEvalResult(BitBoard board, int color) {
		super(board, color);
	}
}
