package org.hn.sato.reversi.ai.bitsimulator;

import java.text.MessageFormat;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.ArtificalIntelligence;
import org.hn.sato.reversi.ai.EvalResult;
import org.hn.sato.reversi.bit.BitBoard;
import org.hn.sato.reversi.bit.BitBoardUtil;
import org.hn.sato.reversi.bit.BitEvalResult;
import org.hn.sato.reversi.bit.BitMask;
import org.hn.sato.reversi.bit.BitPoint;
import org.hn.sato.reversi.logger.ReversiLogger;

import lombok.Getter;

/**
 * 盤面評価ロジック
 * <p>
 * <ul>
 * <li>Mini-Max法
 * <li>αβ法
 * </ul>
 *
 * @author s-kotaki
 *
 */
public class SBitMiniMaxAlphaBeta implements ArtificalIntelligence {

	/** ロガー */
	private static final ReversiLogger LOGGER = ReversiLogger.getInstance();

	@Getter
	private long evalCount;
	@Getter
	private long alphaCutCount;
	@Getter
	private long betaCutCount;

	@Override
	public final EvalResult execute(ReversiBoard board, ReversiColor color, int depth, int finalDepth) throws InterruptedException {
		// 開始時間
		final long start = System.currentTimeMillis();

		// シミュレーション
		LOGGER.out(MessageFormat.format("評価色 {0}\n{1}", color, board));

		// ビットボードの生成
		final BitBoard bitBoard = new BitBoard(board);

		final int none = bitBoard.countNone();
		final BitSimulator simulator;
		final int actualDepth;
//		if (48 < none) {
		if (Long.bitCount((bitBoard.blackDiscs | bitBoard.whiteDiscs) & BitMask.DIAGONAL.mask) <= 2) {
			// 辺に2つ以下
			simulator = new BeginningBitSimulator();
			actualDepth = depth;
		} else if (finalDepth < none) {
			simulator = new MiddleBitSimulator();
			actualDepth = depth;
		} else {
			// 最終読み
			simulator = new FinalBitSimulator();
			actualDepth = none;
		}
		LOGGER.out(MessageFormat.format("思考ルーチン：{0}", simulator.getClass().getSimpleName()));
		LOGGER.out(MessageFormat.format("探索の深さ：{0}", actualDepth));
		// 集計用
		// 評価回数
		long evalCount = 0L;
		// αカット回数
		long alphaCutCount = 0L;
		// βカット回数
		long betaCutCount = 0L;

		final int turn = color.getValue();
		// 最善手の評価
		EvalResult bestResult = BitEvalResult.min(bitBoard, turn);

		// 自分が選択した最高値
		double alpha = Double.NEGATIVE_INFINITY;
		// 相手が選択した最低値
		final double beta = Double.POSITIVE_INFINITY;

		final long[] flipPointArray = simulator.sortedFlip(bitBoard, turn);
		LOGGER.out(MessageFormat.format("着手可能位置：{0}", BitBoardUtil.convertToBitPointList(flipPointArray)));
		for (long point : flipPointArray) {
			// １手しか選べない場合でもその評価値を取得するために探索を行う
			final BitPoint bitPoint = BitPoint.fromMask(point);
			LOGGER.out(MessageFormat.format("{0} の探索", bitPoint));
			long inStart = System.currentTimeMillis();
			final BitBoard newBoard = bitBoard.flip(point, turn);
			final EvalResult result = simulator.execute(newBoard, bitBoard, -turn, turn, actualDepth - 1, alpha, beta);
			if (alpha < result.getEvaluation()) {
				// １手目は自身の着手であることが確定しているのでαのみ設定
				alpha = result.getEvaluation();
				bestResult = result;
				bestResult.setPoint(bitPoint.toReversiPoint());
			}
			long inEnd = System.currentTimeMillis();
			LOGGER.out(MessageFormat.format("評価：{0}({1}={2}) {3}ms 探索回数={4} α={5} β={6} ({7})\n{8}", color, bitPoint,
					Double.toString(result.getEvaluation()), inEnd - inStart, simulator.getEvalCount() - evalCount,
					simulator.getAlphaCutCount() - alphaCutCount, simulator.getBetaCutCount() - betaCutCount, simulator.getClass().getSimpleName(), result));
			evalCount = simulator.getEvalCount();
			alphaCutCount = simulator.getAlphaCutCount();
			betaCutCount = simulator.getBetaCutCount();
		}

		final long end = System.currentTimeMillis();

		// 評価値の集計
		this.evalCount += simulator.getEvalCount();
		this.alphaCutCount += simulator.getAlphaCutCount();
		this.betaCutCount += simulator.getBetaCutCount();

		LOGGER.out(MessageFormat.format("着手：{0}({1})={2} ({3})\n{4}", color, bestResult.getPoint(),
				Double.toString(bestResult.getEvaluation()), simulator.getClass().getSimpleName(), bestResult));
		LOGGER.out(MessageFormat.format("α枝狩り回数：{0}", simulator.getAlphaCutCount()));
		LOGGER.out(MessageFormat.format("β枝狩り回数：{0}", simulator.getBetaCutCount()));
		LOGGER.out(MessageFormat.format("αβ枝狩り回数：{0}", simulator.getAlphaBetaCutCount()));
		LOGGER.out(MessageFormat.format("評価回数：{0}", simulator.getEvalCount()));
		LOGGER.out(MessageFormat.format("経過時間：{0}ms\n", end - start));

		return bestResult;
	}

	@Override
	public void resetEvaluation() {
		this.evalCount = 0L;
		this.alphaCutCount = 0L;
		this.betaCutCount = 0L;
	}
}
