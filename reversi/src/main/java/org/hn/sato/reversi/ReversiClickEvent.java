package org.hn.sato.reversi;

import org.hn.sato.reversi.ai.ArtificalIntelligence;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import lombok.Getter;
import lombok.Setter;

/**
 * 盤面クリックイベント
 *
 * @author s-kotaki
 *
 */
public class ReversiClickEvent implements EventHandler<MouseEvent> {

	/** リバーシメイン */
	private Reversi reversi;
	/** 色 */
	private ReversiColor color;
	/** 着手位置 */
	@Setter
	@Getter
	private ReversiPoint point;
	/** AI */
	@Setter
	private ArtificalIntelligence intelligence;

	/**
	 * コンストラクタ
	 *
	 * @param reversi リバーシメイン
	 * @param color 色
	 */
	public ReversiClickEvent(Reversi reversi, ReversiColor color) {
		this.reversi = reversi;
		this.color = color;
	}

	@Override
	public void handle(MouseEvent event) {
		if (this.intelligence == null || this.reversi.currentTurn != color.getValue()) {
			return;
		}
		final ReversiPoint point = this.reversi.calcPoint(event.getSceneX(), event.getSceneY());
		if (this.reversi.board.canFlip(point, this.reversi.currentTurn)) {
			this.setPoint(point);
			synchronized (this.intelligence) {
				this.intelligence.notifyAll();
			}
		}
	}
}
