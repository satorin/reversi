package org.hn.sato.reversi.logger;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

/**
 * 操作ログ
 *
 * @author kotaki_s
 *
 */
public final class ReversiLogger {

	/** 自インスタンス */
	private static ReversiLogger instance;
	/** ロガー */
	private static Logger logger;

	/**
	 * コンストラクタ
	 */
	private ReversiLogger() {
		logger = Logger.getLogger("reversi");
	}

	/**
	 * 操作ログのインスタンスを返却する
	 *
	 * @return 操作ログインスタンス
	 */
	public static ReversiLogger getInstance() {
		if (instance == null) {
			instance = new ReversiLogger();
		}
		return instance;
	}

	/**
	 * ログを出力する
	 *
	 * @param message メッセージ
	 */
	public void out(String message) {
		logger.info(message);
	}

	/**
	 * ログを出力する
	 *
	 * @param message メッセージ
	 * @param replacements 置換パラメータ
	 */
	public void out(String message, Object... replacements) {
		logger.info(MessageFormat.format(message, replacements));
	}
}
