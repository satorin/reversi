package org.hn.sato.reversi.bit;

import java.io.Serializable;
import java.text.MessageFormat;

import org.hn.sato.reversi.ReversiAbstractBoard;
import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;

/**
 * ビットボード
 * <p>
 * 一般的にビットボードの実装はA1(左上)を最下位ビット、H8(右下)を最上位ビットとするようであるが、<br>
 * 左に9ビットシフトしたときに実際には右下を示すなど直感的にわかりにくいため、<br>
 * ここではA1(左上)を最上位ビット、H8(右下)を最下位ビットとして実装する。
 *
 * @author s-kotaki
 *
 */
public class BitBoard extends ReversiAbstractBoard implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = -260606181545423505L;
	/** 黒 */
	public long blackDiscs;
	/** 白 */
	public long whiteDiscs;

	/**
	 * コンストラクタ
	 */
	public BitBoard() {
	}

	/**
	 * コンストラクタ
	 *
	 * @param blackDiscs 黒の盤面
	 * @param whiteDiscs 白の盤面
	 */
	public BitBoard(long blackDiscs, long whiteDiscs) {
		this.blackDiscs = blackDiscs;
		this.whiteDiscs = whiteDiscs;
	}

	/**
	 * 盤面の文字列情報(黒:● 白:○ 空:□)からビットボードを生成する
	 *
	 * @param boardString 盤面の文字列情報
	 */
	public BitBoard(String boardString) {
		int index = 0;
		for (char c : boardString.toCharArray()) {
			if (c == '●') {
				blackDiscs |= BitMask.MIN.mask >>> index;
				index++;
			} else if (c == '○') {
				whiteDiscs |= BitMask.MIN.mask >>> index;
				index++;
			} else if (c == '□') {
				index++;
			}
		}
	}

	/**
	 * コンストラクタ
	 *
	 * @param board 盤面
	 */
	public BitBoard(ReversiBoard board) {
		this.blackDiscs = 0L;
		this.whiteDiscs = 0L;
		// A1を最上位ビット、H8を最下位ビットに配置する long 型に変換する
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				final int color = board.getColor(x, y);
				if (color == ReversiColor.BLACK.getValue()) {
					this.blackDiscs |= (BitMask.MIN.mask >>> (x + y * 8));
				} else if (color == ReversiColor.WHITE.getValue()) {
					this.whiteDiscs |= (BitMask.MIN.mask >>> (x + y * 8));
				}
			}
		}
	}

	/**
	 * ビットマスクで指定した場所に石を配置する
	 *
	 * @param point 位置
	 * @param color 色
	 */
	public void put(long point, int color) {
		if (Long.bitCount(point) != 1) {
			throw new IllegalArgumentException("無効な位置が指定されました。" + BitBoardUtil.toBitString(point));
		}
		if (color == ReversiColor.BLACK.getValue()) {
			this.blackDiscs |= point;
			this.whiteDiscs &= ~point;
		} else if (color == ReversiColor.WHITE.getValue()) {
			this.whiteDiscs |= point;
			this.blackDiscs &= ~point;
		} else if (color == ReversiColor.NONE.getValue()) {
			this.blackDiscs &= ~point;
			this.whiteDiscs &= ~point;
		} else {
			throw new IllegalArgumentException("無効な色が指定されました。" + color);
		}
	}

	/**
	 * ビットポイントで指定した場所に石を配置する
	 *
	 * @param point 位置
	 * @param color 色
	 */
	public void put(BitPoint point, int color) {
		this.put(point.mask, color);
	}

	/**
	 * 着手可能位置のビットを立てた long 値を返す
	 *
	 * @param color 色
	 * @return 着手可能位置
	 */
	public long getFlipPoint(int color) {
		final long none = this.getNone();
		long myBoard;
		long opBoard;
		if (color == ReversiColor.BLACK.getValue()) {
			myBoard = this.blackDiscs;
			opBoard = this.whiteDiscs;
		} else if (color == ReversiColor.WHITE.getValue()) {
			myBoard = this.whiteDiscs;
			opBoard = this.blackDiscs;
		} else {
			throw new IllegalArgumentException("指定した色が無効です。" + color);
		}
		// マスク
		long mask;
		// ビット演算用変数
		long t;
		// 着手可能位置
		long flip = 0L;

		// 左右のマスク
		mask = opBoard & BitMask.LEFT_RIGHT_REVERSE.mask;

		// 左方向
		t = mask & (myBoard << 1); // 自石の左にある相手の石を調べる
		t |= mask & (t << 1);
		t |= mask & (t << 1);
		t |= mask & (t << 1);
		t |= mask & (t << 1);
		t |= mask & (t << 1);
		flip |= none & (t << 1);

		// 右方向
		t = mask & (myBoard >>> 1); // 自石の右にある相手の石を調べる
		t |= mask & (t >>> 1);
		t |= mask & (t >>> 1);
		t |= mask & (t >>> 1);
		t |= mask & (t >>> 1);
		t |= mask & (t >>> 1);
		flip |= none & (t >>> 1);

		// 上下のマスク
		mask = opBoard & BitMask.UPPER_LOWER_REVERSE.mask;

		// 上方向
		t = mask & (myBoard << 8); // 自石の上にある相手の石を調べる
		t |= mask & (t << 8);
		t |= mask & (t << 8);
		t |= mask & (t << 8);
		t |= mask & (t << 8);
		t |= mask & (t << 8);
		flip |= none & (t << 8);

		// 下方向
		t = mask & (myBoard >>> 8); // 自石の下にある相手の石を調べる
		t |= mask & (t >>> 8);
		t |= mask & (t >>> 8);
		t |= mask & (t >>> 8);
		t |= mask & (t >>> 8);
		t |= mask & (t >>> 8);
		flip |= none & (t >>> 8);

		// 斜めのマスク
		mask = opBoard & BitMask.DIAGONAL_REVERSE.mask;

		// 右上方向
		t = mask & (myBoard << 7); // 自石の右上にある相手の石を調べる
		t |= mask & (t << 7);
		t |= mask & (t << 7);
		t |= mask & (t << 7);
		t |= mask & (t << 7);
		t |= mask & (t << 7);
		flip |= none & (t << 7);

		// 左下方向
		t = mask & (myBoard >>> 7); // 自石の左下にある相手の石を調べる
		t |= mask & (t >>> 7);
		t |= mask & (t >>> 7);
		t |= mask & (t >>> 7);
		t |= mask & (t >>> 7);
		t |= mask & (t >>> 7);
		flip |= none & (t >>> 7);

		// 左上方向
		t = mask & (myBoard << 9); // 自石の左上にある相手の石を調べる
		t |= mask & (t << 9);
		t |= mask & (t << 9);
		t |= mask & (t << 9);
		t |= mask & (t << 9);
		t |= mask & (t << 9);
		flip |= none & (t << 9);

		// 右下方向
		t = mask & (myBoard >>> 9); // 自石の右下にある相手の石を調べる
		t |= mask & (t >>> 9);
		t |= mask & (t >>> 9);
		t |= mask & (t >>> 9);
		t |= mask & (t >>> 9);
		t |= mask & (t >>> 9);
		flip |= none & (t >>> 9);

		return flip;
	}

	/**
	 * 着手可能位置のリストを返す
	 *
	 * @param color 色
	 * @return 着手可能位置リスト
	 */
	public long[] getFlipPointArray(int color) {
		return BitBoardUtil.toPointArray(this.getFlipPoint(color));
	}

	/**
	 * 指定した位置に着手可能かを返す
	 *
	 * @param point 着手位置
	 * @param color 色
	 * @return 着手可能な場合 true
	 */
	public boolean canFlip(long point, int color) {
		return (this.getFlipPoint(color) & point) != 0;
	}

	/**
	 * 指定した色がパスかどうかを返す
	 *
	 * @param color 色
	 * @return 指定した色がパスの場合 true
	 */
	public boolean isPass(int color) {
		return this.getFlipPoint(color) == 0;
	}

	/**
	 * 指定した位置に着手する
	 *
	 * @param point 着手位置
	 * @param color 色
	 * @return 着手後のビットボード
	 */
	public BitBoard flip(long point, int color) {
		if (Long.bitCount(point) != 1) {
			throw new IllegalArgumentException("無効な位置が指定されました。" + BitBoardUtil.toBitString(point));
		}
		if ((this.getNone() & point) == 0) {
			throw new IllegalArgumentException(MessageFormat.format("{0}には既に石が置かれています。", BitPoint.fromMask(point)));
		}
		long myBoard;
		long opBoard;
		if (color == ReversiColor.BLACK.getValue()) {
			myBoard = this.blackDiscs;
			opBoard = this.whiteDiscs;
		} else if (color == ReversiColor.WHITE.getValue()) {
			myBoard = this.whiteDiscs;
			opBoard = this.blackDiscs;
		} else {
			throw new IllegalArgumentException("無効な色が指定されました。" + color);
		}

		// マスク
		long mask;
		// ビット演算用変数
		long t;
		// 反転用盤面
		long rev = 0L;

		// 左上方向
		for (t = 0L, mask = (point << 9) & BitMask.LOWER_RIGHT_REVERSE.mask; (mask & opBoard) != 0; mask = (mask << 9) & BitMask.LOWER_RIGHT_REVERSE.mask) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 上方向
		for (t = 0L, mask = (point << 8); (mask & opBoard) != 0; mask = (mask << 8)) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 右上方向
		for (t = 0L, mask = (point << 7) & BitMask.LOWER_LEFT_REVERSE.mask; (mask & opBoard) != 0; mask = (mask << 7) & BitMask.LOWER_LEFT_REVERSE.mask) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 左方向
		for (t = 0L, mask = (point << 1) & BitMask.RIGHT_LINE_REVERSE.mask; (mask & opBoard) != 0; mask = (mask << 1) & BitMask.RIGHT_LINE_REVERSE.mask) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 右方向
		for (t = 0L, mask = (point >>> 1) & BitMask.LEFT_LINE_REVERSE.mask; (mask & opBoard) != 0; mask = (mask >>> 1) & BitMask.LEFT_LINE_REVERSE.mask) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 左下方向
		for (t = 0L, mask = (point >>> 7) & BitMask.UPPER_RIGHT_REVERSE.mask; (mask & opBoard) != 0; mask = (mask >>> 7) & BitMask.UPPER_RIGHT_REVERSE.mask) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 下方向
		for (t = 0L, mask = (point >>> 8); (mask & opBoard) != 0; mask = (mask >>> 8)) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}
		// 右下方向
		for (t = 0L, mask = (point >>> 9) & BitMask.UPPER_LEFT_REVERSE.mask; (mask & opBoard) != 0; mask = (mask >>> 9) & BitMask.UPPER_LEFT_REVERSE.mask) {
			t |= mask;
		}
		if ((mask & myBoard) != 0) {
			rev |= t;
		}

		// 反転
		myBoard ^= point | rev;
		opBoard ^= rev;

		return (color == ReversiColor.BLACK.getValue()) ? new BitBoard(myBoard, opBoard)
				: new BitBoard(opBoard, myBoard);
	}

	/**
	 * 空白のビットを返す
	 *
	 * @return 空白のビット
	 */
	public long getNone() {
		return ~(this.blackDiscs | this.whiteDiscs);
	}

	/**
	 * 指定した色の数を返す
	 *
	 * @param color 色
	 * @return 指定した色の数
	 */
	public int count(int color) {
		if (color == ReversiColor.BLACK.getValue()) {
			return this.countBlack();
		}
		if (color == ReversiColor.WHITE.getValue()) {
			return this.countWhite();
		}
		if (color == ReversiColor.NONE.getValue()) {
			return this.countNone();
		}
		throw new IllegalArgumentException("無効な色が指定されました。" + color);
	}

	/**
	 * 黒の数を返す
	 *
	 * @return 黒の数
	 */
	public int countBlack() {
		return Long.bitCount(this.blackDiscs);
	}

	/**
	 * 白の数を返す
	 *
	 * @return 白の数
	 */
	public int countWhite() {
		return Long.bitCount(this.whiteDiscs);
	}

	/**
	 * 空白の数を返す
	 *
	 * @return 空白の数
	 */
	public int countNone() {
		return Long.bitCount(this.getNone());
	}

	/**
	 * 指定した色のビット列を返す
	 *
	 * @param color 色
	 * @return ビット列
	 */
	public String toBitString(int color) {
		if (color == ReversiColor.BLACK.getValue()) {
			return BitBoardUtil.toBitString(this.blackDiscs);
		}
		if (color == ReversiColor.WHITE.getValue()) {
			return BitBoardUtil.toBitString(this.whiteDiscs);
		}
		if (color == ReversiColor.NONE.getValue()) {
			return BitBoardUtil.toBitString(this.getNone());
		}
		throw new IllegalArgumentException("無効な色が指定されました。" + color);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(64 + 8);
		// A1が最上位ビット、H8が最下位ビット
		for (int i = 63; 0 <= i; i--) {
			final long shift = 1L << i;
			if ((this.blackDiscs & shift) != 0) {
				sb.append("●");
			} else if ((this.whiteDiscs & shift) != 0) {
				sb.append("○");
			} else {
				sb.append("□");
			}
			if (i % 8 == 0 && 0 < i) {
				// 右端の石と一致した場合
				sb.append("\n");
			}
		}
		return sb.toString();
	}
}
