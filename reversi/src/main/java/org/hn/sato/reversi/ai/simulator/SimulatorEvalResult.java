package org.hn.sato.reversi.ai.simulator;

import java.io.Serializable;

import org.hn.sato.reversi.ReversiBoard;
import org.hn.sato.reversi.ReversiColor;
import org.hn.sato.reversi.ai.common.AbstractEvalResult;

/**
 * 評価結果オブジェクト
 *
 * @author s-kotaki
 *
 */
public class SimulatorEvalResult extends AbstractEvalResult implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 4222423669068635244L;

	/**
	 * 最大値を示す評価結果を生成する
	 *
	 * @param board 盤面
	 * @param color 基準色
	 * @return 評価結果
	 */
	public static SimulatorEvalResult max(ReversiBoard board, int color) {
		final SimulatorEvalResult result = new SimulatorEvalResult(board, color);
		if (color == ReversiColor.BLACK.getValue()) {
			result.setEvaluation(Double.POSITIVE_INFINITY);
		} else {
			result.setEvaluation(Double.POSITIVE_INFINITY);
		}
		return result;
	}

	/**
	 * 最小値を示す評価結果を生成する
	 *
	 * @param board 盤面
	 * @param color 基準色
	 * @return 評価結果
	 */
	public static SimulatorEvalResult min(ReversiBoard board, int color) {
		final SimulatorEvalResult result = new SimulatorEvalResult(board, color);
		if (color == ReversiColor.BLACK.getValue()) {
			result.setEvaluation(Double.NEGATIVE_INFINITY);
		} else {
			result.setEvaluation(Double.NEGATIVE_INFINITY);
		}
		return result;
	}

	/**
	 * コンストラクタ
	 *
	 * @param board 盤面
	 * @param color 基準色
	 */
	public SimulatorEvalResult(ReversiBoard board, int color) {
		super(board, color);
	}
}
